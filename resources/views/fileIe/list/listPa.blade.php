<div class="col-md-12 contenedorTable">
	<table id="list" class="table table-sm table-hover my-0" style="width:100%;">
    @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')!= false)
    <thead class="text-center">
        <tr>
            <th>Edad</th>
            <th>Sección</th>
            <th>P. academico</th>
            <th>Estado</th>
            <th>Fecha de registro</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($list as $item)
        <tr class="text-center">
            <td>{{$item->tGrado->nombre}}</td>
            <td>Sección {{$item->tSeccion->nombre}}</td>
            <td>{{$item->periodoAcademico}}</td>
            <td>
                @if($item->estadoArchivo=='0')
                    <span class="badge badge-info">Privado</span>
                @else
                    <span class="badge badge-info">Publico</span>
                @endif
            </td>
            <td>{{$item->createddate}}</td>
            <td>
                @if($item->comentario!='')
                <span class="btn btn-info btn-xs" data-toggle="tooltip" title="{{$item->comentario}}"><i class="fab fa-rocketchat"></i></span>
                @endif
                <a href="{{asset('filedcte/')}}/{{session()->get('Person')->dni}}-{{$item->idcd}}.{{$item->formato}}" target="_blank" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a>
                <span class="badge badge-secondary"><i class="fa fa-file"></i> {{count($item->tUnidad)}} unidades</span>
            </td>
        </tr>
        @endforeach
    </tbody>   
    @endif
    @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'secundaria')!= false || stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'primaria')!= false)
    <thead class="text-center">
        <tr>
            <th>Grado</th>
            <th>Sección</th>
            <th>Curso</th>
            <th>P. academico</th>
            <th>Estado</th>
            <th>Fecha de registro</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($list as $item)
        <tr class="text-center">
            <td>{{$item->tGrado->nombre}}</td>
            <td>Sección {{$item->tSeccion->nombre}}</td>
            <td>{{$item->tCurso->nombre}}</td>
            <td>{{$item->periodoAcademico}}</td>
            <td>
                @if($item->estadoArchivo=='0')
                    <span class="badge badge-info">Privado</span>
                @else
                    <span class="badge badge-info">Publico</span>
                @endif
            </td>
            <td>{{$item->createddate}}</td>
            <td>
                @if($item->comentario!='')
                <span class="btn btn-info btn-xs" data-toggle="tooltip" title="{{$item->comentario}}"><i class="fab fa-rocketchat"></i></span>
                @endif
                <a href="{{asset('filedcte/')}}/{{session()->get('Person')->dni}}-{{$item->idcd}}.{{$item->formato}}" target="_blank" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a>
                <span class="badge badge-secondary"><i class="fa fa-file"></i> {{count($item->tUnidad)}} unidades</span>
            </td>
        </tr>
        @endforeach
    </tbody> 
    @endif
    
</table>
</div>

<script>
	$(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "La I.E. tiene _TOTAL_ plan curricular subidos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No hay plan curricular de docentes.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar Docente');
    } );
</script>