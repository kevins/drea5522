<div class="col-md-12 contenedorTable">
	<h1>{{$dni}}</h1>
	<table id="listPcDcte" class="table table-sm table-hover my-0" style="width:100%;">
    <thead class="text-center">
        <tr>
            <th>DNI</th>
            <th>Nombre</th>
            <th>Grados</th>
            <th>Secciones</th>
            <th>Cursos</th>
            <th>Formato</th>
            <th>Peso</th>
            <th>Periodo Academico</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>
</div>

<script>
	// $(document).ready( function () {
 //        $('#listPcDcte').DataTable( {
 //            "paging": true,
 //            "scrollX": true,
 //            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
 //            "language": {
 //                "info": "La I.E. tiene _TOTAL_ plan curricular subidos.",
 //                "search":"",
 //                "infoFiltered": "(filtrado de _MAX_ Docentes)",
 //                "infoEmpty": "No hay registros disponibles",
 //                "sEmptyTable": "No hay plan curricular de docentes.",
 //                "lengthMenu":"_MENU_",
 //                "paginate": {
 //                    "first": "Primero",
 //                    "last": "Ultimo",
 //                    "next": "Siguiente",
 //                    "previous": "Anterior"
 //                }
 //            }
 //        } );
 //        $('input[type=search]').parent().addClass('mr-2');
 //        $('input[type=search]').prop('placeholder','Buscar Docente');
 //    } );
</script>