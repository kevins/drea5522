@extends('template/template')
@section('generalBody')
<style>
	.selectedRow{
		background: rgba(1, 1, 1, 0.22);
	}
</style>

<div class="row mt-3">
	<div class="col-md-4">
		<div class="container-fluid">
			<div class="card card-default card-info card-outline">
				<div class="card-header py-2 pl-2">
					<h3 class="card-title font-weight-bold">Compartir archivo</h3>
					<div class="card-tools">
		              	<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
		            </div>
				</div>
				<div class="card-body pt-2 pb-0">
					<form id="formInsert" action="{{url('fileIe/shareFile')}}" method="post" enctype="multipart/form-data" class="m-0">
						<input type="hidden" id="idarchivoie" name="idarchivoie">
						<input type="hidden" id="listShared" name="listShared">
						<div class="row">
							<div class="alert alert-info col-md-12 py-1 px-3 text-center mb-1 msj" style="display: none;">Compartido con <strong class="cantShared">2</strong> docentes.</div>
							<div class="col-md-12 listSharedDcte">
			                    <div class="form-group">
			                        <label for="" class="m-0">Docentes</label>
			                        <select id="shareDocente" class="form-control form-control-sm select2" name="docentes[]" multiple="multiple">
			                            <!-- <option disabled selected>Elija docente</option> -->
			                            @foreach($listTdocente as $item)
			                                <option value="{{$item->dni}}">{{$item->tPersona_->nombres}} {{$item->tPersona_->apaterno}} {{$item->tPersona_->amaterno}}</option>
			                            @endforeach
			                        </select>
			                    </div>
			                </div>
							<div class="col-md-12">
		                        <div class="form-group mb-2">
		                            <label class="m-0">Nombre:</label>
		                            <input type="text" name="nombre" id="nombre" class="form-control form-control-sm">
		                        </div>
		                    </div>
		                    <div class="col-md-12">
		                    	<div class="form-group mb-2">
		                            <label class="m-0">Archivo:</label>
		                            <input class="form-control form-control-sm" type="File" name="file" id="file">
		                        </div>
		                    </div>
		                    <div class="col-md-12">
		                        <div class="form-group">
		                            <label class="m-0">Video:</label>
		                            <input type="text" name="video" id="video" class="form-control form-control-sm">
		                        </div>
		                    </div>
		                    <div class="col-md-12">
		                        <div class="form-group">
		                            <label class="m-0">Comentario:</label>
		                            <textarea name="comentario" id="comentario" cols="30" rows="4" class="form-control form-control-sm"></textarea>
		                        </div>
		                    </div>
						</div>
						{{csrf_field()}}
					</form>
				</div>
				<div class="card-footer p-2">
		            <input type="button" value="Guardar" form="formInsert" class="btn btn-success btn-sm float-right ml-1 submit">
		            <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1 limpiar">
		        </div>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="container-fluid">
			<div class="card card-default card-info card-outline">
				<div class="card-header py-2 pl-2">Archivos compartidos</div>
				<div class="card-body p-0">
		            <div class="row">
		                <div class="col-md-12">
		                    <table id="example1" class="table table-sm table-hover m-0">
		                        <thead class="text-center">
		                            <tr>
		                                <th>Nombre</th>
		                                <th>Formato</th>
		                                <th>Compartido</th>
		                                <th>Registrado</th>
		                                <th>Opciones</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	@foreach($listTarchivoShare as $item)
		                        	<tr class="text-center">
		                            	<td>{{$item->nombre}}</td>
		                            	<td>
		                            		@if($item->formato!='')
		                            		<span class="fa {{$item->formato=='pdf'?'fa-file-pdf':''}}
		                            		{{$item->formato=='docx'?'fa-file-word':''}}
		                            		{{$item->formato=='xlsx'?'fa-file-excel':''}}"></span> {{$item->formato}}
											@else- -
											@endif
		                            	</td>
		                            	<td>
											<span class="btn right badge bg-info color-palette compartido" data-toggle="tooltip" title='' data-share="{{$item->compartido}}">Compartido con...</span>
		                            	</td>
		                            	<td>{{$item->createddate}}</td>
		                            	<td>
		                            		<a href="#" class="edit btn btn-primary btn-xs" data-id="{{$item->idarchivoie}}" data-toggle="tooltip" title="Editar registro"><i class="fa fa-edit"></i></a>
		                            		<span class="btn btn-info btn-xs fab fa-rocketchat" data-toggle="tooltip" title="Comentario/observacion: {{$item->comentario}}"></span>
		                            		@if(strpbrk($item->nombrereal, '-')!='')
		                            			<a href="{{asset('fileIe/')}}/{{$item->nombrereal}}.{{$item->formato}}" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-download"></i></a>
		                            		@endif
		                            		<a href="{{url('/fileIe/delete').'/'.$item->idarchivoie}}" class="btn btn-danger btn-xs" title="Borrar registro" data-toggle="tooltip"><i class="fa fa-trash"></i></a>
		                            	</td>
		                            </tr>
		                        	@endforeach
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
			</div>
		</div>
	</div>
</div>
<script>
    $("#formInsert").validate({
        errorClass: "text-danger font-italic font-weight-normal",
        // validClass: "text-success",
        rules: {
            nombre: "required",
            comentario: "required",
        },
        messages: {
            nombre: "Ingrese el nombre",
            comentario: "Ingrese el comentario",
        },
    });
</script>
<script>
	
	$( document ).ready(function() 
	{ 
		$('.select2').select2();
	});
	$('.limpiar').on('click',function(){
		$('.msj').css('display','none');
		$('#formInsert').attr('action','{{url('fileIe/shareFile')}}');
		$('#shareDocente').val(null).trigger('change');
		$('#nombre').val('');
		$('#file').val('');
		$('#video').val('');
		$('#comentario').val('');
	});
	$('.submit').on('click',function(){
		var dnis='-';
		for(var i=0;i<$('#shareDocente').val().length;i++)
		{
			dnis = dnis+$('#shareDocente').val()[i]+'-';
		}

		$('#listShared').val(dnis=='-'?'todos':dnis);
		console.log($('#listShared').val());
		$('#formInsert').submit();
	});
	$('.compartido').on('mouseover',function(){
		var shared=$(this).attr('data-share');
		var nombres = '';
		if(shared!='todos')
		{
			$('#shareDocente option').each(function(){
				if(shared.includes($(this).val()))
				{
					nombres = nombres + $(this).html()+', ';
				}
			});
		}
		else
		{
			nombres='TODOS';
		}
		$(this).attr('data-original-title',nombres);
	});
	$('.edit').on('click',function(){
		$('table>tbody').find('.selectedRow').removeClass('selectedRow');
		$(this).parent().parent().addClass('selectedRow');
        $(this).attr('data-id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/shareIe/shareFileEdit') }}",
            data: {idarchivoie:$(this).attr('data-id')},
            method: 'get',
            success: function(result){
                $('#formInsert').attr('action','{{url('shareIe/shareFileEdit')}}');
                $('#idarchivoie').val(result.data.idarchivoie);
				$('.msj').css('display','block');
				var cantShared=0;
				var docentesShare = [];
				$('#shareDocente option').each(function(){
                	var sharedDcte = result.data.compartido;
					if(sharedDcte!='todos')
					{
						if(sharedDcte.includes($(this).val()))
						{
							cantShared++;
							docentesShare.push($(this).val());
						}
					}
					else
					{
						cantShared++;
						docentesShare.push($(this).val());
					}
				});
				console.log(docentesShare);
				$('#shareDocente').val(docentesShare);
				$('#shareDocente').trigger('change');
				$('.cantShared').html(cantShared);
                $('#nombre').val(result.data.nombre);
                $('#video').val(result.data.video);
                $('#comentario').val(result.data.comentario);
            }
        });
    });
</script>
@endsection