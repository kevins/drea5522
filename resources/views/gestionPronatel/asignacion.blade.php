@extends('gestionPronatel/template')
@section('generalBody')
<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2 ui-sortable-handle" style="cursor: move;">
            <h3 class="card-title font-weight-bold">Datos de la Institucion educativa</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group mb-1">
                        <label for="inputEmail3" class="m-0">DRE</label>
                        <p class="m-0">APURIMAC</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group mb-1">
                        <label for="inputEmail3" class="m-0">UGEL</label>
                        <p class="m-0">{{$tie->ugelid}}</p>
                    </div>
                </div>
            </div>  
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group mb-1">
                        <label for="inputEmail3" class="m-0">REGION</label>
                        <p class="m-0">APURIMAC</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group mb-1">
                        <label for="inputEmail3" class="m-0">PROVINCIA</label>
                        <p class="m-0">APURIMAC</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group mb-1">
                        <label for="inputEmail3" class="m-0">DISTRITO</label>
                        <p class="m-0">APURIMAC</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group mb-1">
                        <label for="inputEmail3" class="m-0">INSTITUCION EDUCATIVA</label>
                        <p class="m-0">{{$tie->ie_nombre}}</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group mb-1">
                        <label for="inputEmail3" class="m-0">NIVEL</label>
                        <p class="m-0">{{$tie->nivel}}</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group mb-1">
                        <label for="inputEmail3" class="m-0">CODIGO MODULAR</label>
                        <p class="m-0">{{$tie->codigomodular}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Lista de materiales</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0 mt-2">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs navbar-toggleable-sm" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Estudinate</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Docente</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="button" class="btn btn-sm btn-success w-100" value="Agregar nuevo estudinate">
                                </div>
                                <div class="col-lg-12 mt-2">
                                    <table id="example1" class="table table-sm table-hover m-0 w-100">
                                        <thead class="text-center">
                                            <tr>
                                                <th>Nº</th>
                                                <th>Dni</th>
                                                <th>Codigo de estudinate</th>
                                                <th>Apellidos y nombres</th>
                                                <th>Tableta</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="text-center">
                                                <td>1</td>
                                                <td>12345678</td>
                                                <td>21212121221</td>
                                                <td>cjnasco csalcnaskn</td>
                                                <td><a href="#" data-toggle="modal" data-target="#exampleModal"><span class="badge badge-success p-2"><i class="fas fa-tablet-alt fa-2x"></i></span></a></td>
                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="button" class="btn btn-sm btn-success w-100" value="Agregar nuevo docente">
                                </div>
                                <div class="col-lg-12 mt-2">
                                    <table id="example2" class="table table-sm table-hover m-0 w-100">
                                        <thead class="text-center">
                                            <tr>
                                                <th>Nº</th>
                                                <th>Numero de documento</th>
                                                <th>Apellidos y nombres</th>
                                                <th>Tableta</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="text-center">
                                                <td>1</td>
                                                <td>12345678</td>
                                                <td>cjnasco csalcnaskn</td>
                                                <td><span class="badge badge-success p-2"><i class="fas fa-tablet-alt fa-2x"></i></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-4">
                        <table id="example3" class="table table-sm table-hover m-0 w-100">
                            <thead class="text-center">
                                <tr>
                                    <th>Nº</th>
                                    <th>Serie</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center">
                                    <td>1</td>
                                    <td>12345678</td>
                                    <td><span class="badge badge-success">Bueno</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('#example1').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );

        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
        $('#example2').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
    } );
   
    $(document).ready( function () {
        $('#example3').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );

        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
</script>
@endsection