<div class="col-lg-4 col-md-6">    
	<label class="m-0 font-italic font-weight-light">Código Modular:</label>    
	<strong>{{$ie->codigomodular}}</strong>    
</div>
<div class="col-lg-4 col-md-6">    
	<label class="m-0 font-italic font-weight-light">Nombre I.E.:</label>    
	<strong>{{$ie->ie_nombre}}</strong>   
</div>
<div class="col-lg-4 col-md-6">    
	<label class="m-0 font-italic font-weight-light">Ugel:</label>    
	<strong>{{$ie->tUgel_->ugel_nombre}}</strong>   
</div>
<div class="col-lg-4 col-md-6">    
	<label class="m-0 font-italic font-weight-light">Tipo de IE:</label>    
	<strong>{{$ie->tgestion}}</strong>   
</div>
<div class="col-lg-4 col-md-6">    
	<label class="m-0 font-italic font-weight-light">Provincia:</label>    
	<strong>{{$provincia}}</strong>   
</div>
<div class="col-lg-4 col-md-6">    
	<label class="m-0 font-italic font-weight-light">Distrito:</label>    
	<strong>{{$ie->tUbigeo_->ubigeo_nombre}}</strong>   
</div>
<div class="col-lg-4 col-md-6">    
	<label class="m-0 font-italic font-weight-light">Nivel:</label>    
	<strong>{{$ie->nivelm}}</strong>   
</div>