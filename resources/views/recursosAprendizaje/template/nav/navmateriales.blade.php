<!-- <li class="nav-item has-treeview">
    <a href="{{url('/')}}" class="nav-link">
        <i class="fas fa-home"></i>
        <p>INICIO</p>
    </a>
</li> -->
<li class="nav-item has-treeview">
    <a href="https://aulavirtual.dreapurimac.gob.pe/" class="nav-link">
        <i class="fas fa-vr-cardboard"></i>
        <p>AULA VIRTUAL</p>
    </a>
</li>
<!-- <li class="nav-item has-treeview">
    <a href="{{url('recursosAprendizaje/registrar')}}" class="nav-link">
        <i class="fas fa-briefcase"></i>
        <p>REGISTRAR MATERIALES</p>
    </a>
</li>

<li class="nav-item has-treeview">
    <a href="{{url('recursosAprendizaje/listar')}}" class="nav-link">
        <i class="fas fa-briefcase"></i>
        <p>LISTA DE MATERIALES</p>
    </a>
</li> -->
<li class="nav-item has-treeview menu-open">
    <a href="#" class="nav-link">
        <i class="fas fa-briefcase"></i>
        <p>EXPERIENCIAS<i class="right fas fa-angle-left"></i></p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item has-treeview">
            <a href="{{url('expAprIni/registrar')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Inicial</p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="{{url('expAprPri/registrar')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Primaria</p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="{{url('expAprSec/registrar')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Secundaria</p>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item has-treeview menu-open">
    <a href="#" class="nav-link">
        <i class="fas fa-briefcase"></i>
        <p>AUTOAPRENDIZAJE<i class="right fas fa-angle-left"></i></p>
    </a>
    <ul class="nav nav-treeview">
        <!-- <li class="nav-item has-treeview">
            <a href="{{url('recAprIni/registrar')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Inicial</p>
            </a>
        </li> -->
        <li class="nav-item has-treeview">
            <a href="{{url('recAprPri/registrar')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Primaria</p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="{{url('recAprSec/registrar')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Secundaria</p>
            </a>
        </li>
    </ul>
</li>

