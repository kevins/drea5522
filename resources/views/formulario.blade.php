<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>OTIC - DREA</title>
</head>
<body style="background-image: url('{{asset('imgFormulario/fondo.webp')}}')">
    <div class="container-fluid">
        <br><br>
        <img src="{{asset('imgFormulario/banner.webp')}}" class="w-100">
        <a href="{{url('formulario')}}">VOLVER AL INICIO DEL FORMULARIO</a>
        <p class="text-center mt-4 font-weight-bold" style="font-size: 1.3rem;">Señor Director, el presente formulario tiene por objetivo recoger información sobre diversos aspectos relacionados con conectividad de docentes y estudiantes de la institución educativa de su gestión para el acceso a aprendo en casa, así mismo recoger información sobre infraestructura tecnológica con la que cuenta su institución. Esperamos su colaboración brindándonos la información correcta a través de este cuestionario. Gracias.</p>
        <div class="alert alert-info">
            <h3 class="m-0 text-center font-weight-bold">El numero para consultas sobre el formulario es <strong>929 701 394</strong>, la atencion se realizara desde las 9am a 1pm</h3>
        </div>
    </div>
    <div class="container-fluid">
        <!-- style="display: none;" -->
        <div class="col-md-6 m-auto">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="cm" class="m-0">Codigo modular:</label>
                        <input type="text" name="cm" id="cm" class="form-control form-control-sm" maxlength="7">
                    </div>                                                      
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" style="visibility: hidden;" class="m-0">-</label>
                        <input type="button" value="BUSCAR" class="btn btn-primary btn-sm form-control form-control-sm buscar font-weight-bold">
                    </div>
                </div>  
            </div>
        </div>
        
    </div>
    <div class="container-fluid containerMensaje" style="display: none;">
        <div class="alert alert-warning mensaje text-center">
            <span class="text-uppercase"><strong>La ie ya se registró</strong>, los nuevos datos ingresados actualizaran la informaciòn, una vez realice click en el botón </span>
            <!-- los datos ingresados se sobrescribirá una vez realice click en el botón -->
            <span class="badge badge-success">REGISTRAR</span>
        </div>
    </div>
    <div class="container-fluid containerExist" style="display: none;">
        <div class="alert alert-warning mensaje text-center">
            <span>LA IE NO SE ENCONTRO</span>
        </div>
    </div>
    @if(Session::has('mensaje'))
    <div class="container-fluid containerExito">
        <div class="alert alert-success text-center">
            <p class="m-0">{{session()->get('mensaje')}}</p>
        </div>
    </div>
    @endif
    <div class="container-fluid formularioConteiner p-0" style="display: none;">
        <form id="form" action="{{url('registrar')}}" method="get">
            <input type="hidden" name="hcm" id="hcm" class="hcm">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header py-1 text-uppercase font-weight-bold text-center bg-info text-white">En caso de tener un (comentario / observacion <!-- / inconvenientes en el llenado del formulario -->) lo puede realizar en este recuadro</div>
                            <div class="card-body">
                                <p class="m-0 text-center">Si tiene un inconveniente en el llenado del formulario, colocar la primera palabra <strong>incidencia</strong>, para los otros casos ingresar comentario</p>
                                <div class="form-group">
                                    <!-- <label class="m-0" style="visibility: hidden;">cas</label> -->
                                    <textarea name="cam0" id="cam0" cols="30" rows="5" class="form-control form-control-sm" placeholder="incidencia, mi comentario"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="card mt-1">
                            <div class="card-header py-1 bg-info text-white text-uppercase">1.- Información sobre la IE (CENSO ESCOLAR 2020)</div>
                            <div class="card-body p-2">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Código modular:</strong><span class="a_cm"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Anexo:</strong><span class="a_anexo"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Código local:</strong><span class="a_cl"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Nombre de la IE:</strong><span class="a_nombreIe"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Nivel:</strong><span class="a_nivel"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Gestion:</strong><span class="a_gestion"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Director:</strong><span class="a_director"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Teléfono:</strong><span class="a_telefono"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Correo:</strong><span class="a_email"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Centro poblado:</strong><span class="a_cenPoblado"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Zona:</strong><span class="a_zona"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Departamento:</strong><span class="a_dep"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Provincia:</strong><span class="a_pro"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Distrito:</strong><span class="a_dis"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Estado de la IE:</strong><span class="a_estado"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Cant. alumnos:</strong><span class="a_alumno"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Cant. docentes:</strong><span class="a_docente"></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p class="m-0"><strong>Cant. secciones:</strong><span class="a_seccion"></span></p>
                                    </div>

                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                    <div class="col-lg-7">
                        <div class="card mt-1">
                            <div class="card-header py-1 bg-info text-white">2.- DATOS DE LA IE Y DEL DIRECTOR</div>
                            <div class="card-body p-2">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="cam21" class="m-0">Nombres:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="text" name="cam21" id="cam21" class="form-control form-control-sm">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="cam22" class="m-0">Apellidos:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="text" name="cam22" id="cam22" class="form-control form-control-sm">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="cam23" class="m-0">DNI:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="text" name="cam23" id="cam23" class="form-control form-control-sm" maxlength="8">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="cam24" class="m-0">Correo:</label>
                                            <input type="email" name="cam24" id="cam24" class="form-control form-control-sm" placeholder="name@example.com">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="cam25" class="m-0">Numero de teléfono:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="text" name="cam25" id="cam25" class="form-control form-control-sm" maxlength="9">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="m-0">Centro poblado o anexo:</label>
                                            <input type="text" name="cam26" id="cam26" class="form-control form-control-sm">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="" class="m-0">¿Cuántos estudiantes tiene la instituciòn por grado?:</label>
                                            <div class="form-control form-control-sm">
                                                <div class="row">
                                                    <div class="col-lg-2 px-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto1grado pr-0"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc46" id="camc46">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 px-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto2grado pr-0"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc47" id="camc47">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 px-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto3grado pr-0"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc48" id="camc48">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 px-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto4grado pr-0"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc49" id="camc49">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 px-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto5grado pr-0"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc410" id="camc410">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 px-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto6grado pr-0"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc411" id="camc411">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 px-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto7grado pr-0"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc412" id="camc412">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card mt-1">
                            <div class="card-header py-1 bg-info text-white">3.- EQUIPAMENTO Y CONECTIVIDAD CON QUE CUENTAN LOS DOCENTES</div>
                            <div class="card-body p-2">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="alert alert-info text-center font-weight-bold py-1" style="font-size:1.2rem;">CONECTIVIDAD</div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="m-0">¿Con cuántos docentes cuenta la IE en el 2021?:</label>
                                            <input type="number" name="camc31" id="camc31" class="form-control form-control-sm text-center">
                                        </div>
                                    </div>
                                    <!-- 5 -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="m-0">¿Cuántos docentes tienen señal de internet en la zona donde viven?:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="number" name="camc32" id="camc32" class="form-control form-control-sm text-center">
                                        </div>
                                    </div>
                                    <!-- 4 -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="camc33" class="m-0">¿Cuántos docentes cuentan con plan de datos limitado?:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="number" name="camc33" id="camc33" class="form-control form-control-sm text-center">
                                        </div>
                                    </div>

                                    <!-- 2 -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="camc34" class="m-0">¿Cuántos docentes viven en el poblado donde está la IE?:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="number" name="camc34" id="camc34" class="form-control form-control-sm text-center">
                                        </div>
                                    </div>
                                    <!-- 3 -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="camc35" class="m-0">¿Cuántos docentes cuentan con internet ilimitado?:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="number" name="camc35" id="camc35" class="form-control form-control-sm text-center">
                                        </div>
                                    </div>
                                    <!-- 6 -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="camc36" class="m-0">¿Cuántos docentes recargan datos?:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="number" name="camc36" id="camc36" class="form-control form-control-sm text-center">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="alert alert-info text-center font-weight-bold py-1" style="font-size:1.2rem;">EQUIPAMENTO</div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="m-0">¿Cuántos docentes tienen ...?:</label>
                                            <div class="form-control form-control-sm px-3">
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <p class="mb-1 text-center">DISPOSITIVOS</p>
                                                        <p><span class="badge badge-info">A</span>-smartphone</p>
                                                        <p><span class="badge badge-info">B</span>-tabletas</p>
                                                        <p><span class="badge badge-info">C</span>-radio</p>
                                                        <p><span class="badge badge-info">D</span>-televisor</p>
                                                        <p><span class="badge badge-info">E</span>-laptop o computadora</p>
                                                        <p><span class="badge badge-info">F</span>-celular basico</p>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <p class="mb-1 text-center">1 DISPOSITIVO</p>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">A</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came31" id="came31">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">B</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came32" id="came32">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">C</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came33" id="came33">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">D</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came34" id="came34">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">E</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came35" id="came35">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">F</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came36" id="came36">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <p class="mb-1 text-center">2 DISPOSITIVOS</p>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A y B</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came37" id="came37">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A y C</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came38" id="came38">
                                                                </div>
                                                            </div>  
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A y D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came39" id="came39">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A y E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came310" id="came310">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A y F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came311" id="came311">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B y C</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came312" id="came312">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B y D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came313" id="came313">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B y E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came314" id="came314">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B y F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came315" id="came315">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">C y D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came316" id="came316">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">C y E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came317" id="came317">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">C y F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came318" id="came318">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">D y E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came319" id="came319">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">D y F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came320" id="came320">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">E y F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came321" id="came321">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <p class="mb-1 text-center">3 DISPOSITIVOS</p>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,B,C</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came322" id="came322">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,B,D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came323" id="came323">
                                                                </div>
                                                            </div>  
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,B,E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came324" id="came324">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,B,F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came325" id="came325">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,C,D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came326" id="came326">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,C,E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came327" id="came327">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,C,F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came328" id="came328">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B,C,D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came329" id="came329">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B,C,E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came330" id="came330">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B,C,F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came331" id="came331">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,D,B</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came332" id="came332">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,D,E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came333" id="came333">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,D,F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came334" id="came334">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <hr class="m-2">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Ningun dispositivo</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came335" id="came335">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">4 a mas dispositivos mencionados anteriormente</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came336" id="came336">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card mt-1">
                            <div class="card-header py-1 bg-info text-white">4.- EQUIPAMIENTO Y CONECTIVIDAD CON QUE CUENTAN LOS ESTUDIANTES</div>
                            <div class="card-body p-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="alert alert-info text-center font-weight-bold py-1" style="font-size:1.2rem;">CONECTIVIDAD</div>
                                    </div>
                                    <!-- 5 -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="camc41" class="m-0">¿Cuántos alumnos tienen señal de internet en la zona donde viven?:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="number" name="camc41" id="camc41" class="form-control form-control-sm text-center">
                                        </div>
                                    </div>
                                    <!-- 4 -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="camc42" class="m-0">¿Cuántos alumnos cuentan con plan de datos limitado?:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="number" name="camc42" id="camc42" class="form-control form-control-sm text-center">
                                        </div>
                                    </div>

                                    <!-- 2 -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="camc43" class="m-0">¿Cuántos alumnos viven en zona rural?:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="number" name="camc43" id="camc43" class="form-control form-control-sm text-center">
                                        </div>
                                    </div>
                                    <!-- 3 -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="camc44" class="m-0">¿Cuántos alumnos cuentan con internet ilimitado?:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="number" name="camc44" id="camc44" class="form-control form-control-sm text-center">
                                        </div>
                                    </div>
                                    <!-- 6 -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="camc45" class="m-0">¿Cuántos alumnos recargan datos?:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <input type="number" name="camc45" id="camc45" class="form-control form-control-sm text-center">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <hr class="m-2">
                                    </div>
                                    <!-- <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="" class="m-0">¿Cuántos alumnos tienen por grado o edad?:</label>
                                            <div class="form-control form-control-sm">
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto1grado"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc46" id="camc46">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto2grado"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc47" id="camc47">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto3grado"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc48" id="camc48">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto4grado"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc49" id="camc49">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto5grado"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc410" id="camc410">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto6grado"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc411" id="camc411">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text texto7grado"></span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="camc412" id="camc412">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-lg-12">
                                        <div class="alert alert-info text-center font-weight-bold py-1" style="font-size:1.2rem;">EQUIPAMENTO</div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="m-0">¿Cuántos alumnos tienen ...?:</label>
                                            <div class="form-control form-control-sm px-3">
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <p class="mb-1 text-center">DISPOSITIVOS</p>
                                                        <p><span class="badge badge-info">A</span>-smartphone</p>
                                                        <p><span class="badge badge-info">B</span>-tabletas</p>
                                                        <p><span class="badge badge-info">C</span>-radio</p>
                                                        <p><span class="badge badge-info">D</span>-televisor</p>
                                                        <p><span class="badge badge-info">E</span>-laptop o computadora</p>
                                                        <p><span class="badge badge-info">F</span>-celular basico</p>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <p class="mb-1 text-center">1 DISPOSITIVO</p>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">A</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came41" id="came41">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">B</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came42" id="came42">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">C</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came43" id="came43">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">D</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came44" id="came44">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">E</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came45" id="came45">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">F</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came46" id="came46">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <p class="mb-1 text-center">2 DISPOSITIVOS</p>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A y B</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came47" id="came47">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A y C</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came48" id="came48">
                                                                </div>
                                                            </div>  
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A y D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came49" id="came49">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A y E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came410" id="came410">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A y F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came411" id="came411">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B y C</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came412" id="came412">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B y D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came413" id="came413">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B y E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came414" id="came414">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B y F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came415" id="came415">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">C y D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came416" id="came416">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">C y E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came417" id="came417">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">C y F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came418" id="came418">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">D y E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came419" id="came419">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">D y F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came420" id="came420">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">E y F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came421" id="came421">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <p class="mb-1 text-center">3 DISPOSITIVOS</p>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,B,C</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came422" id="came422">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,B,D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came423" id="came423">
                                                                </div>
                                                            </div>  
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,B,E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came424" id="came424">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,B,F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came425" id="came425">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,C,D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came426" id="came426">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,C,E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came427" id="came427">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,C,F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came428" id="came428">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B,C,D</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came429" id="came429">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B,C,E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came430" id="came430">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">B,C,F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came431" id="came431">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,D,B</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came432" id="came432">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,D,E</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came433" id="came433">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="input-group input-group-sm mb-1">
                                                                    <div class="input-group-prepend"><span class="input-group-text">A,D,F</span></div>
                                                                    <input type="number" class="form-control form-control-sm text-center" value="0" name="came434" id="came434">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <hr class="m-2">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Ningun dispositivo</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came435" id="came435">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">4 a mas dispositivos mencionados anteriormente</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="came436" id="came436">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card mt-1">
                            <div class="card-header py-1 bg-info text-white text-uppercase">5.- DOTACION DE TABLETAS Y LAPTOP PARA ESTUDIANTES DE LA IE</div>
                            <div class="card-body p-2">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="cam51" class="m-0">¿Cuántas tabletas recibiò de pronatel en total?:</label>
                                            <input type="number" name="cam51" id="cam51" class="form-control form-control-sm text-center" >
                                        </div>
                                        <div class="form-group">
                                            <label for="cam52" class="m-0">¿Cuántas tabletas de pronatel estan dañadas?:</label>
                                            <input type="number" name="cam52" id="cam52" class="form-control form-control-sm text-center" >
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="cam53" class="m-0">¿Cuántas tabletas recibiò del DS-006-2020 en total?:</label>
                                            <input type="number" name="cam53" id="cam53" class="form-control form-control-sm text-center" >
                                        </div>
                                        <div class="form-group">
                                            <label for="cam54" class="m-0">¿Cuántas tabletas del DS-006-2020 estan dañadas?:</label>
                                            <input type="number" name="cam54" id="cam54" class="form-control form-control-sm text-center" >
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="cam55" class="m-0">¿Cuántas tabletas recibiò de alguna donacion u otra fuente en total?:</label>
                                            <input type="number" name="cam55" id="cam55" class="form-control form-control-sm text-center" >
                                        </div>
                                        <div class="form-group">
                                            <label for="cam56" class="m-0">¿Cuántas tabletas ya sea de alguna donacion estan dañadas?:</label>
                                            <input type="number" name="cam56" id="cam56" class="form-control form-control-sm text-center" >
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="cam57" class="m-0">¿Cuántas laptop recibiò de alguna donacion o proyecto?:</label>
                                            <input type="number" name="cam57" id="cam57" class="form-control form-control-sm text-center" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card mt-1">
                            <div class="card-header py-1 bg-info text-white text-uppercase">6.- DESARROLLO DE LA educación A DISTANCIA</div>
                            <div class="card-body p-2">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="" class="m-0">¿Cuántos estudiantes acceden por los diferentes medios a la educación a distancia?:</label>
                                            <div class="form-control form-control-sm">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <p class="mb-1 text-center">MEDIOS</p>
                                                        <p><span class="badge badge-info">A</span>-Aprendo en casa web</p>
                                                        <p><span class="badge badge-info">B</span>-Aprendo en casa TV</p>
                                                        <p><span class="badge badge-info">C</span>-Aprendo en casa radio</p>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <p class="mb-1 text-center">1 MEDIO</p>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">A</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="cam61" id="cam61">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">B</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="cam62" id="cam62">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">C</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="cam63" id="cam63">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <p class="mb-1 text-center">2 MEDIOS</p>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">A y B</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="cam64" id="cam64">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">A y C</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="cam65" id="cam65">
                                                        </div>
                                                        <div class="input-group input-group-sm mb-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">B y C</span>
                                                            </div>
                                                            <input type="number" class="form-control form-control-sm text-center" value="0" name="cam66" id="cam66">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam67" class="col-sm-9 col-form-label">Fichas complementarias</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm text-center" id="cam67" name="cam67">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam68" class="col-sm-9 col-form-label">Videollamadas (zoom, meet, jitsi, teams, webex, etc.)</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm text-center" id="cam68" name="cam68">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam622n" class="col-sm-9 col-form-label">Classroom</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm text-center" id="cam622n" name="cam622n" value="0">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam623n" class="col-sm-9 col-form-label">Moodle</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm text-center" id="cam623n" name="cam623n" value="0">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam69" class="col-sm-9 col-form-label">Otros medios</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm text-center" id="cam69" name="cam69">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam610" class="col-sm-9 col-form-label">No accede</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm text-center" id="cam610" name="cam610">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="m-0">¿Cuántos docentes se comunican con los estudiantes?:

                                            <!-- ¿Con què frecuencia se comunica el docente con los estudiantes?: -->
                                            </label>
                                            <div class="form-control form-control-sm">
                                                <div class="form-group row mb-0">
                                                    <label for="cam611" class="col-sm-9 col-form-label">Diario</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="cam611" name="cam611">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam612" class="col-sm-9 col-form-label">Una vez por semana</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="cam612" name="cam612">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam613" class="col-sm-9 col-form-label">Cada 15 dias</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="cam613" name="cam613">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam614" class="col-sm-9 col-form-label">Una vez al mes</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="cam614" name="cam614">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam615" class="col-sm-9 col-form-label">No se comunica</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="cam615" name="cam615">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="" class="m-0">
                                                ¿Cuántos estudiantes envian sus evidencias, segun los siguientes medios?:
                                            <!-- ¿Cómo entregan sus evidencias los estudiantes?: -->
                                            </label>
                                            <div class="form-control form-control-sm">
                                                <div class="form-group row mb-0">
                                                    <label for="cam616" class="col-sm-9 col-form-label">Impresos por sobre</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="cam616" name="cam616">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam617" class="col-sm-9 col-form-label">Fotos via wassap</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="cam617" name="cam617">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam618" class="col-sm-9 col-form-label">Por anydesk o teamweber</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="cam618" name="cam618">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam619" class="col-sm-9 col-form-label">Correo electronico</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="cam619" name="cam619">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam620" class="col-sm-9 col-form-label">Google drive</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="cam620" name="cam620">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="cam621" class="col-sm-2 col-form-label" style="font-size: .75rem;">Otro</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control form-control-sm" id="cam621" name="cam621" placeholder="Mencione el medio">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card mt-1">
                            <div class="card-header py-1 bg-info text-white">7.- SERVICIO DE INTERNET EN LA IE</div>
                            <div class="card-body p-2">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="" class="m-0">Con què servicios de internet cuenta:</label>
                                            <p class="m-0">Realice click para seleccionar el tipo de internet, en caso de no encontrar su proveedor mencionar en la casilla "OTRO" .</p>
                                            <div class="form-control form-control-sm">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" id="cam71" name="cam71" value="si">
                                                    <label class="form-check-label" for="cam71">Contrato N° 012-2019-MINEDU/SG-OGA-OL (VIETTEL PERU)</label>
                                                    <hr class="m-0">
                                                    <input class="form-check-input" type="checkbox" id="cam72" name="cam72" value="si">
                                                    <label class="form-check-label" for="cam72">Plataforma Satelital del MINEDU (VSAT)</label>
                                                    <hr class="m-0">
                                                    <input class="form-check-input" type="checkbox" id="cam73" name="cam73" value="si">
                                                    <label class="form-check-label" for="cam73">Contrato de Concesión VIETTEL PERU SAC-MTC</label>
                                                    <hr class="m-0">
                                                    <input class="form-check-input" type="checkbox" id="cam74" name="cam74" value="si">
                                                    <label class="form-check-label" for="cam74">Contrato de Concesión TELEFÓNICA DEL PERU S.A.A.-MTC</label>
                                                    <hr class="m-0">
                                                    <input class="form-check-input" type="checkbox" id="cam75" name="cam75" value="si">
                                                    <label class="form-check-label" for="cam75">Gilat: red dorsal de fibra optica</label>
                                                    <hr class="mt-0 mb-2">
                                                    <div class="form-group row">
                                                        <label for="cam76" class="col-sm-2 col-form-label">Otro:</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control form-control-sm" id="cam76" name="cam76" placeholder="Mencione el proovedor de internet">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="m-0">Con cuàntos megabyte(MB o velocidad) cuenta el servicio de internet:</label>
                                            <p class="m-0">Según el servicio de internet seleccionado, llenar en las casillas la cantidad o velocidad de internet que recibe.</p>
                                            <div class="form-control form-control-sm">
                                                <div class="form-group row mb-0">
                                                    <label for="cam77" class="col-sm-10 col-form-label">Contrato N° 012-2019-MINEDU/SG-OGA-OL (VIETTEL PERU):</label>
                                                    <div class="col-sm-2">
                                                        <input type="number" class="form-control form-control-sm" id="cam77" name="cam77">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam78" class="col-sm-10 col-form-label">Plataforma Satelital del MINEDU (VSAT):</label>
                                                    <div class="col-sm-2">
                                                        <input type="number" class="form-control form-control-sm" id="cam78" name="cam78">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam79" class="col-sm-10 col-form-label">Contrato de Concesión VIETTEL PERU SAC-MTC:</label>
                                                    <div class="col-sm-2">
                                                        <input type="number" class="form-control form-control-sm" id="cam79" name="cam79">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam710" class="col-sm-10 col-form-label">Contrato de Concesión TELEFÓNICA DEL PERU S.A.A.-MTC:</label>
                                                    <div class="col-sm-2">
                                                        <input type="number" class="form-control form-control-sm" id="cam710" name="cam710">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam711" class="col-sm-10 col-form-label">Gilat: red dorsal de fibra optica:</label>
                                                    <div class="col-sm-2">
                                                        <input type="number" class="form-control form-control-sm" id="cam711" name="cam711">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="cam712" class="col-sm-10 col-form-label">Otro:</label>
                                                    <div class="col-sm-2">
                                                        <input type="number" class="form-control form-control-sm" id="cam712" name="cam712">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0">
                                            <label for="cam713" class="col-sm-10 col-form-label">Días al mes que cuenta con internet:<!-- <span class="badge badge-danger">*</span> --></label>
                                            <div class="col-sm-2">
                                                <input type="number" class="form-control form-control-sm" id="cam713" name="cam713">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid mt-1">
                <div class="card">
                    <div class="card-header py-1 bg-info text-white text-uppercase">8.- información SOBRE LA INFRAESTRUCTURA tecnológica</div>
                    <div class="card-body p-2">
                        <div class="alert alert-info text-center font-weight-bold py-1" style="font-size:1.3rem;">Lea en detalle cada uno de los conceptos </div>
                        <!-- para completar los datos siguientes: -->
                        <ul>
                            <li><strong><span class="font-weight-bold badge badge-warning">1</span> IMG de referencia:</strong> Realizar click para mostrar imagen referencial del dispositivo. </li>
                            <li><strong><span class="font-weight-bold badge badge-warning">2</span> Formulario:</strong> En caso de contar con el dispositivo, realizar click para abrir un formulario de google en donde se subirán imágenes y datos del dispositivo mencionado.</li>
                            <li><strong><span class="font-weight-bold badge badge-warning">3</span> En uso:</strong> Equipos que están siendo usados.<span class="badge badge-info">CANTIDAD</span></li>
                            <li><strong><span class="font-weight-bold badge badge-warning">4</span> Operativos: </strong>Equipos que están en buenas condiciones, pero están sin usar.<span class="badge badge-info">CANTIDAD</span></li>
                            <li><strong><span class="font-weight-bold badge badge-warning">5</span> Dañadas: </strong>Equipos que no están operativas para su uso.<span class="badge badge-info">CANTIDAD</span></li>
                            <li><strong><span class="font-weight-bold badge badge-warning">6</span> Guardadas: </strong>Equipos que están en buenas condiciones, pero por problemas con el proyecto que lo implementò no se puede usar.<span class="badge badge-info">CANTIDAD</span></li>
                            <li><strong><span class="font-weight-bold badge badge-warning">7</span> Mantenimiento: </strong>Mencionar el mantenimiento que necesita el equipo.<span class="badge badge-info">TEXTO</span></li>
                            <li><strong><span class="font-weight-bold badge badge-warning">8</span> Comentario/Obs.:</strong> Comentario/observación referente al dispositivo mencionado.<span class="badge badge-info">TEXTO</span></li>
                        </ul>
                        <p><strong>En caso de que la IE no cuente con alguno de estos dispositivos, dejar en blanco.</strong></p>
                        <div class="alert alert-info text-center font-weight-bold py-1" style="font-size:1.3rem;">Una vez revisado los conceptos anteriores, completar los siguientes datos</div>
                        <hr>
                        <div class="row px-2">
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Servidores <a href="#" data-toggle="modal" data-target=".modal-servidores"><span class="font-weight-bold badge badge-warning">1</span>:(IMG de referencia)</a> | <a href="https://forms.gle/t3iwZ8WiTYDcdbdb8" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="serEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántos servidores están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="serEnU" name="serEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="serFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántos servidores están operativos?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="serFun" name="serFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="serDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántos servidores están dañados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="serDan" name="serDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="serGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántos servidores están guardados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="serGua" name="serGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántos servidores están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="serMan" name="serMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre el servidor(es)?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="serCom" name="serCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Router <a href="#" data-toggle="modal" data-target=".modal-router"><span class="font-weight-bold badge badge-warning">1</span>(IMG de referencia)</a> | <a href="https://forms.gle/SnHhTJmRrnx3XEJy7" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <!-- <div class="form-control form-control-sm col-lg-2 text-center"><a href="https://forms.gle/SnHhTJmRrnx3XEJy7" class="text-center" target="_blank">Formulario</a></div> -->
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="rouEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántos router están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="rouEnU" name="rouEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="rouFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántos router están operativos?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="rouFun" name="rouFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="rouDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántos router están dañados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="rouDan" name="rouDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="rouGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántos router están guardados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="rouGua" name="rouGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántos router están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="rouMan" name="rouMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre el router(s)?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="rouCom" name="rouCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Switch <a href="#" data-toggle="modal" data-target=".modal-switch"><span class="font-weight-bold badge badge-warning">1</span>(IMG de referencia)</a> | <a href="https://forms.gle/nYUaXZoZY4P68waT9" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <!-- <div class="form-control form-control-sm col-lg-2 text-center"><a href="https://forms.gle/nYUaXZoZY4P68waT9" class="text-center" target="_blank">Formulario</a></div> -->
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="swiEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántos switch están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="swiEnU" name="swiEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="swiFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántos switch están operativos?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="swiFun" name="swiFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="swiDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántos switch están dañados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="swiDan" name="swiDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="swiGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántos switch están guardados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="swiGua" name="swiGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántos switch están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="swiMan" name="swiMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre el switch(s)?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="swiCom" name="swiCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Access point en aulas <a href="#" data-toggle="modal" data-target=".modal-apa"><span class="font-weight-bold badge badge-warning">1</span>(IMG de referencia)</a> | <a href="https://forms.gle/AumqVM74Y7VpAH8w8" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="apaEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántos access point en aulas están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="apaEnU" name="apaEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="apaFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántos access point en aulas están operativos?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="apaFun" name="apaFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="apaDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántos access point en aulas están dañados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="apaDan" name="apaDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="apaGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántos access point en aulas están guardados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="apaGua" name="apaGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántos access point en aulas están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="apaMan" name="apaMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre access point en aulas?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="apaCom" name="apaCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Access point en exteriores <a href="#" data-toggle="modal" data-target=".modal-ape"><span class="font-weight-bold badge badge-warning">1</span>(IMG de referencia)</a> | <a href="https://forms.gle/miD8C5Xd77nRepGE9" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="apeEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántos access point en exteriores están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="apeEnU" name="apeEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="apeFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántos access point en exteriores están operativos?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="apeFun" name="apeFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="apeDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántos access point en exteriores están dañados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="apeDan" name="apeDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="apeGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántos access point en exteriores están guardados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="apeGua" name="apeGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántos access point en exteriores están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="apeMan" name="apeMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre access point en exteriores?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="apeCom" name="apeCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Laptops | <a href="https://forms.gle/7VUAPwiecUHqjXtx8" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="lapEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántas laptops están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="lapEnU" name="lapEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="lapFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántas laptops están operativos?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="lapFun" name="lapFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="lapDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántas laptops están dañadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="lapDan" name="lapDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="lapGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántas laptops están guardadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="lapGua" name="lapGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántas laptops están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="lapMan" name="lapMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre las laptops?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="lapCom" name="lapCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Computadoras | <a href="https://forms.gle/7mmHnezpmvBpzuaeA" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="comEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántas computadoras están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="comEnU" name="comEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="comFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántas computadoras están operativos?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="comFun" name="comFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="comDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántas computadoras están dañadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="comDan" name="comDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="comGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántas computadoras están guardadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="comGua" name="comGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántas computadoras están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="comMan" name="comMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre las computadoras?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="comCom" name="comCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Proyectores <a href="#" data-toggle="modal" data-target=".modal-pro"><span class="font-weight-bold badge badge-warning">1</span>(IMG de referencia)</a> | <a href="https://forms.gle/uUkDVLv2L2G9n3hF6" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="proEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántos proyectores están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="proEnU" name="proEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="proFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántos proyectores están operativos?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="proFun" name="proFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="proDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántos proyectores están dañados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="proDan" name="proDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="proGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántos proyectores están guardados?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="proGua" name="proGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántos proyectores están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="proMan" name="proMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre los proyectores?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="proCom" name="proCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Smarth TV <a href="#" data-toggle="modal" data-target=".modal-sma"><span class="font-weight-bold badge badge-warning">1</span>(IMG de referencia)</a> | <a href="https://forms.gle/Y3GzhE75wJcEfsXq5" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="smaEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántas smarth tv están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="smaEnU" name="smaEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="smaFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántas smarth tv están operativas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="smaFun" name="smaFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="smaDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántas smarth tv están dañadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="smaDan" name="smaDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="smaGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántas smarth tv están guardadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="smaGua" name="smaGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántas smarth tv están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="smaMan" name="smaMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre el/los smarth tv?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="smaCom" name="smaCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Pizarras interactivas <a href="#" data-toggle="modal" data-target=".modal-pii"><span class="font-weight-bold badge badge-warning">1</span>(IMG de referencia)</a> | <a href="https://forms.gle/LtomWh3yiXVZkcEF7" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="piiEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántas pizarras interactivas están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="piiEnU" name="piiEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="piiFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántas pizarras interactivas están operativas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="piiFun" name="piiFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="piiDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántas pizarras interactivas están dañadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="piiDan" name="piiDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="piiGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántas pizarras interactivas están guardadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="piiGua" name="piiGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántas pizarras interactivas están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="piiMan" name="piiMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre la/las pizarras interactivas?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="piiCom" name="piiCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Pararayos <a href="#" data-toggle="modal" data-target=".modal-par"><span class="font-weight-bold badge badge-warning">1</span>(IMG de referencia)</a> | <a href="https://forms.gle/wRdzg7UjZBG4JQab7" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="parEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántos pararayos están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="parEnU" name="parEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="parFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántos pararayos están operativos?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="parFun" name="parFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="parDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántos pararayos están dañadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="parDan" name="parDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="parGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántos pararayos están guardadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="parGua" name="parGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántos pararayos están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="parMan" name="parMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre el/los pararayos?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="parCom" name="parCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 p-0">
                                <div class="form-group">
                                    <label class="m-0">Pozo a tierra <a href="#" data-toggle="modal" data-target=".modal-pot"><span class="font-weight-bold badge badge-warning">1</span>(IMG de referencia)</a> | <a href="https://forms.gle/ADpRaR78YAa8hHKx7" class="text-center" target="_blank"><span class="font-weight-bold badge badge-warning">2</span>-Formulario</a>:</label>
                                    <div class="form-control form-control-sm">
                                        <div class="row p-2">
                                            <div class="col-lg-12"><p class="m-0 font-weight-bold">Complete estos datos:</p></div>
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="potEnU" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">3</span>-¿Cuántos pozo a tierra están en uso?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="potEnU" name="potEnU">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="potFun" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">4</span>-¿Cuántos pozo a tierra están operativos?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="potFun" name="potFun">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-lg-6">
                                                <div class="form-group row mb-0">
                                                    <label for="potDan" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">5</span>-¿Cuántos pozo a tierra están dañadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="potDan" name="potDan">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <label for="potGua" class="col-sm-9 col-form-label"><span class="font-weight-bold badge badge-warning">6</span>-¿Cuántos pozo a tierra están guardadas?</label>
                                                    <div class="col-sm-3">
                                                        <input type="number" class="form-control form-control-sm" id="potGua" name="potGua">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">7</span>-¿Cuántos pozo a tierra están en mantenimiento?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="potMan" name="potMan">
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="m-0"><span class="font-weight-bold badge badge-warning">8</span>-¿Comentario/Observación sobre el/los pozo a tierra?</label>
                                                <input type="text" class="form-control form-control-sm col-lg-12" id="potCom" name="potCom">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container fluid">
                <input type="button" value="REGISTRAR" class="btn btn-sm col-lg-12 btn-success registrar">
            </div>
        </form>
        
    </div>

    
<!-- ---------------------------------------------------- -->
    <div class="modal fade modal-servidores" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
            <div class="modal-content">
                <div class="modal-header p-2">
                    <h5 class="modal-title">Imagenes de referencia de servidores: Los servidores se encuentran dentro de los gabinetes</h5>
                    <span>Presione (esc) para salir</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <p class="m-0 badge badge-success">GABINETES</p>
                    <img src="{{asset('imgFormulario/gabinetes.webp')}}" class="w-100">
                    <p class="m-0 badge badge-success">SERVIDORES</p>
                    <img src="{{asset('imgFormulario/servidores.webp')}}" class="w-100">
                </div>
                <div class="modal-footer p-2">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">cerrar</button>
                </div>
            </div>
        </div>
    </div>
<!-- ---------------------------------------------------- -->
    <div class="modal fade modal-router" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
            <div class="modal-content">
                <div class="modal-header p-2">
                    <h5 class="modal-title">Imagenes de referencia de router: </h5>
                    <span>Presione (esc) para salir</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <p class="m-0 badge badge-success">ROUTER</p>
                    <img src="{{asset('imgFormulario/router.webp')}}" class="w-100">
                </div>
                <div class="modal-footer p-2">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">cerrar</button>
                </div>
            </div>
        </div>
    </div>
<!-- ---------------------------------------------------- -->
    <div class="modal fade modal-switch" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
            <div class="modal-content">
                <div class="modal-header p-2">
                    <h5 class="modal-title">Imagenes de referencia de switch: </h5>
                    <span>Presione (esc) para salir</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <p class="m-0 badge badge-success">SWITCH</p>
                    <img src="{{asset('imgFormulario/switch.webp')}}" class="w-100">
                </div>
                <div class="modal-footer p-2">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">cerrar</button>
                </div>
            </div>
        </div>
    </div>
<!-- ---------------------------------------------------- -->
    <div class="modal fade modal-apa" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
            <div class="modal-content">
                <div class="modal-header p-2">
                    <h5 class="modal-title">Imagenes de referencia de access point: para aulas </h5>
                    <span>Presione (esc) para salir</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <p class="m-0">Se encuentra instaladas dentro de las aulas</p>
                    <p class="m-0 badge badge-success">ACCESS POINT</p>
                    <img src="{{asset('imgFormulario/ap.webp')}}" class="w-100">
                </div>
                <div class="modal-footer p-2">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">cerrar</button>
                </div>
            </div>
        </div>
    </div>
<!-- ---------------------------------------------------- -->
    <div class="modal fade modal-ape" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
            <div class="modal-content">
                <div class="modal-header p-2">
                    <h5 class="modal-title">Imagenes de referencia de access point: para exteriores </h5>
                    <span>Presione (esc) para salir</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <p class="m-0">Se encuentran instaladas en el exterior para poder liberar el internet e intranet</p>
                    <p class="m-0 badge badge-success">ACCESS POINT</p>
                    <img src="{{asset('imgFormulario/ap.webp')}}" class="w-100">
                </div>
                <div class="modal-footer p-2">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">cerrar</button>
                </div>
            </div>
        </div>
    </div>
<!-- ---------------------------------------------------- -->
    <div class="modal fade modal-pro" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
            <div class="modal-content">
                <div class="modal-header p-2">
                    <h5 class="modal-title">Imagenes de referencia de proyectores: </h5>
                    <span>Presione (esc) para salir</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <p class="m-0 badge badge-success">PROYECTORES</p>
                    <img src="{{asset('imgFormulario/proyectores.webp')}}" class="w-100">
                </div>
                <div class="modal-footer p-2">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">cerrar</button>
                </div>
            </div>
        </div>
    </div>
<!-- ---------------------------------------------------- -->
    <div class="modal fade modal-sma" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
            <div class="modal-content">
                <div class="modal-header p-2">
                    <h5 class="modal-title">Imagenes de referencia de smart TV: </h5>
                    <span>Presione (esc) para salir</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <p class="m-0 badge badge-success">SMARTH TV</p>
                    <img src="{{asset('imgFormulario/smtv.webp')}}" class="w-100">
                </div>
                <div class="modal-footer p-2">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">cerrar</button>
                </div>
            </div>
        </div>
    </div>
<!-- ---------------------------------------------------- -->
    <div class="modal fade modal-pii" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
            <div class="modal-content">
                <div class="modal-header p-2">
                    <h5 class="modal-title">Imagenes de referencia de pizarras interactivas: </h5>
                    <span>Presione (esc) para salir</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <p class="m-0 badge badge-success">PIZARRAS INTERACTIVAS</p>
                    <img src="{{asset('imgFormulario/pantalla.webp')}}" class="w-100">
                </div>
                <div class="modal-footer p-2">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">cerrar</button>
                </div>
            </div>
        </div>
    </div>
<!-- ---------------------------------------------------- -->
    <div class="modal fade modal-par" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
            <div class="modal-content">
                <div class="modal-header p-2">
                    <h5 class="modal-title">Imagenes de referencia de pararayos: </h5>
                    <span>Presione (esc) para salir</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <p class="m-0 badge badge-success">PARARAYOS</p>
                    <img src="{{asset('imgFormulario/pararayos.webp')}}" class="w-100">
                </div>
                <div class="modal-footer p-2">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">cerrar</button>
                </div>
            </div>
        </div>
    </div>
<!-- ---------------------------------------------------- -->
    <div class="modal fade modal-pot" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
            <div class="modal-content">
                <div class="modal-header p-2">
                    <h5 class="modal-title">Imagenes de referencia de pozo a tierra: </h5>
                    <span>Presione (esc) para salir</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <p class="m-0 badge badge-success">POZO A TIERRA</p>
                    <img src="{{asset('imgFormulario/pozo.webp')}}" class="w-100">
                </div>
                <div class="modal-footer p-2">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
    <script src="{{asset('admin3/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('plugin/adminlte/plugins/jquery/jquery.min.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script>
        function segunNivel(nivel)
        {
            var banGrado = nivel.includes('Inicial');
            $('.texto1grado').html(banGrado?'0 años':'1 grado');
            $('.texto2grado').html(banGrado?'1 años':'2 grado');
            $('.texto3grado').html(banGrado?'2 años':'3 grado');
            $('.texto4grado').html(banGrado?'3 años':'4 grado');
            $('.texto5grado').html(banGrado?'4 años':'5 grado');
            if(nivel.includes('Inicial'))
            {
                $('.texto6grado').html('5 años');
                $('.texto7grado').html('6 años');
                $('.texto6grado').parent().parent().find('input').prop('disabled',false);
                $('.texto7grado').parent().parent().find('input').prop('disabled',false);
            }
            if(nivel.includes('Primaria'))
            {
                $('.texto6grado').html('6 grado');
                $('.texto6grado').parent().parent().find('input').prop('disabled',false);
                // $('.texto7grado').parent().parent().css('display','none');
                $('.texto7grado').parent().parent().find('input').prop('disabled',true);
            }
            if(nivel.includes('Secundaria'))
            {
                $('.texto6grado').parent().parent().find('input').prop('disabled',true);
                $('.texto7grado').parent().parent().find('input').prop('disabled',true);
            }
        }

        $('.registrar').on('click',function(){
            $('#form').submit();
            // if($('#cam21').val()!='' &&
            // $('#cam22').val()!='' &&
            // $('#cam23').val()!='' &&
            // $('#cam25').val()!='' &&
            // $('#camc43').val()!='' &&
            // $('#cam713').val()!='')
            // {
            //     $('#form').submit();
            // }
            // else
            // {
            //     alert('Es necesario q ingrese los datos q estan marcados con asterisco rojo.');
            // }
        });
        $('.buscar').on('click',function(){
            // if($('#cm').val().length!=7)
            // {
            //     alert('No corresponde al formato de codigo modular');
            // }
            // console.log($('#cm').val().length==7);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            jQuery.ajax(
            { 
                url: "{{ url('/buscar') }}",
                data: {cm:$('#cm').val()},
                method: 'get',
                success: function(result){
                    console.log(result.exist);
                    
                    $('.containerMensaje').css('display','none');
                    $('.containerExist').css('display','none');
                    $('.containerExito').css('display','none');
                    

                    if(result.exist=='1')
                    {
                        $('.formularioConteiner').css('display','block');
                        $('.a_cm').html(result.data.COD_MOD);
                        $('.a_anexo').html(result.data.ANEXO);
                        $('.a_cl').html(result.data.CODLOCAL);
                        $('.a_nombreIe').html(result.data.CEN_EDU);
                        $('.a_nivel').html(result.data.D_NIV_MOD);
                        $('.a_gestion').html(result.data.D_GESTION);
                        $('.a_director').html(result.data.DIRECTOR);
                        $('.a_telefono').html(result.data.TELEFONO);
                        $('.a_email').html(result.data.EMAIL);
                        $('.a_cenPoblado').html(result.data.CEN_POB);
                        $('.a_zona').html(result.data.DAREACENSO);
                        $('.a_dep').html(result.data.D_DPTO);
                        $('.a_pro').html(result.data.D_PROV);
                        $('.a_dis').html(result.data.D_DIST);
                        $('.a_estado').html(result.data.D_ESTADO);
                        $('.a_alumno').html(result.data.TALUMNO);
                        $('.a_docente').html(result.data.TDOCENTE);
                        $('.a_seccion').html(result.data.TSECCION);
                        // alert(result.data.D_NIV_MOD.includes('Inicial'));
                        segunNivel(result.data.D_NIV_MOD);
                        // var banGrado = result.data.D_NIV_MOD.includes('Inicial');
                        // $('.texto1grado').html(banGrado?'0 años':'1 grado');
                        // $('.texto2grado').html(banGrado?'1 años':'2 grado');
                        // $('.texto3grado').html(banGrado?'2 años':'3 grado');
                        // $('.texto4grado').html(banGrado?'3 años':'4 grado');
                        // $('.texto5grado').html(banGrado?'4 años':'5 grado');
                        // if(result.data.D_NIV_MOD.includes('Inicial'))
                        // {
                        //     $('.texto6grado').html(banGrado?'5 años':'6 grado');
                        //     $('.texto7grado').html('6 años');
                        // }
                        // if(result.data.D_NIV_MOD.includes('Primaria'))
                        // {
                        //     $('.texto6grado').html(banGrado?'5 años':'6 grado');
                        //     $('.texto7grado').parent().parent().css('display','none');
                        // }
                        

                        $('.hcm').val(result.data.COD_MOD);

                        $('#cam0').val('');
                        // 2
                        $('#cam21').val('');
                        $('#cam22').val('');
                        $('#cam23').val('');
                        $('#cam24').val('');
                        $('#cam25').val('');
                        $('#cam26').val('');
                        // 3c
                        $('#camc31').val('0');
                        $('#camc32').val('0');
                        $('#camc33').val('0');
                        $('#camc34').val('0');
                        $('#camc35').val('0');
                        $('#camc36').val('0');
                        // 3e
                        $('#came31').val('0');
                        $('#came32').val('0');
                        $('#came33').val('0');
                        $('#came34').val('0');
                        $('#came35').val('0');
                        $('#came36').val('0');

                        $('#came37').val('0');
                        $('#came38').val('0');
                        $('#came39').val('0');
                        $('#came310').val('0');
                        $('#came311').val('0');
                        $('#came312').val('0');
                        $('#came313').val('0');
                        $('#came314').val('0');
                        $('#came315').val('0');
                        $('#came316').val('0');
                        $('#came317').val('0');
                        $('#came318').val('0');
                        $('#came319').val('0');
                        $('#came320').val('0');
                        $('#came321').val('0');

                        $('#came322').val('0');
                        $('#came323').val('0');
                        $('#came324').val('0');
                        $('#came325').val('0');
                        $('#came326').val('0');
                        $('#came327').val('0');
                        $('#came328').val('0');
                        $('#came329').val('0');
                        $('#came330').val('0');
                        $('#came331').val('0');
                        $('#came332').val('0');
                        $('#came333').val('0');
                        $('#came334').val('0');

                        $('#came335').val('0');
                        $('#came336').val('0');
                        // 4c
                        $('#camc41').val('0');
                        $('#camc42').val('0');
                        $('#camc43').val('0');
                        $('#camc44').val('0');
                        $('#camc45').val('0');
                        $('#camc46').val('0');
                        $('#camc47').val('0');
                        $('#camc48').val('0');
                        $('#camc49').val('0');
                        $('#camc410').val('0');
                        $('#camc411').val('0');
                        $('#camc412').val('0');
                        // 4e
                        $('#came41').val('0');
                        $('#came42').val('0');
                        $('#came43').val('0');
                        $('#came44').val('0');
                        $('#came45').val('0');
                        $('#came46').val('0');

                        $('#came47').val('0');
                        $('#came48').val('0');
                        $('#came49').val('0');
                        $('#came410').val('0');
                        $('#came411').val('0');
                        $('#came412').val('0');
                        $('#came413').val('0');
                        $('#came414').val('0');
                        $('#came415').val('0');
                        $('#came416').val('0');
                        $('#came417').val('0');
                        $('#came418').val('0');
                        $('#came419').val('0');
                        $('#came420').val('0');
                        $('#came421').val('0');

                        $('#came422').val('0');
                        $('#came423').val('0');
                        $('#came424').val('0');
                        $('#came425').val('0');
                        $('#came426').val('0');
                        $('#came427').val('0');
                        $('#came428').val('0');
                        $('#came429').val('0');
                        $('#came430').val('0');
                        $('#came431').val('0');
                        $('#came432').val('0');
                        $('#came433').val('0');
                        $('#came434').val('0');

                        $('#came435').val('0');
                        $('#came436').val('0');
                        // 5
                        $('#cam51').val('0');
                        $('#cam52').val('0');
                        $('#cam53').val('0');
                        $('#cam54').val('0');
                        $('#cam55').val('0');
                        $('#cam56').val('0');
                        $('#cam57').val('0');
                        // 6
                        $('#cam61').val('0');
                        $('#cam62').val('0');
                        $('#cam63').val('0');
                        $('#cam64').val('0');
                        $('#cam65').val('0');
                        $('#cam66').val('0');
                        $('#cam67').val('0');
                        $('#cam68').val('0');
                        $('#cam69').val('0');
                        $('#cam610').val('0');

                        $('#cam611').val('0');
                        $('#cam612').val('0');
                        $('#cam613').val('0');
                        $('#cam614').val('0');
                        $('#cam615').val('0');
                        $('#cam616').val('0');
                        $('#cam617').val('0');
                        $('#cam618').val('0');
                        $('#cam619').val('0');
                        $('#cam620').val('0');

                        $('#cam621').val('');
                        $('#cam622n').val('');
                        $('#cam623n').val('');
                        // 7
                        $('#cam71').attr("checked", false);
                        $('#cam72').attr("checked", false);
                        $('#cam73').attr("checked", false);
                        $('#cam74').attr("checked", false);
                        $('#cam75').attr("checked", false);
                        $('#cam76').val('');
                        
                        $('#cam77').val('0');
                        $('#cam78').val('0');
                        $('#cam79').val('0');
                        $('#cam710').val('0');
                        $('#cam711').val('0');
                        $('#cam712').val('0');
                        $('#cam713').val('0');

                            $('#serEnU').val('');$('#serFun').val('');
                            $('#serDan').val('');$('#serGua').val('');
                            $('#serMan').val('');$('#serCom').val('');

                            $('#rouEnU').val('');$('#rouFun').val('');
                            $('#rouDan').val('');$('#rouGua').val('');
                            $('#rouMan').val('');$('#rouCom').val('');

                            $('#swiEnU').val('');$('#swiFun').val('');
                            $('#swiDan').val('');$('#swiGua').val('');
                            $('#swiMan').val('');$('#swiCom').val('');

                            $('#apaEnU').val('');$('#apaFun').val('');
                            $('#apaDan').val('');$('#apaGua').val('');
                            $('#apaMan').val('');$('#apaCom').val('');

                            $('#apeEnU').val('');$('#apeFun').val('');
                            $('#apeDan').val('');$('#apeGua').val('');
                            $('#apeMan').val('');$('#apeCom').val('');

                            $('#lapEnU').val('');$('#lapFun').val('');
                            $('#lapDan').val('');$('#lapGua').val('');
                            $('#lapMan').val('');$('#lapCom').val('');

                            $('#comEnU').val('');$('#comFun').val('');
                            $('#comDan').val('');$('#comGua').val('');
                            $('#comMan').val('');$('#comCom').val('');

                            $('#proEnU').val('');$('#proFun').val('');
                            $('#proDan').val('');$('#proGua').val('');
                            $('#proMan').val('');$('#proCom').val('');

                            $('#smaEnU').val('');$('#smaFun').val('');
                            $('#smaDan').val('');$('#smaGua').val('');
                            $('#smaMan').val('');$('#smaCom').val('');

                            $('#piiEnU').val('');$('#piiFun').val('');
                            $('#piiDan').val('');$('#piiGua').val('');
                            $('#piiMan').val('');$('#piiCom').val('');

                            $('#parEnU').val('');$('#parFun').val('');
                            $('#parDan').val('');$('#parGua').val('');
                            $('#parMan').val('');$('#parCom').val('');

                            $('#potEnU').val('');$('#potFun').val('');
                            $('#potDan').val('');$('#potGua').val('');
                            $('#potMan').val('');$('#potCom').val('');

                        if(result.mensaje!=null)
                        {
                            $('.containerMensaje').css('display','block');

                            $('#cam0').val(result.tformulario.cam0);
                            // 2
                            $('#cam21').val(result.tformulario.cam21);
                            $('#cam22').val(result.tformulario.cam22);
                            $('#cam23').val(result.tformulario.cam23);
                            $('#cam24').val(result.tformulario.cam24);
                            $('#cam25').val(result.tformulario.cam25);
                            $('#cam26').val(result.tformulario.cam26);
                            // 3c

                            $('#camc31').val(result.tformulario.camc31);
                            $('#camc32').val(result.tformulario.camc32);
                            $('#camc33').val(result.tformulario.camc33);
                            $('#camc34').val(result.tformulario.camc34);
                            $('#camc35').val(result.tformulario.camc35);
                            $('#camc36').val(result.tformulario.camc36);
                            // 3e
                            $('#came31').val(result.tformulario.came31);
                            $('#came32').val(result.tformulario.came32);
                            $('#came33').val(result.tformulario.came33);
                            $('#came34').val(result.tformulario.came34);
                            $('#came35').val(result.tformulario.came35);
                            $('#came36').val(result.tformulario.came36);

                            $('#came37').val(result.tformulario.came37);
                            $('#came38').val(result.tformulario.came38);
                            $('#came39').val(result.tformulario.came39);
                            $('#came310').val(result.tformulario.came310);
                            $('#came311').val(result.tformulario.came311);
                            $('#came312').val(result.tformulario.came312);
                            $('#came313').val(result.tformulario.came313);
                            $('#came314').val(result.tformulario.came314);
                            $('#came315').val(result.tformulario.came315);
                            $('#came316').val(result.tformulario.came316);
                            $('#came317').val(result.tformulario.came317);
                            $('#came318').val(result.tformulario.came318);
                            $('#came319').val(result.tformulario.came319);
                            $('#came320').val(result.tformulario.came320);
                            $('#came321').val(result.tformulario.came321);

                            $('#came322').val(result.tformulario.came322);
                            $('#came323').val(result.tformulario.came323);
                            $('#came324').val(result.tformulario.came324);
                            $('#came325').val(result.tformulario.came325);
                            $('#came326').val(result.tformulario.came326);
                            $('#came327').val(result.tformulario.came327);
                            $('#came328').val(result.tformulario.came328);
                            $('#came329').val(result.tformulario.came329);
                            $('#came330').val(result.tformulario.came330);
                            $('#came331').val(result.tformulario.came331);
                            $('#came332').val(result.tformulario.came332);
                            $('#came333').val(result.tformulario.came333);
                            $('#came334').val(result.tformulario.came334);

                            $('#came335').val(result.tformulario.came335);
                            $('#came336').val(result.tformulario.came336);
                            // 4c
                            $('#camc41').val(result.tformulario.camc41);
                            $('#camc42').val(result.tformulario.camc42);
                            $('#camc43').val(result.tformulario.camc43);
                            $('#camc44').val(result.tformulario.camc44);
                            $('#camc45').val(result.tformulario.camc45);
                            $('#camc46').val(result.tformulario.camc46);
                            $('#camc47').val(result.tformulario.camc47);
                            $('#camc48').val(result.tformulario.camc48);
                            $('#camc49').val(result.tformulario.camc49);
                            $('#camc410').val(result.tformulario.camc410);
                            $('#camc411').val(result.tformulario.camc411);
                            $('#camc412').val(result.tformulario.camc412);
                            // 4e
                            $('#came41').val(result.tformulario.came41);
                            $('#came42').val(result.tformulario.came42);
                            $('#came43').val(result.tformulario.came43);
                            $('#came44').val(result.tformulario.came44);
                            $('#came45').val(result.tformulario.came45);
                            $('#came46').val(result.tformulario.came46);

                            $('#came47').val(result.tformulario.came47);
                            $('#came48').val(result.tformulario.came48);
                            $('#came49').val(result.tformulario.came49);
                            $('#came410').val(result.tformulario.came410);
                            $('#came411').val(result.tformulario.came411);
                            $('#came412').val(result.tformulario.came412);
                            $('#came413').val(result.tformulario.came413);
                            $('#came414').val(result.tformulario.came414);
                            $('#came415').val(result.tformulario.came415);
                            $('#came416').val(result.tformulario.came416);
                            $('#came417').val(result.tformulario.came417);
                            $('#came418').val(result.tformulario.came418);
                            $('#came419').val(result.tformulario.came419);
                            $('#came420').val(result.tformulario.came420);
                            $('#came421').val(result.tformulario.came421);

                            $('#came422').val(result.tformulario.came422);
                            $('#came423').val(result.tformulario.came423);
                            $('#came424').val(result.tformulario.came424);
                            $('#came425').val(result.tformulario.came425);
                            $('#came426').val(result.tformulario.came426);
                            $('#came427').val(result.tformulario.came427);
                            $('#came428').val(result.tformulario.came428);
                            $('#came429').val(result.tformulario.came429);
                            $('#came430').val(result.tformulario.came430);
                            $('#came431').val(result.tformulario.came431);
                            $('#came432').val(result.tformulario.came432);
                            $('#came433').val(result.tformulario.came433);
                            $('#came434').val(result.tformulario.came434);
                        
                            $('#came435').val(result.tformulario.came435);
                            $('#came436').val(result.tformulario.came436);
                            // 5
                            $('#cam51').val(result.tformulario.cam51);
                            $('#cam52').val(result.tformulario.cam52);
                            $('#cam53').val(result.tformulario.cam53);
                            $('#cam54').val(result.tformulario.cam54);
                            $('#cam55').val(result.tformulario.cam55);
                            $('#cam56').val(result.tformulario.cam56);
                            $('#cam57').val(result.tformulario.cam57);
                            // 6
                            $('#cam61').val(result.tformulario.cam61);
                            $('#cam62').val(result.tformulario.cam62);
                            $('#cam63').val(result.tformulario.cam63);
                            $('#cam64').val(result.tformulario.cam64);
                            $('#cam65').val(result.tformulario.cam65);
                            $('#cam66').val(result.tformulario.cam66);
                            $('#cam67').val(result.tformulario.cam67);
                            $('#cam68').val(result.tformulario.cam68);
                            $('#cam69').val(result.tformulario.cam69);
                            $('#cam610').val(result.tformulario.cam610);

                            $('#cam611').val(result.tformulario.cam611);
                            $('#cam612').val(result.tformulario.cam612);
                            $('#cam613').val(result.tformulario.cam613);
                            $('#cam614').val(result.tformulario.cam614);
                            $('#cam615').val(result.tformulario.cam615);
                            $('#cam616').val(result.tformulario.cam616);
                            $('#cam617').val(result.tformulario.cam617);
                            $('#cam618').val(result.tformulario.cam618);
                            $('#cam619').val(result.tformulario.cam619);
                            $('#cam620').val(result.tformulario.cam620);
                            
                            $('#cam621').val(result.tformulario.cam621);
                            $('#cam622n').val(result.tformulario.cam622n);
                            $('#cam623n').val(result.tformulario.cam623n);
                            // 7
                            $('#cam71').attr("checked", result.tformulario.cam71=='si'?true:false);
                            $('#cam72').attr("checked", result.tformulario.cam72=='si'?true:false);
                            $('#cam73').attr("checked", result.tformulario.cam73=='si'?true:false);
                            $('#cam74').attr("checked", result.tformulario.cam74=='si'?true:false);
                            $('#cam75').attr("checked", result.tformulario.cam75=='si'?true:false);
                            // alert()
                            $('#cam76').val(result.tformulario.cam76);
                            $('#cam77').val(result.tformulario.cam77);
                            $('#cam78').val(result.tformulario.cam78);
                            $('#cam79').val(result.tformulario.cam79);
                            $('#cam710').val(result.tformulario.cam710);
                            $('#cam711').val(result.tformulario.cam711);
                            $('#cam712').val(result.tformulario.cam712);
                            $('#cam713').val(result.tformulario.cam713);

                            $('#serEnU').val(result.tformulario.serEnU);$('#serFun').val(result.tformulario.serFun);
                            $('#serDan').val(result.tformulario.serDan);$('#serGua').val(result.tformulario.serGua);
                            $('#serMan').val(result.tformulario.serMan);$('#serCom').val(result.tformulario.serCom);

                            $('#rouEnU').val(result.tformulario.rouEnU);$('#rouFun').val(result.tformulario.rouFun);
                            $('#rouDan').val(result.tformulario.rouDan);$('#rouGua').val(result.tformulario.rouGua);
                            $('#rouMan').val(result.tformulario.rouMan);$('#rouCom').val(result.tformulario.rouCom);

                            $('#swiEnU').val(result.tformulario.swiEnU);$('#swiFun').val(result.tformulario.swiFun);
                            $('#swiDan').val(result.tformulario.swiDan);$('#swiGua').val(result.tformulario.swiGua);
                            $('#swiMan').val(result.tformulario.swiMan);$('#swiCom').val(result.tformulario.swiCom);

                            $('#apaEnU').val(result.tformulario.apaEnU);$('#apaFun').val(result.tformulario.apaFun);
                            $('#apaDan').val(result.tformulario.apaDan);$('#apaGua').val(result.tformulario.apaGua);
                            $('#apaMan').val(result.tformulario.apaMan);$('#apaCom').val(result.tformulario.apaCom);

                            $('#apeEnU').val(result.tformulario.apeEnU);$('#apeFun').val(result.tformulario.apeFun);
                            $('#apeDan').val(result.tformulario.apeDan);$('#apeGua').val(result.tformulario.apeGua);
                            $('#apeMan').val(result.tformulario.apeMan);$('#apeCom').val(result.tformulario.apeCom);

                            $('#lapEnU').val(result.tformulario.lapEnU);$('#lapFun').val(result.tformulario.lapFun);
                            $('#lapDan').val(result.tformulario.lapDan);$('#lapGua').val(result.tformulario.lapGua);
                            $('#lapMan').val(result.tformulario.lapMan);$('#lapCom').val(result.tformulario.lapCom);

                            $('#comEnU').val(result.tformulario.comEnU);$('#comFun').val(result.tformulario.comFun);
                            $('#comDan').val(result.tformulario.comDan);$('#comGua').val(result.tformulario.comGua);
                            $('#comMan').val(result.tformulario.comMan);$('#comCom').val(result.tformulario.comCom);

                            $('#proEnU').val(result.tformulario.proEnU);$('#proFun').val(result.tformulario.proFun);
                            $('#proDan').val(result.tformulario.proDan);$('#proGua').val(result.tformulario.proGua);
                            $('#proMan').val(result.tformulario.proMan);$('#proCom').val(result.tformulario.proCom);

                            $('#smaEnU').val(result.tformulario.smaEnU);$('#smaFun').val(result.tformulario.smaFun);
                            $('#smaDan').val(result.tformulario.smaDan);$('#smaGua').val(result.tformulario.smaGua);
                            $('#smaMan').val(result.tformulario.smaMan);$('#smaCom').val(result.tformulario.smaCom);

                            $('#piiEnU').val(result.tformulario.piiEnU);$('#piiFun').val(result.tformulario.piiFun);
                            $('#piiDan').val(result.tformulario.piiDan);$('#piiGua').val(result.tformulario.piiGua);
                            $('#piiMan').val(result.tformulario.piiMan);$('#piiCom').val(result.tformulario.piiCom);

                            $('#parEnU').val(result.tformulario.parEnU);$('#parFun').val(result.tformulario.parFun);
                            $('#parDan').val(result.tformulario.parDan);$('#parGua').val(result.tformulario.parGua);
                            $('#parMan').val(result.tformulario.parMan);$('#parCom').val(result.tformulario.parCom);

                            $('#potEnU').val(result.tformulario.potEnU);$('#potFun').val(result.tformulario.potFun);
                            $('#potDan').val(result.tformulario.potDan);$('#potGua').val(result.tformulario.potGua);
                            $('#potMan').val(result.tformulario.potMan);$('#potCom').val(result.tformulario.potCom);
                        } 
                    }
                    else
                    {
                        $('.formularioConteiner').css('display','none');
                        $('.containerExist').css('display','block');
                    }
                }
            });
        });
    </script>
</body>
</html>