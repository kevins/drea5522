<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> -->
    <script type="text/javascript" src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<meta name="_token" content="{{csrf_token()}}" />
</head>
<body>
	<div class="container">
		<div class="card">
			<div class="card-header">
				gestion de arcbhivos
			</div>
			<div class="card-body">
				<form id="formInsert" action="{{url('ajax/ajax')}}" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label for="">curso</label>
								<select name="curso" id="curso" class="form-control">
									<option disabled selected>Elija un curso</option>
									@foreach($listTcurso as $item)
									<option value="{{$item->idcurso }}">{{$item->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="unidad">unidad</label>
								<input type="text" class="form-control" id="unidad" name="unidad" autocomplete="off" value="{{ old('unidad') }}">
							</div>
							<div id="listUnidad"></div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="sesion">sesion</label>
								<input type="text" class="form-control" id="sesion" name="sesion">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="">archivo por.</label>
								<div class="form-control">
									<label for="runidad" class="m-0"><input type="radio" name="opcion" id="runidad"> unidad</label>
									<label for="rsesion" class="m-0"><input type="radio" name="opcion" id="rsesion"> sesion</label>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="sesion">File</label>
								<input type="file" class="form-control" id="file" name="file">
							</div>
						</div>
					</div>
					{{csrf_field()}}
            		<input type="submit" value="guardar" class="btn btn-success">
            		<!-- <input type="button" value="guardar" class="btn btn-success" id="ajaxSubmit"> -->
				</form>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				cascas
			</div>
			<div class="card-body">
				<div class="contenedor-ajax">
					<table class="table">
						<thead>
							<tr>
								<th>curso</th>
								<th>unidad</th>
								<th>sesion</th>
							</tr>
						</thead>
						<tbody class="loadRegistros">
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
<script>
	$(document).ready(function()
	{
		loadRegistros();

		$('#unidad').keyup(function(){
			var query = $(this).val();
			$.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		            }
		    });
			jQuery.ajax(
			{ 
				url: "{{ url('/autocomplete/listUnidad') }}",
		        data: {query:query},
		        method: 'post',
		        success: function(result){
		           $('#listUnidad').fadeIn();
		           $('#listUnidad').html(result);
		    	}
			});
		});
	});
	$(document).on('click','li',function(){
		$('#unidad').val($(this).text());
		$('#listUnidad').fadeOut();
	});

	function loadRegistros()
	{
		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
      	});
		jQuery.ajax(
		{ 
			url: "{{ url('/gestionfile/listar') }}",
	        data: {},
	        method: 'post',
	        success: function(result){
	           $('.loadRegistros').append(result)
	    	}
		});
	}
	jQuery('#ajaxSubmit').click(function(e)
	{
		var upload = $('#file');
		// upload.addEventListener('change',preview)
		console.log(upload);
		data={
			idcurso: $('#curso').val(),
			unidad: $('#unidad').val(),
			sesion: $('#sesion').val(),
			bunidad: $('#runidad').is(':checked'),
			bsesion: $('#rsesion').is(':checked'),
			file: $('#file').val()
		};
		console.dir(data);
       	e.preventDefault();
       	$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
      	});
       	jQuery.ajax({
          	url: "{{ url('/ajax/ajax') }}",
          	method: 'post',
          	data: data,
          	success: function(result){
          		$('.contenedor-ajax').append(result)
          	}
        });
    });
	
</script>
</body>
</html>