@extends('template/template')
@section('generalBody')
<meta name="_token" content="{{csrf_token()}}" />
<style>
    .select2-selection.select2-selection--single{padding-top: .20rem!important;}
</style>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Documentos de gestion de instituciones educativas</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <form id="formSerch" action="{{url('fileIe/listPcDcte')}}" method="post">
                <div class="row mx-2">
                	<div class="col-md-4">
                        <div class="form-group mb-2">
                            <label class="m-0">Ugel:</label>
                            <select name="ugel" id="ugel" class="form-control form-control-sm">
                                <option disabled selected>Elija la ugel:</option>
                                <option value="30001">UGEL Abancay</option>
                                <option value="30002">UGEL Andahuaylas</option>
                                <option value="30003">UGEL Antabamba</option>
                                <option value="30004">UGEL Aymaraes</option>
                                <option value="30005">UGEL Cotabambas</option>
                                <option value="30006">UGEL Chincheros</option>
                                <option value="30007">UGEL Grau</option>
                                <option value="30008">UGEL Huancarama</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group mb-2">
                            <label class="m-0">Nivel:</label>
                            <select name="nivel" id="nivel" class="form-control form-control-sm">
                                <option disabled selected>Elija el nivel:</option>
                                <option value="inicial">Inicial</option>
                                <option value="primaria">Primaria</option>
                                <option value="secundaria">Secundaria</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">I.E.:</label>
                            <select name="listIe" id="listIe" class="form-control form-control-sm">
                                <option disabled selected>Seleccione I.E.</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="col-md-3">
                        <div class="form-group">
                            <label class="m-0">Fecha inicio:</label>
                            <input type="date" name="fechaI" id="fechaI" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="m-0">Fecha final:</label>
                            <input type="date" name="fechaF" id="fechaF" class="form-control form-control-sm">
                        </div>
                    </div> -->
                </div>
                {{csrf_field()}}
            </form>

            <div class="row mx-2 mb-3">
                <div class="col-md-4">
                    <input type="button" value="limpiar" class="btn btn-secondary btn-sm w-100 limpiar">
                </div>
                <div class="col-md-4">
                    <input type="button" value="Buscar" class="btn btn-success btn-sm w-100 btnBuscar">
                </div>
            </div>
            <div class="row contenedorAjax">
                <div class="col-md-12 contenedorTable">
                    <div class="alert alert-primary text-center m-0 font-weight-bold">Agregue los datos para comensar a buscar.</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $( document ).ready(function() 
    { 
        $("#listIe").select2({
            placeholder: "Seleccione una I.E.",
        });
    });
    $('.limpiar').on('click',function(){
        $('#ugel option:nth-child(1)').prop("selected",true);
        $('#nivel option:nth-child(1)').prop("selected",true);
        $('#listIe').val(null).trigger('change');
        // $('#fechaI').val('');
        // $('#fechaF').val('');
    });
    $('.btnBuscar').on('click',function()
    {
        if($('#listIe').val()===null)
        {
            new PNotify(
            {
                title : 'No se pudo proceder',
                text : 'Debe de seleccionar una institución educativa.',
                type : 'alert'
            });
            return 0;
        }
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSpecialist2/listDocGes') }}",
            data: {
                cm:$('#listIe').val(),
                fi:$('#fechaI').val(),
                ff:$('#fechaF').val(),
            },
            method: 'get',
            success: function(result){
                $('.contenedorTable').remove();
                $('.contenedorAjax').append(result);
            }
        });
    });
    $('#nivel,#ugel').on('change',function(ev){
        var nivel=$('#nivel').val();
        var ugel=$('#ugel').val();
        if(nivel!==null && ugel!==null)
        {
            console.log('cargaie(nivel);');
            cargaie(nivel);
        }
            
    });
    function cargaie(nivel)
    {
        console.log('entro a la funcion para cargar las ie');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSpecialist/getIeGeneralUgel') }}",
            data: {nivel:nivel,idugel:$('#ugel').val()},
            method: 'get',
            success: function(result){
                // $('.select2-selection.select2-selection--single').addClass('pt-1');
                $("#listIe").empty();
                $("#listIe").append("<option disabled selected>Seleccione I. E.</option>");
                $.each( result, function(key, value){
                    var valueIe = value.codigomodular+"//"+value.ie_nombre+"//"+$('#nivel').val();
                    $("#listIe").append("<option value='" + valueIe + "'>"+value.codigomodular+" - "+value.ie_nombre + "</option>");
                });
            }
        });
    }
</script>
@endsection