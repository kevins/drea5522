@extends('template/template')
@section('generalBody')
@if(session()->get('Person')->tEspecialista->ugelid=='30000')
    @include('fileSpecialist2/listDocGesSegunUgel/ugelGeneral')
@else

    @include('fileSpecialist2/listDocGesSegunUgel/ugelParticular')
@endif
@endsection