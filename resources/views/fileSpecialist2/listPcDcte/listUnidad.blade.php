<div class="col-md-12 contenedorTable">
	<table id="list" class="table table-sm table-hover my-0" style="width:100%;">
        <thead class="text-center">
            <tr>
                <th>Edad</th>
                <th>Seccion</th>
                <th>Unidad</th>
                <th>Tipo de unidad</th>
                <th>Nombre de la unidad</th>
                <th>P. academico</th>
                <th>Fecha que se realizo</th>
                <th>Fecha de registro</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($list as $item)
            <tr class="text-center">
                <td>{{$item->tCursoxdocente->tGrado->nombre}}</td>
                <td>Sección {{$item->tCursoxdocente->tSeccion->nombre}}</td>
                <td>{{$item->nombre}}</td>
                <td>{{$item->tipoUnidad}}</td>
                <td>{{$item->nombre}}</td>
                <td>{{$item->tCursoxdocente->periodoAcademico}}</td>
                <td>{{$item->fechaPertenece}}</td>
                <td>{{$item->createddate}}</td>
                <td>
                    @if($item->comentario!='')
                    <span class="btn btn-info btn-xs" data-toggle="tooltip" title="{{$item->comentario}}"><i class="fab fa-rocketchat"></i></span>
                    @endif
                    <a href="{{asset('fileunidad/')}}/{{$item->tCursoxdocente->tDocente->dni}}-{{$item->idunidad}}.{{$item->formato}}" target="_blank" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a>
                    <span class="badge badge-secondary"><i class="fa fa-file"></i> {{count($item->tSesion)}} sesiones</span>
                </td>
            </tr>
            @endforeach
        </tbody>      
    </table>
</div>
<script>
   $(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Se tiene _TOTAL_ docentes.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No hay registros de docentes.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Registros',
                    text: '<i class="fa fa-file-excel p-0 fa-lg"></i>',
                    className:'btn btn-success p-1 excel ml-4',
                },
                {
                    extend: 'pdfHtml5',
                    title: 'Registros',
                    text: '<i class="fa fa-file-pdf p-0 fa-lg"></i>',
                    className:'btn btn-danger p-1',
                },
                {
                    extend: 'print',
                    title: 'Registros',
                    text: '<i class="fa fa-print p-0 fa-lg"></i>',
                    className:'btn btn-info p-1',
                }
            ]
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar Docente');
    } );
</script>