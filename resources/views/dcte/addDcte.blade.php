<style>
	fieldset {
    border: 1.5px solid #dad6d6 !important;
    border-radius: 3px !important;
    padding: .5rem !important;
}
legend {
    width: auto !important;
    margin: 0 !important;
    font-size: 15px !important;
    padding: .25rem !important;
}
</style>
<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2 ui-sortable-handle" style="cursor: move;">
            <h3 class="card-title font-weight-bold">Agregar docente</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="formInsert" action="{{ url('/dcte/addDcte') }}" method="post" class="m-0">
                <input type="hidden" id="cm" name="cm" value="{{$ie->codigomodular}}">
                <div class="col-md-12 alert alert-info msjIe text-center font-weight-bold p-1 mb-1 colegioIe">{{$ie->ie_nombre}}</div>
                <fieldset>
                	<legend>Datos como persona</legend>
                	<div class="row">
                		<div class="col-md-4">
	                        <div class="form-group mb-2">
	                            <label class="m-0">Dni:</label>
	                            <input type="text" name="dni" id="dni" class="form-control form-control-sm" maxlength="8">
	                        </div>
	                    </div>
	                	<div class="col-md-4">
	                        <div class="form-group mb-2">
	                            <label class="m-0">Apellido Paterno:</label>
	                            <input type="text" name="apaterno" id="apaterno" class="form-control form-control-sm">
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="form-group mb-2">
	                            <label class="m-0">Apellido Materno:</label>
	                            <input type="text" name="amaterno" id="amaterno" class="form-control form-control-sm">
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="form-group mb-2">
	                            <label class="m-0">Nombres:</label>
	                            <input type="text" name="nombre" id="nombre" class="form-control form-control-sm">
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="form-group mb-2">
	                            <label class="m-0">Fecha de Nacimiento:</label>
	                            <input type="date" name="fNacimiento" id="fNacimiento" class="form-control form-control-sm">
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <label class="m-0">Sexo:</label>
	                        <div class="form-control form-control-sm text-center">
	                            <input type="radio" name="sexo" id="M" value="M" checked>
			                    <label for="M" style="margin-right:20px" class="font-weight-normal">Masculino</label>
			                    <input type="radio" name="sexo" id="F" value="F"> 
			                    <label for="F" class="font-weight-normal">Femenino</label>
	                        </div>
	                    </div>
	                </div>
                </fieldset>
                <fieldset class="mb-2">
                	<legend>Datos como docente</legend>
                	<div class="row">
                		<div class="form-group col-md-4">    
                			<label for="condicion" class="m-0">Condición del docente:</label>    
                			<select id="condicion" name="condicion" class="form-control form-control-sm" required="">    
                				<option disabled="" selected="">Seleccione condición:</option> 
                				<option value="NOMBRADO">NOMBRADO</option>    
                				<option value="CONTRATADO">CONTRATADO</option>
                				<option value="ENCARGADO">ENCARGADO</option>
                				<option value="DESIGNADO">DESIGNADO</option>
                				<option value="DESTACADO">DESTACADO</option>    
                			</select>
                		</div>
                		<div class="form-group col-md-4">
                			<label for="nombre" class="m-0">Cargo:</label>
                			<input type="text" name="cargo" id="cargo" class="form-control form-control-sm" value="">
                		</div>
                		<div class="col-md-4">
	                        <label class="m-0">Estado:</label>
	                        <div class="form-control form-control-sm text-center">
	                            <input type="radio" name="estado" id="activo" value="activo" checked>
			                    <label for="activo" style="margin-right:20px" class="font-weight-normal">En actividad</label>
			                    <input type="radio" name="estado" id="inactivo" value="inactivo"> 
			                    <label for="inactivo" class="font-weight-normal">Inactivo</label>
	                        </div>
	                    </div>
                	</div>
                {{csrf_field()}}
             </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar" form="formInsert" class="btn btn-success btn-sm float-right ml-1">
            <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1 limpiar">
        </div>
    </div>
</div>
<script>
    var nivel = $("#nivel option:selected").text()==''?$("#nivel").val():$("#nivel option:selected").text();
    $('.colegioIe').html(nivel+' | '+$("#ies option:selected").text());
    $('.limpiar').on('click',function(){
        $('#dni').val('');
        $('#apaterno').val('');
        $('#amaterno').val('');
        $('#nombre').val('');
        $('#fNacimiento').val('');
        $('#condicion').val('');
        $('#cargo').val('');
    });
	// return this.optional(element) ||  /^[a-zA-Z0-9]{5,10}$/.test(value);
	$.validator.addMethod('customphone', function (value, element) 
	{
    	return this.optional(element) ||  /(\d{8})/.test(value);
	}, "Por favor ingrese un número de DNI válido.");

$("#formInsert").validate({
    errorClass: "text-danger font-italic font-weight-normal",
    ignore: ".ignore",
    rules: {
    	dni: {
            required: true,
            customphone: true
        },
		apaterno:"required",
		amaterno:"required",
		nombre:"required",
		fNacimiento:"required",
		condicion:"required",
		cargo:"required",
    },
    messages: {
        dni: {
            required: "Ingrese el DNI.",
            customphone:"Por favor ingrese un número de DNI válido."
        },
		apaterno:"Ingrese el apellido.",
		amaterno:"Ingrese el apellido.",
		nombre:"Ingrese los nombres.",
		fNacimiento:"Ingrese la fecha de nacimiento.",
		condicion:"Ingrese la condicion",
		cargo:"Ingrese el cargo",
    },
});
</script>

