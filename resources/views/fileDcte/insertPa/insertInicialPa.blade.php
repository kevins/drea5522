<style>
    .selectedRow{
        background: rgba(1, 1, 1, 0.22);
    }
</style>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Agregar planificacion anual</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="frmInsertFilePa" action="{{url('fileDcte/insertPa')}}" method="post" enctype="multipart/form-data" class="m-0"> 
                <input type="hidden" id="idcd" name="idcd">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">{{stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')== false?'Grado:':'Edad:'}}</label>
                            <select class="form-control form-control-sm" id="selectGrado" name="selectGrado" style="width: 100%;">
                                <option selected disabled>Seleccione {{stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')== false?'el grado':'la edad'}}</option>
                                @foreach($tGrado as $item)
                                    <option value="{{$item->idgrado}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Sección:</label>
                            <select class="form-control form-control-sm" id="selectSeccion" name="selectSeccion" style="width: 100%;">
                                <option selected disabled>Seleccione la seccion</option>
                                @foreach($tSeccion as $item)
                                <option value="{{$item->idseccion}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')== false)
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Curso:</label>
                            <!-- <select id="shareDocente" class="form-control form-control-sm select2" name="docentes[]" multiple="multiple"> -->
                            <select id="selectCurso" name="selectCurso" class="form-control form-control-sm" style="width: 100%;">
                                <option selected disabled>Seleccione un curso</option>
                                @foreach($tCurso as $item)
                                <option value="{{$item->idcurso}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Archivo:</label>
                            <input class='form-control form-control-sm' type="File" id="file" name="file">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Periodo académico:</label>
                            <div class="form-control form-control-sm text-center" class="radio">
                                <input type="radio" name="periodo" id="Trimestre" value="Trimestre" checked> 
                                <label for="Trimestre" style='margin-right:20px' >Trimestre</label>
                                <input type="radio" name="periodo" id="Bimestre" value="Bimestre">
                                <label for="Bimestre">Bimestre</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="m-0">Estado:</label>
                        <div class="form-control form-control-sm text-center">
                            <input type="checkbox" name="estadoArchivo" id="estadoArchivo" value="publico"> 
                            <label for="estadoArchivo">Compartir el archivo</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Comentario:</label>
                                <textarea class="form-control form-control-sm" id="txtComentario" name="txtComentario" rows="1"></textarea>
                        </div>
                    </div>
                </div>
                {{csrf_field()}}
            </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar"  form="frmInsertFilePa" class="btn btn-success btn-sm float-right ml-1 btnSubmit">
            <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1 limpiar">
        </div>
    </div>
</div>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Lista de plan anual</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <div class="row">
                <div class="col-md-12">
                    <table id="agregar" class="table table-sm table-hover m-0">
                        <thead class="text-center">
                            <tr>
                                <th>Nivel</th>
                                <th>Edad</th>
                                <th>Sección</th>
                                <th class="existCurso">Curso</th>
                                <th>P. academico</th>
                                <th>Formato</th>
                                <th>Peso</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tCursoxDocente as $item)
                            <tr class="text-center">
                                <td>{{$item->nivel}}</td>
                                <td>{{$item->tgrado->nombre}}</td>
                                <td>seccion {{$item->tseccion->nombre}}</td>
                                <td class="existCurso">{{$item->tcurso->nombre}}</td>
                                <td>{{$item->periodoAcademico}}</td>
                                <td>
                                    @if($item->formato!='')
                                    <span class="fa {{$item->formato=='pdf'?'fa-file-pdf':''}}
                                    {{$item->formato=='docx'?'fa-file-word':''}}
                                    {{$item->formato=='xlsx'?'fa-file-excel':''}}"></span> {{$item->formato}}
                                    @else- -
                                    @endif
                                </td>
                                <td>{{$item->peso}}</td>
                                <td>
                                    <a href="#" class="edit btn btn-primary btn-xs" data-id="{{$item->idcd}}" data-toggle="tooltip" title="Editar registro"><i class="fa fa-edit"></i></a>

                                    @if($item->comentario!='')
                                    <span class="btn btn-info btn-xs" data-toggle="tooltip" title="{{$item->comentario}}"><i class="fab fa-rocketchat"></i></span>
                                    @endif

                                    <a href="{{asset('filedcte/')}}/{{session()->get('Person')->dni}}-{{$item->idcd}}.{{$item->formato}}" target="_blank" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a>
                                    @if(count($item->tUnidad)==0)
                                    <span class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar" onclick="window.location.href='{{url('fileDcte/deletePa')}}/{{$item->idcd}}';"><i class="fa fa-trash"></i></span>
                                    @endif
                                    <span class="badge badge-secondary"><i class="fa fa-file"></i> {{count($item->tUnidad)}} unidades</span>
                                </td>
                            </tr>
                          @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var existCurso = {!! json_encode(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')== false) !!};
    $('.existCurso').css('display',existCurso==true?'block':'none');

    $( document ).ready(function() 
    { 
        $("#selectCurso").select2({
            placeholder: "Seleccione un curso",
        });
    });

    $('.limpiar').on('click',function(){
        $('#selectGrado option:nth-child(1)').prop("selected",true);
        $('#selectSeccion option:nth-child(1)').prop("selected",true);
        $('#frmInsertFilePa').attr('action','{{url('fileDcte/insertPa')}}');
        $('#selectCurso').val(null).trigger('change');
        $('#file').val('');
        $('#txtComentario').val('');
        $('.btnSubmit').val('Guardar');
    });

    $("#frmInsertFilePa").validate({
        errorClass: "text-danger font-italic font-weight-normal",
        ignore: ".ignore",
        rules: {
            selectGrado:"required",
            selectSeccion:"required",
            selectCurso:"required",
            file: {
                required: true,
                extension: "docx|xls|xlsx|doc|pdf"
            },
        },
        messages: {
            selectGrado:"Seleccione una edad",
            selectSeccion:"Seleccione la seccion",
            selectCurso:"Seleccione el curso",
            file: {
                required: 'Ingrese un documento',
                extension:"Solo se acepta word, pdf, excel"
            }
        },
    });
</script>
<script>
    $('.edit').on('click',function(){
        $('table>tbody').find('.selectedRow').removeClass('selectedRow');
        $(this).parent().parent().addClass('selectedRow');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileDcte/editPa') }}",
            data: {idcd:$(this).attr('data-id')},
            method: 'get',
            success: function(result){
                console.log(result.data);
                $('#frmInsertFilePa').attr('action','{{url('fileDcte/editPa')}}');
                $('.btnSubmit').val('Guardar cambios');
                $('#idcd').val(result.data.idcd);
                $('#selectCurso').val(result.data.idcurso);
                $('#selectCurso').trigger('change');

                $('#selectSeccion option').each(function(){
                    if(result.data.idseccion==$(this).val())
                        $(this).attr("selected",true);
                });
                $('#selectGrado option').each(function(){
                    if(result.data.idgrado==$(this).val())
                        $(this).attr("selected",true);
                });
                
                if(result.data.periodoAcademico=='Bimestre') 
                    $("#Bimestre").attr('checked',true);
                if(result.data.periodoAcademico=='Trimestre') 
                    $("#Trimestre").attr('checked',true);

                if(result.data.estadoArchivo==1) 
                    $("#estadoArchivo").prop('checked',true);
                else
                    $("#estadoArchivo").prop('checked',false);
                
                $('#txtComentario').val(result.data.comentario);
                $('#file').addClass('ignore');
            }
        });
    });
    // $("#radio_1").prop("checked", true);//para versiones >3jquery
</script>
<script src="{{asset('viewResources/file/insert.js')}}"></script>