<style>
    .showEdit{visibility: inherit;}
    .selectedRow{background: rgba(1, 1, 1, 0.22) !important;}
</style>
<div class="container-fluid p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Agregar unidad didactica</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="frmInsertFile" action="{{url('fileUnidad/insertUnidad')}}" method="post" enctype="multipart/form-data" class="m-0"> 
                <input type="hidden" name="idunidad" id="idunidad">
                <input type="hidden" name="idcd" id="idcd" value="{{$idcd}}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Tipo de unidad:</label>
                            <select class="form-control form-control-sm" id="selectTUnidad" name="selectTUnidad" style="width: 100%;">
                                <option selected disabled>Seleccione tipo de unidad</option>
                                <option value="Proyecto">Proyecto</option>
                                <option value="Taller">Taller</option>
                                <option value="Unidad de aprendizaje">Unidad de aprendizaje</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Nombre de unidad:</label>
                            <input type="text" name="nombre" id="nombre" class="form-control form-control-sm">
                        </div>
                    </div>
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Fecha a la que pertenece:</label>
                            <input type="date" name="fechaPertenece" id="fechaPertenece" class="form-control form-control-sm">
                        </div>
                    </div> -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Fecha a la que pertenece:</label>
                            <select name="fechaPertenece" id="fechaPertenece" class="form-control form-control-sm">
                                <option disabled selected>Elija el mes:</option>
                                <option value="enero">Enero</option><option value="febrero">Febrero</option>
                                <option value="marzo">Marzo</option><option value="abril">Abril</option>
                                <option value="mayo">Mayo</option><option value="junio">Junio</option>
                                <option value="julio">Julio</option><option value="agosto">Agosto</option>
                                <option value="setiembre">Setiembre</option><option value="octubre">Octubre</option>
                                <option value="noviembre">Noviembre</option><option value="diciembre">Diciembre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Archivo:</label>
                            <input class='form-control form-control-sm' type="File" id="file" name="file">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="m-0">Estado:</label>
                        <div class="form-control form-control-sm text-center">
                            <input type="checkbox" name="estadoArchivo" id="estadoArchivo" value="publico"> 
                            <label for="estadoArchivo">Compartir el archivo</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Comentario:</label>
                            <textarea class="form-control form-control-sm" rows="1" id="comentario" name="comentario" placeholder="Enter ..."></textarea>   
                        </div>
                    </div>
                </div>
                {{csrf_field()}}
            </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar" form="frmInsertFile" class="btn btn-success btn-sm float-right ml-1 btnSubmit">
            <a href="{{url('fileDcte/insertUS')}}" class="btn btn-primary btn-sm float-right ml-1">Mostrar todos los archivos</a>
            <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1 limpiar">
        </div>
    </div>
</div>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2 ui-sortable-handle" style="cursor: move;">
            <h3 class="card-title font-weight-bold">Lista de unidades</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
            @include('fileDcte/share/listUnidad')
    </div>
</div>
<script>
    $('.limpiar').on('click',function(){
        $('#selectTUnidad option:nth-child(1)').prop("selected",true);
        $('#nombre').val("");
        $('#fechaPertenece').val("");
        $('#frmInsertFile').attr('action','{{url('fileUnidad/insertUnidad')}}');
        $('#file').val('');
        $('#comentario').val('');
        $('.btnSubmit').val('Guardar');
    });
</script>
<script>
    $('.edit').on('click',function(){
        $('table>tbody').find('.selectedRow').removeClass('selectedRow');
        $(this).parent().parent().addClass('selectedRow');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileUnidad/editUnidad') }}",
            data: {idunidad:$(this).attr('data-id')},
            method: 'get',
            success: function(result){
                console.log('result.data');
                $('#frmInsertFile').attr('action','{{url('fileUnidad/editUnidad')}}');
                $('.btnSubmit').val('Guardar cambios');
                $('#idunidad').val(result.data.idunidad);

                $('#selectTUnidad option').each(function(){
                    if(result.data.tipoUnidad==$(this).val())
                        $(this).attr("selected",true);
                });

                if(result.data.estadoArchivo==1) 
                    $("#estadoArchivo").prop('checked',true);
                else
                    $("#estadoArchivo").prop('checked',false);
                
                // $('#fechaPertenece').val(result.data.fechaPertenece);
                $('#fechaPertenece option').each(function(){
                    if(result.data.fechaPertenece==$(this).val())
                        $(this).attr("selected",true);
                });
                $('#nombre').val(result.data.nombre);
                $('#comentario').val(result.data.comentario);
                $('#file').addClass('ignore');
            }
        });
    });
    // $("#radio_1").prop("checked", true);//para versiones >3jquery
    $("#frmInsertFile").validate({
        errorClass: "text-danger font-italic font-weight-normal",
        ignore: ".ignore",
        rules: {
            selectTUnidad:"required",
            nombre:"required",
            fechaPertenece:"required",
            file: {
                required: true,
                extension: "docx|xls|xlsx|doc|pdf"
            },
        },
        messages: {
            selectTUnidad:"Seleccione el tipo de unidad.",
            nombre:"Ingrese un nombre.",
            fechaPertenece:"Fecha de la unidad.",
            file: {
                required: 'Ingrese un documento.',
                extension:"Solo se acepta word, pdf, excel."
            }
        },
    });
</script>