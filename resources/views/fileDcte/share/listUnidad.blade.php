<div class="card-body p-0">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive pt-2">
            	<table id="listTunidad" class="table table-sm table-hover m-0" style="width:100%;">
            	    <thead class="text-center">
            	        <tr>
            	        	<th>Edad</th>
            	            <th>Tipo</th>
            	            <th>Nombre</th>
            	            <th>Fecha de la unidad</th>
            	            <th>Formato</th>
            	            <th>Peso</th>
            	            <th>Opciones</th>
            	        </tr>
            	    </thead>
            	    <tbody>
            	    	@foreach($listTunidad as $item)
						<tr class="text-center">
							<td>{{$item->tCursoxdocente->tGrado->nombre}}</td>
							<td>{{$item->tipoUnidad}}</td>
							<td>{{$item->nombre}}</td>
							<td>{{$item->fechaPertenece}}</td>
							<td>{{$item->formato}}</td>
							<td>{{$item->peso}}</td>
							<td>
								<a href="#" class="edit btn btn-primary btn-xs showEdit edit" data-id="{{$item->idunidad}}" title="Editar registro"><i class="fa fa-edit"></i></a>

                                @if($item->comentario!='')
                                <span class="btn btn-info btn-xs" title="{{$item->comentario}}"><i class="fab fa-rocketchat"></i></span>
                                @endif

                                <a href="{{asset('fileunid/')}}/{{session()->get('Person')->dni}}-{{$item->idunidad}}.{{$item->formato}}" target="_blank" title="Descargar" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a>
                                @if(count($item->tSesion)==0)
                                <span class="btn btn-danger btn-xs" title="Eliminar" onclick="window.location.href='{{url('fileUnidad/deleteUnidad')}}/{{$item->idunidad}}';"><i class="fa fa-trash"></i></span>
                                @endif
                                <span class="badge badge-secondary"><i class="fa fa-file"></i> {{count($item->tSesion)}} sesiones</span>
							</td>
						</tr>
						@endforeach
            		</tbody>
            	    <tfoot>
            	    </tfoot>
            	</table>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready( function () {
        $('#listTunidad').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
</script>