@extends('template/template')
@section('generalBody')
<style>
	.showEdit{visibility: hidden;}
	.seleccionado{background:rgba(91, 208, 43,.7) !important;}
</style>
<div class="container-fluid my-3">
    <div class="row">
    	@if(count($tCursoxDocente)==0)
    	<div class="alert alert-warning w-100 text-center m-0">Debe de <strong><u>cargar plan curricular</u></strong> para poder subir los archivos de unidades.</div>
    	@endif
    	@foreach($tCursoxDocente as $item)
    	<div class="col-lg-3 col-6">
            <div class="small-box bg-info m-0">
                <div class="inner py-0">
                	@if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')== false)
                    	<h3 class="m-0 pt-2 text-lg" style="font-size: 1rem !important;">{{$item->tcurso->nombre}} | {{$item->periodoAcademico}}</h3>
                        <p class="mb-0">{{$item->tGrado->nombre}}</p>
                        <p class="mb-0">Tiene {{count($item->tUnidad)}} unidades</p>
                    @else
                        <h3 class="m-0 pt-2 text-lg">{{$item->tgrado->nombre}} | {{$item->periodoAcademico}}</h3>
                        <p class="mb-0">Seccion {{$item->tSeccion->nombre}}</p>
                        <p class="mb-1">Tiene {{count($item->tUnidad)}} unidades</p>
                	@endif
                </div>
                <div class="icon"><i class="fa fa-briefcase"></i></div>
                <a href="#" class="small-box-footer p-0 showFormUs" data-id="{{$item->idcd}}" data-toggle="collapse" data-target="#id{{$item->idcd}}" aria-expanded="false" aria-controls="id{{$item->idcd}}"> <i class="fa fa-arrow-circle-right"></i></a>
                
                <!-- <div id="accordion">
				  	<div class="card">
				    	<div id="id{{$item->idcd}}" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
				      		<div class="card-body text-dark p-2">
				      			<a href="#" class="dropdown-item p-0 showFormUs" title="Agregar unidades y sesiones" data-toggle="tooltip">
				                    <i class="fa fa-plus mr-2"></i> 1er trimestre ( 2 archivos).
				                </a>
				                <div class="dropdown-divider m-0"></div>
				                <a href="#" class="dropdown-item p-0" title="Agregar unidades y sesiones" data-toggle="tooltip">
				                    <i class="fa fa-plus mr-2"></i> 2do trimestre ( 2 archivos).
				                </a>
				                <div class="dropdown-divider m-0"></div>
				                <a href="#" class="dropdown-item p-0" title="Agregar unidades y sesiones" data-toggle="tooltip">
				                    <i class="fa fa-plus mr-2"></i> 3er trimestre ( 1 archivos).
				                </a>
				      		</div>
				    	</div>
				  	</div>
				</div> -->
            </div>
        </div>
		@endforeach
    </div>
</div>
<div class="contenedor-ajax">
	<div class="container-fluid p-0">
	    <div class="card card-default card-info card-outline">
	        <div class="card-header py-2 pl-2 ui-sortable-handle" style="cursor: move;">
	            <h3 class="card-title font-weight-bold">Lista de todas las unidades</h3>
	            <div class="card-tools">
	                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
	            </div>
	        </div>
	        @include('fileDcte/share/listUnidad')
	    </div>
	</div>
</div>
<script>
	

    $('.showFormUs').on('click',function(){
    	$('.seleccionado').removeClass('seleccionado');
    	$(this).addClass('seleccionado');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileUnidad/showFormInsertU') }}",
            data: {idcd:$(this).attr('data-id')},
            method: 'get',
            success: function(result){
                $('.contenedor-ajax>div').remove();
                $('.contenedor-ajax').append(result);
            }
        });
    });
</script>
@endsection