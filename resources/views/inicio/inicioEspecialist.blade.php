@extends('template/template')
@section('generalBody')
<script src="{{asset('js/export/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/export/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/export/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/export/jszip.min.js')}}"></script>

<script src="{{asset('js/export/pdfmake.min.js')}}"></script>

<script src="{{asset('js/export/vfs_fonts.js')}}"></script>
<script src="{{asset('js/export/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/export/buttons.print.min.js')}}"></script>
<style>
    .dt-buttons{
        float: left !important;
    }
</style>
<script src="{{asset('js/Chart.min.js')}}"></script>
<meta name="_token" content="{{csrf_token()}}" />

<!-- <div class="container-fluid mt-3">
    <div class="row">
        <div class="col-lg-3 col-6">
            <div class="small-box bg-info m-0">
                <div class="inner py-0">
                    <h3 class="m-0">150</h3>
                    <p class="m-0">Cant. I.E.</p>
                </div>
                <div class="icon"><i class="ion ion-bag"></i></div>
                <a href="#" class="small-box-footer p-0">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-6">
            <div class="small-box bg-success m-0">
                <div class="inner py-0">
                    <h3 class="m-0">53<sup style="font-size: 20px">%</sup></h3>
                    <p class="m-0">Cant. docentes</p>
                </div>
                <div class="icon"><i class="ion ion-stats-bars"></i></div>
                <a href="#" class="small-box-footer p-0">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-6">
            <div class="small-box bg-warning m-0">
                <div class="inner py-0">
                    <h3 class="m-0">44</h3>
                    <p class="m-0">Monitoreo de I.E.</p>
                </div>
                <div class="icon"><i class="ion ion-person-add"></i></div>
                <a href="#" class="small-box-footer p-0">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-6">
            <div class="small-box bg-danger m-0">
                <div class="inner py-0">
                    <h3 class="m-0">65</h3>
                    <p class="m-0">Monintoreo de docentes</p>
                </div>
                <div class="icon"><i class="ion ion-pie-graph"></i></div>
                <a href="#" class="small-box-footer p-0">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
</div> -->
<div class="card card-default card-info card-outline mt-3">
    <div class="card-header py-2 pl-2 mb-2">Resumen por ugel</div>
    <div class="card-body p-0">
        <div class="col-md-12 contenedorTable p-0">
            <table id="list" class="table table-sm table-hover my-0" style="width:100%;">
                <thead class="text-center">
                    <tr>
                        <th>Codigo</th>
                        <th>Ugel</th>
                        <th># I.E.</th>
                        <th># Docentes</th>
                        <th># Plan anual</th>
                        <th># Unidades</th>
                        <th># Sesiones</th>
                    </tr>
                </thead>
                <tbody>
                    
                    @for($i=0;$i<=7;$i++)
                    <tr class="text-center">
                        <td>{{$listPa[$i]['idugel']}}</td>
                        <td>{{$listNie[$i]->ugel_nombre}}</td>
                        <td>{{$listNie[$i]->nie}}</td>
                        <td>{{$listDcte[$i]->ndcte}}</td>
                        <td><p class="mb-0"><i class="fa fa-file"></i> {{$listPa[$i]['np']}}</p></td>
                        <td><p class="mb-0"><i class="fa fa-file"></i> {{$listU[$i]['nu']}}</p></td>
                        <td><p class="mb-0"><i class="fa fa-file"></i> {{$listS[$i]['ns']}}</p></td>
                    </tr>
                    @endfor
                </tbody>   
            </table>
        </div>
    </div>
</div>
<!-- <div class="row mt-3">
    <div class="col-md-6">
        <div class="container-fluid">
            <div class="card card-default card-info card-outline">
                <div class="card-header py-2 pl-2">Cuadro-1: Plan curricular segun grado, seccion, curso</div>
                <div class="card-body px-1 pt-2 pb-0">
                    <canvas id="bar" width="150" height="60" class="canvas-bar"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="container-fluid">
            <div class="card card-default card-info card-outline">
                <div class="card-header py-2 pl-2">Cuadro-2: Plan curricular subido por meses</div>
                <div class="card-body px-1 pt-2 pb-0">
                    <canvas id="bar3" width="150" height="60" class="canvas-bar"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="container-fluid">
            <div class="card card-default card-info card-outline">
                <div class="card-header py-2 pl-2">Cuadro-3: Fecha de registros segun docente</div>
                <div class="card-body px-1 pt-2 pb-0">
                    <canvas id="bar4" width="150" height="60" class="canvas-bar"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="container-fluid">
            <div class="card card-default card-info card-outline">
                <div class="card-header py-2 pl-2">Cuadro-4: Fecha de registros segun docente</div>
                <div class="card-body px-1 pt-2 pb-0">
                    <canvas id="bar2" width="150" height="60" class="canvas-bar"></canvas>
                </div>
            </div>
        </div>
    </div>
</div> -->
<script>
   $(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "_TOTAL_ UGEL.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No hay registros de instituciones.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Registros',
                    text: '<i class="fa fa-file-excel p-0 fa-lg"></i>',
                    className:'btn btn-success p-1 excel ml-4',
                },
                {
                    extend: 'pdfHtml5',
                    title: 'Registros',
                    text: '<i class="fa fa-file-pdf p-0 fa-lg"></i>',
                    className:'btn btn-danger p-1',
                },
                {
                    extend: 'print',
                    title: 'Registros',
                    text: '<i class="fa fa-print p-0 fa-lg"></i>',
                    className:'btn btn-info p-1',
                }
            ]
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar ugel');
    } );
</script>

<script>
    var bar = document.getElementById('bar').getContext('2d');
    var bard = {
        label: ["Finalizado", "Proceso", "No iniciado"],
        data: [15, 20, 8,0],
        backgroundColor: [
            '#28a745',
            '#ffc107',
            '#dc3545',
            'rgba(255, 206, 86, 0.2)'
        ],
    };
     
    var objbar = new Chart(bar, {
        type: 'bar',
        data: {
            labels: ["Grado", "Seccion", "Curso"],
            datasets: [bard]
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'index'
            },
            legend: {
                display: false
            },
        }
    });

    var bar = document.getElementById('bar2').getContext('2d');
    var bard2 = {
        label: ["Campo 1", "Campo 2", "Campo 3"],
        data: [5, 10, 12,0],
        backgroundColor: [
            '#28a745',
            '#ffc107',
            '#dc3545',
            'rgba(255, 3, 86, 0.2)'
        ],
    };
     
    var objbar = new Chart(bar, {
        type: 'bar',
        data: {
            labels: ["Campo 1", "Campo 2", "Campo 3"],
            datasets: [bard2]
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'index'
            },
            legend: {
                display: false
            },
        }
    });


    var bar = document.getElementById('bar3').getContext('2d');
    var bard = {
        label: ["Finalizado", "Proceso", "No iniciado"],
        data: [15, 20, 8,0],
        backgroundColor: [
            '#28a745',
            '#ffc107',
            '#dc3545',
            'rgba(255, 206, 86, 0.2)'
        ],
    };
     
    var objbar = new Chart(bar, {
        type: 'line',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                    label: 'Docentes que subieron su P.C.',
                    backgroundColor: '#7f7f7f',
                    borderColor: '#7f7f7f',
                    data: [10, 30, 39, 20, 25, 34, -10],
                    fill: false,
                }, ]
        },
        options: {
                responsive: true,
                title: {
                    display: false,
                    text: 'Grid Line Settings'
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            drawBorder: false,
                            color: ['pink', 'red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'purple']
                        },
                        ticks: {
                            min: 0,
                            max: 50,
                            stepSize: 10
                        }
                    }]
                }
            }
    });
    

    var bar = document.getElementById('bar4').getContext('2d');
    var bard2 = {
        label: ["Campo 1", "Campo 2", "Campo 3"],
        data: [5, 10, 12,0],
        backgroundColor: [
            '#28a745',
            '#ffc107',
            '#dc3545',
            'rgba(255, 206, 86, 0.2)'
        ],
    };
     
    var objbar = new Chart(bar, {
        type: 'radar',
        data: {
            labels: ["Campo 1", "Campo 2", "Campo 3"],
            datasets: [bard2]
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'index'
            },
            legend: {
                display: false
            },
        }
    });
</script>
@endsection