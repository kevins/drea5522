@extends('recursosAprendizaje/template/template')
@section('generalBody')
<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Gestion de recursos de aprendizaje para primaria</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="formInsert" action="{{url('recAprPri/registrar')}}" method="post" enctype="multipart/form-data" class="m-0" novalidate="novalidate">
                <div class="row">
                    <div class="col-md-12 alert alert-info msjIe text-center font-weight-bold p-1 mb-1" style="display: none;"></div>
                    
                    <div class="col-md-3">
                        <div class="form-group mb-2">
	                        <label class="m-0">Ugel:</label>
	                        <select name="ugel" id="ugel" class="form-control form-control-sm">
	                            <option disabled="" selected="">Elija la ugel:</option>
	                            <option value="UGEL Abancay">UGEL Abancay</option>
	                            <option value="UGEL Andahuaylas">UGEL Andahuaylas</option>
	                            <option value="UGEL Antabamba">UGEL Antabamba</option>
	                            <option value="UGEL Aymaraes">UGEL Aymaraes</option>
	                            <option value="UGEL Cotabambas">UGEL Cotabambas</option>
	                            <option value="UGEL Chincheros">UGEL Chincheros</option>
	                            <option value="UGEL Grau">UGEL Grau</option>
	                            <option value="UGEL Huancarama">UGEL Huancarama</option>
	                        </select>
	                    </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group mb-2">
                            <label class="m-0">Categoria:</label>
                            <select name="categoria" id="categoria" class="form-control form-control-sm">
                                <option disabled="" selected="">Elija la categoria:</option>
                                <option value="cat1">EXPERIENCIAS DE APRENDIZAJE URBANO EIB</option>
                                <option value="cat2">EXPERIENCIAS DE APRENDIZAJE REVITALIZACIÓN</option>
                                <option value="cat3">EXPERIENCIAS DE APRENDIZAJE FORTALECIMIENTO</option>
                                <option value="cat4">OTROS</option>
                                <option value="cat5">OTROS (URBANO)</option>
                                <option value="cat6">OTROS (RURAL)</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mb-2">
                            <label class="m-0">Mes:</label>
                            <select name="mes" id="mes" class="form-control form-control-sm">
                                <option disabled="" selected="">Elija el mes:</option>
                                <option value="Enero">Enero</option>
                                <option value="Febrero">Febrero</option>
                                <option value="Marzo">Marzo</option>
                                <option value="Abril">Abril</option>
                                <option value="Mayo">Mayo</option>
                                <option value="Junio">Junio</option>
                                <option value="Julio">Julio</option>
                                <option value="Agosto">Agosto</option>
                                <option value="Setimbre">Setimbre</option>
                                <option value="Octubre">Octubre</option>
                                <option value="Noviembre">Noviembre</option>
                                <option value="Diciembre">Diciembre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 gradoPs">
                        <div class="form-group mb-2">
                            <label class="m-0">Experiencias de aprendizaje:</label>
                            <select name="ide" id="ide" class="form-control form-control-sm">
                                <option disabled selected>Elija la opcion:</option>
                                @foreach($listTexperiencias as $item)
                                <option value="{{$item->ide}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mb-2">
                            <label class="m-0">Grado:</label>
                            <select name="grado" id="grado" class="form-control form-control-sm">
                                <option disabled selected>Grado:</option>
                                <option value="1">1° Grado</option>
                                <option value="2">2° Grado</option>
                                <option value="3">3° Grado</option>
                                <option value="4">4° Grado</option>
                                <option value="5">5° Grado</option>
                                <option value="6">6° Grado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mb-2">
                            <label class="m-0">Curso:</label>
                            <select name="curso" id="curso" class="form-control form-control-sm curso">
                                <option disabled="" selected="">Elija el curso:</option>
                                <option value="otras">Otras</option>
                                <option value="ciencias sociales">Ciencias Sociales</option>
                                <option value="comunicacion">Comunicación</option>
                                <option value="matematica">Matemática</option>
                                <option value="ciencia y tecnologia">Ciencia y Tecnología</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mb-2">
                            <label class="m-0">Orden:</label>
                            <input type="number" name="orden" id="orden" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mb-2">
                            <label class="m-0">Nombre:</label>
                            <input type="text" name="nombre" id="nombre" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="m-0">Descripcion:</label>
                            <textarea name="descripcion" id="descripcion" cols="30" rows="4" class="form-control form-control-sm"></textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Subir archivo en pdf:</label>
                            <input class="form-control form-control-sm" type="file" name="archivoPdf" id="archivoPdf">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Subir archivo en word:</label>
                            <input class="form-control form-control-sm" type="file" name="archivoDoc" id="archivoDoc">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Enlace de archivo:</label>
                            <input class="form-control form-control-sm" type="text" name="earchivo" id="earchivo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="m-0">Enlace de video:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-video"></i></span>
                            </div>
                            <input type="text" name="evideo" id="evideo" class="form-control form-control-sm" placeholder="Video">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="m-0">Enlace de audio:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-video"></i></span>
                            </div>
                            <input type="text" name="eaudio" id="eaudio" class="form-control form-control-sm" placeholder="Audio">
                        </div>
                    </div>
                </div>
                {{csrf_field()}}
             </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar" form="formInsert" class="btn btn-success btn-sm float-right ml-1">
        </div>
    </div>
</div>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Lista de materiales</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <div class="row">
                <div class="col-md-12">
                    <table id="example1" class="table table-sm table-hover m-0 w-100">
                        <thead class="text-center">
                            <tr>
                                <th>Categoria</th>
                                <th>Mes</th>
                                <th>Experiencia</th>
                                <th>Grado</th>
                                <th>Curso</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Archivo</th>
                                <th>Enlaces</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listTrasecundaria as $item)
                            <tr class="text-center">
                                <td>{{$item->categoria}}</td>
                                <td>{{$item->mes}}</td>
                                <td>{{$item->ide}}</td>
                                <td>{{$item->grado}}</td>
                                <td>{{$item->curso}}</td>
                                <td>{{$item->nombre}}</td>
                                <td>{{$item->descripcion}}</td>
                                <td>{{$item->archivoPdf}}</td>
                                <td>
                                    @if($item->earchivo!='')
                                        <a href="{{$item->earchivo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> archivo</a>
                                    @endif
                                    @if($item->evideo!='')
                                        <a href="{{$item->evideo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> video</a>
                                    @endif
                                    @if($item->eaudio!='')
                                        <a href="{{$item->eaudio}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> audio</a>
                                    @endif
                                </td>
                                <td>
                                    <!-- {{asset('filera/pri/pdf').'/'.$item->archivoPdf}} -->
                                    <!-- http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/pdf/{{$item->archivoPdf}} -->
                                    @if($item->archivoPdf!='')
                                        <a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/pri/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
                                    @endif
                                    @if($item->archivoDoc!='')
                                        <a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/pri/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
                                    @endif
                                    <!-- <a href="#" class="btn btn-success btn-xs editar" data-idra="{{$item->idra}}" title="Editar"><i class="fa fa-edit"></i></a> -->
                                    <a href="{{url('/recAprPri/delete').'/'.$item->idras}}" class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash fa-2x"></i><!--  Eliminar --></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('#example1').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );

        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
    $('.tipoMaterial').on('change',function(){
        if($(this).val()=='expApr')
        {
            $('.curso').prop('disabled',true);
            $('.curso').val('');
        }
        else
        {
            $('.curso').prop('disabled',false);
        }
    });
</script>
@endsection