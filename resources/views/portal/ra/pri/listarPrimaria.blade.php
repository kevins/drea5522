@extends('template/templatePortal')
@section('generalBodyPortal')
<div class="container-fluid mt-3" style="padding: 0px 50px;">
	<div class="row justify-content-center align-items-center">
		<!-- justify-content-center align-items-center /conjuntamente con row-->
        <div class="col-md-3">
            <div class="form-group mb-2">
                <label class="m-0">Categoria:</label>
                <select name="categoria" id="categoria" class="form-control form-control-sm">
                    <option disabled="" selected="">Elija la categoria:</option>
                    <!-- <option value="cat1">EXPERIENCIAS DE APRENDIZAJE URBANO EIB</option>
                    <option value="cat2">EXPERIENCIAS DE APRENDIZAJE REVITALIZACIÓN</option> -->
                    <option value="cat3">EXPERIENCIAS DE APRENDIZAJE RURAL</option>
                    <option value="cat5">EXPERIENCIAS DE APRENDIZAJE URBANO</option>
                </select>
            </div>
        </div>
		
        <div class="col-md-3">
            <div class="form-group mb-2">
                <label class="m-0">Grado:</label>
                <select name="grado" id="grado" class="form-control form-control-sm">
                    <option disabled="" selected="">Grado:</option>
                    <option value="1">1° Grado</option>
                    <option value="2">2° Grado</option>
                    <option value="3">3° Grado</option>
                    <option value="4">4° Grado</option>
                    <option value="5">5° Grado</option>
                    <option value="6">6° Grado</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group mb-2">
                <label class="m-0">Periodo de vigencia:</label>
                <select name="mes" id="mes" class="form-control form-control-sm">
                    <option disabled="" selected="">Elija el periodo:</option>
                    <option value="Junio">Del 24 de mayo al 18 de junio</option>
                    <option value="Julio">Del 21 de junio al 23 de julio</option>
                    @foreach($listMes as $item)
                    <!-- <option value="{{$item->mes}}">Del 24 de mayo al 18 de junio</option> -->
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group mb-2">
                <label class="m-0" style="visibility: hidden;">Grado:</label>
	                <a href="#" class="form-control form-control-sm btn btn-success btn-sm buscar"><i class="fa fa-search"></i> Buscar</a>
            </div>
        </div>
        <!-- <div class="col-lg-12">
			<p class="text-center">Experiencias de aprendizaje articuladas por grado a desarrollarse entre el <strong>24 de mayo y el 18 de junio 2021</strong></p>
		</div> -->
        <div class="col-lg-12 msjPeriodoContenedor" style="visibility: hidden;">
            <p class="text-center">Experiencias de aprendizaje articuladas por grado a desarrollarse <strong class="msjPeriodo">24 de mayo y el 18 de junio 2021</strong></p>
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12">
							<h2 class="text-center mensaje">Elija el mes y grado para poder acceder a las experiencias de aprendizaje y fichas de autoaprendizaje</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row contenedorBuscarPri">
		
	</div>
</div>

<script>

	$('.titulo-banner').html('Materiales Educativos 2021 - PRIMARIA');
	$('body').css('background-image','url({{asset('imgSocio/fondo.png')}})');
	$('body').css('background-repeat','round');

    // $('#categoria').on('change',function(){
    //     $('#grado').attr('disabled',$(this).val()!='cat1'?true:false);
    // });

    $('.buscar').on('click',function(){
        $('.msjPeriodoContenedor').css('visibility','inherit');
        $(".msjPeriodo").html($("#mes option:selected").text()+" del 2021");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/raPortal/buscarPrimaria') }}",
            data: {mes:$('#mes').val(),grado:$('#grado').val(),categoria:$('#categoria').val()},
            method: 'get',
            success: function(result){
                if(result.data==false)
                {
                	alert('ingrese los datos');
                }
                else
                {
                	$('.buscarPrimaria').remove();
                	$('.contenedorBuscarPri').append(result);
                	$('.mensaje').html($('#categoria option:selected').html()+' DEL MES DE '+$('#mes').val().toUpperCase());
                	$('.mensaje').addClass('text-primary');
                }
            }
        });
    });
</script>
@endsection
