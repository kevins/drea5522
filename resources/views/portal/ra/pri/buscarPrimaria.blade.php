<div class="row buscarPrimaria">
	<div class="col-lg-12">
		@if($categoria=='cat1')
			@if(COUNT($listEa)=='1')
			<h3 class="text-center mb-4" style="text-decoration: underline;"><a href="{{asset('filera/sec/pdf').'/'.$listEa[0]->archivoPdf}}" target="_blank">{{$listEa[0]->grado}} GRADO : {{$listEa[0]->nombre}}</a></h3>
			@else
			<h3 class="text-center mb-4">{{$grado}} GRADO</h3>
			@endif
		@endif
		@if($categoria=='cat2' || $categoria=='cat3' || $categoria=='cat5')
		<h3 class="text-center mb-4">{{$grado}}º GRADO</h3>
		@endif
	</div>
	@if($categoria=='cat1')
		@if(COUNT($listEa)!='1')
			@if(COUNT($listEa)!='0')
				@include('portal.ra.pri.tablaExperienciasAprendizaje')
			@endif
		@endif
	@else
		@include('portal.ra.pri.tablaExperienciasAprendizaje')
	@endif
	@if($categoria=='cat3' || $categoria=='cat5')
		@include('portal.ra.pri.tarjetasCat3')
	@else
		@include('portal.ra.pri.tarjetasCat12')
	@endif
</div>
<script>
	$(document).ready( function () {
        $('#example1').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay materiales disponibles",
                "sEmptyTable": "No se cuenta con materiales.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar materiales');
        $('.pagination').addClass('m-0');
    } );
</script>