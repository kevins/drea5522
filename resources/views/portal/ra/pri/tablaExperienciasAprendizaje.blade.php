<div class="col-lg-12">
			<h3>EXPERIENCIAS DE APRENDIZAJE</h3>
		</div>
		<div class="col-md-12">
			<div class="card card-primary card-outline">
				<div class="card-body py-2 px-0">
					<table id="example1" class="table table-sm table-hover m-0 w-100">
		                <thead class="text-center">
		                    <tr>
		                        <th>Grado</th>
		                        <th>Nombre</th>
		                        <th>Descripcion</th>
		                        <th>Archivo pdf</th>
		                        <th>Archivo word</th>
		                        <th>Enlaces</th>
		                        <th>Opciones</th>
		                    </tr>
		                </thead>
		                <tbody>
		                    @foreach($listEa as $item)
		                    <tr class="text-center">
		                        <td>{{$item->grado}}º GRADO</td>
		                        <td>{{$item->nombre}}</td>
		                        <td>{{$item->descripcion}}</td>
		                        <td>{{$item->archivoPdf}}</td>
		                        <td>{{$item->archivoDoc}}</td>
		                        <td>
		                            @if($item->earchivo!='')
		                                <a href="{{$item->earchivo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> archivo</a>
		                            @endif
		                            @if($item->evideo!='')
		                                <a href="{{$item->evideo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> video</a>
		                            @endif
		                            @if($item->eaudio!='')
		                                <a href="{{$item->eaudio}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> audio</a>
		                            @endif
		                        </td>
		                        <td>
		                            @if($item->archivoPdf!='')
		                                <a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileea/pri/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
		                            @endif
		                            @if($item->archivoDoc!='')
		                                <a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileea/pri/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
		                            @endif
		                        </td>
		                    </tr>
		                    @endforeach
		                </tbody>
		            </table>
				</div>
			</div>
		</div>