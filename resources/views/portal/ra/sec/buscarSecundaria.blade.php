<div class="row buscarSecundaria">
	<div class="col-lg-12">
		<h3 class="text-center mb-4">{{$grado}} GRADO</h3>
	</div>
	<div class="col-lg-12">
		<h3>EXPERIENCIAS DE APRENDIZAJE</h3>
	</div>
	<div class="col-md-12">
		<div class="card card-primary card-outline">
			<div class="card-body py-2 px-0">
				<table id="example1" class="table table-sm table-hover m-0 w-100">
	                <thead class="text-center">
	                    <tr>
	                        <th>Nombre</th>
	                        <th>Descripcion</th>
	                        <th>Archivo pdf</th>
	                        <th>Archivo word</th>
	                        <th>Enlaces</th>
	                        <th>Opciones</th>
	                        <!-- <th>Fichas autoaprendizaje</th> -->
	                    </tr>
	                </thead>
	                <tbody>
	                    <tr class="text-center">
	                        <td>{{$listEa->nombre}}</td>
	                        <td>{{$listEa->descripcion}}</td>
	                        <td>{{$listEa->archivoPdf}}</td>
	                        <td>{{$listEa->archivoDoc}}</td>
	                        <td>
	                            @if($listEa->earchivo!='')
	                                <a href="{{$listEa->earchivo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> archivo</a>
	                            @endif
	                            @if($listEa->evideo!='')
	                                <a href="{{$listEa->evideo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> video</a>
	                            @endif
	                            @if($listEa->eaudio!='')
	                                <a href="{{$listEa->eaudio}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> audio</a>
	                            @endif
	                        </td>
	                        <td>
	                        	<!-- http://pcr.dreapurimac.gob.pe/laravel/public/fileea/pri/doc/{{$listEa->archivoDoc}} -->
	                            @if($listEa->archivoPdf!='')
	                                <a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileea/sec/pdf/{{$listEa->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
	                            @endif
	                            @if($listEa->archivoDoc!='')
	                                <a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileea/sec/doc/{{$listEa->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
	                            @endif
	                        </td>
	                        <!-- <td>
	                        	<a href="#" class="btn btn-success btn-sm mostrarFichas" data-mes="{{$mes}}" data-grado="{{$grado}}" data-ide="{{$listEa->ide}}">Ver fichas</a>
	                        </td> -->
	                    </tr>
	                </tbody>
	            </table>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<h3>FICHAS DE AUTOAPRENDIZAJE</h3>
	</div>
	<!-- <div class="col-lg-12 borrarMsj">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<h2 class="text-center text-primary">Seleccione experiencia de aprendizaje, pulsando en <span class="btn btn-success btn-sm">Ver fichas</span></h2>
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-3">
				<div class="card card-danger card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-danger">Ciencias Sociales</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaCs)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaCs as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								@if($item->archivoDoc!='')
								<!-- http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/doc/{{$item->archivoDoc}} -->
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<!-- http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/pdf/{{$item->archivoPdf}} -->
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card card-info card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-info">Comunicación</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaComu)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaComu as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								@if($item->archivoDoc!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card card-success card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-success">Matemática</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaMate)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaMate as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								@if($item->archivoDoc!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card card-warning card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-warning">Ciencia y Tecnología</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaCyt)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaCyt as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								@if($item->archivoDoc!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12 contenedorBuscarFichasSec">
		
	</div>
</div>
<script>
	$(document).ready( function () {
        $('#example1').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay materiales disponibles",
                "sEmptyTable": "No se cuenta con materiales.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar materiales');
        $('.pagination').addClass('m-0');
    } );
    
    $('.mostrarFichas').on('click',function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/raPortal/buscarSecundariaFichas') }}",
            data: {mes:$(this).attr('data-mes'),grado:$(this).attr('data-grado'),ide:$(this).attr('data-ide')},
            method: 'get',
            success: function(result){
                console.log(result);
                if(result.data==false)
                {
                	alert('ingrese los datos');
                }
                else
                {
                	$('.buscarSecundariaFichas').remove();
                	$('.contenedorBuscarFichasSec').append(result);
                	$('.borrarMsj').remove();
                	
                	// $('.mensajeFichas').html('Material de aprendizaje de '+$('#mes').val());
                	// $('.mensajeFichas').addClass('text-primary');
                }
            }
        });
    });
</script>