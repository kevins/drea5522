<div class="col-lg-6">
	<!-- Educación Intercultural Bilingüe -->
	<p class="px-3 mx-4" style="border-bottom: 2px solid #fdc500;"><strong>EIB: QUECHUA VARIANTE, CHANKAS Y COLLAO</strong></p>
	<div class="mb-4">
		<table id="listLenguasIni" style="width: 100%;" class="table-bordered">
			<thead class="bg-info">
				<tr class="text-center">
					<!-- <th>N°</th> -->
					<th>Nombre</th>
					<th>Comentario</th>
					<th>Archivo</th>
				</tr>
			</thead>
			<tbody>
				<tr class="text-center">
					<td>Experiencias de aprendizaje | COLLAO</td>
					<td>El mismo material, le mostrara para todas las semanas,SIEMBRA QILLAW...</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/ini/ea_oct_collao.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>El mismo material, le mostrara para todas las semanas</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/ini/eib_collao_ant.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>El mismo material, le mostrara para todas las semanas</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/ini/eib_collao_cot.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>El mismo material, le mostrara para todas las semanas</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/ini/eib_collao_gra.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>