<div class="col-lg-6">
	<!-- Educación Intercultural Bilingüe -->
	<p class="px-3 mx-4" style="border-bottom: 2px solid #fdc500;"><strong style="font-size: 0.6rem;">EIB: QUECHUA VARIANTE, CHANKAS Y COLLAO</strong> <strong style="font-size: 0.8rem;">El mismo material, le mostrara para todas las semanas</strong></p>
	<div class="mb-4">
		<table id="listLenguasPri" style="width: 100%;" class="table-bordered">
			<thead class="bg-info">
				<tr class="text-center">
					<th>Nombre</th>
					<th>Comentario</th>
					<th>Archivo</th>
				</tr>
			</thead>
			<tbody>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>PRIMER grado, antabamba</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/1g_eib_ant.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>PRIMER grado, cotabambas</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/1g_eib_cot.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>PRIMER grado, grau</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/1g_eib_gra.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
<!-- -------- -->
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>SEGUNDO grado, antabamba</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/2g_eib_ant.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>SEGUNDO grado, cotabambas</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/2g_eib_cot.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>SEGUNDO grado, grau</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/2g_eib_gra.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
<!-- -------- -->
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>TERCERO grado, antabamba</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/3g_eib_ant.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>TERCERO grado, cotabambas</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/3g_eib_cot.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>TERCERO grado, grau</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/3g_eib_gra.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
<!-- -------- -->
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>CUARTO grado, antabamba</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/4g_eib_ant.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>CUARTO grado, cotabambas</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/4g_eib_cot.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>CUARTO grado, grau</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/4g_eib_gra.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
<!-- -------- -->
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>QUINTO grado, antabamba</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/5g_eib_ant.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>QUINTO grado, cotabambas</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/5g_eib_cot.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>QUINTO grado, grau</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/5g_eib_gra.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
<!-- -------- -->
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>SEXTO grado, antabamba</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/6g_eib_ant.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>SEXTO grado, cotabambas</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/6g_eib_cot.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
				<tr class="text-center">
					<td>EIB | COLLAO</td>
					<td>SEXTO grado, grau</td>
					<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/pri/6g_eib_gra.pdf" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
				</tr>
<!-- -------- -->
			</tbody>
		</table>
	</div>
</div>