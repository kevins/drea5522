@extends('template/templatePortal')
@section('generalBodyPortal')

<div class="p-1" >
	<div class="container-fluid">
		<div class="col-lg-12 text-center font-weight-bold">
			<p style="font-size: 1.2rem; text-shadow: 2px 2px 2px #e2c4c4;">Los equipos de convivencia de la DREA y UGEL de la region de apurimac desde el año 2020 ante la situacion de emergencia sanitaria mundial, ofrece recursos tecnologicos de contenido socioemocional, para atender a estudiantes, docentes y padres de familia integrantes de la comunidad educativa; los recursos contemplan el contexto culturaly linguistico de la dinamica educativa local considerando la siguiente categorizacion:</p>
		 </div>
	</div>
	<div class="container-fluid p-4 font-weight-bold">
	    <div class="card card-default card-info card-outline" style="background: rgb(218 211 131 / 50%);">
	        <div class="card-header py-2 pl-2">
	            <p class="card-title font-weight-bold text-center w-100">{{$nombre}}</p>
	        </div>
	        <div class="card-body p-0 mt-3">
	            <div class="row">
	                <div class="col-md-12">
	                    <table id="example1" class="table table-sm table-hover m-0 w-100">
	                        <thead class="text-center">
	                            <tr>
	                                <th>Ugel</th>
	                                <th>Categoria</th>
	                                <th>Nombre</th>
	                                <th>Descripcion</th>
	                                <th>Archivo</th>
	                                <th>Enlaces</th>
	                                <th>Descargar</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($list as $item)
	                            <tr class="text-center">
	                                <td>{{$item->ugel}}</td>
	                                <td>{{$item->categoria}}</td>
	                                <td>{{$item->nombre}}</td>
	                                <td>{{$item->descripcion}}</td>
	                                <td>{{$item->archivo}}</td>
	                                <td>
	                                    @if($item->earchivo!='')
	                                        <a href="{{$item->earchivo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> archivo</a>
	                                    @endif
	                                    @if($item->evideo!='')
	                                        <a href="{{$item->evideo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> video</a>
	                                    @endif
	                                    @if($item->eaudio!='')
	                                        <a href="{{$item->eaudio}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> audio</a>
	                                    @endif
	                                </td>
	                                <td>
	                                    @if($item->archivo!='')
	                                    <!-- http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/{{$item->nombrereal}}.{{$item->formato}} -->
	                                        <a href="http://pcr.dreapurimac.gob.pe/laravel/public/filesocioemocional/{{$item->archivo}}" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-file"></i> archivo</a>
	                                    @endif
	                                </td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
		
	</div>
	<script>
		$('.titulo-banner').html('APOYO SOCIOEMOCIONAL');
		$('body').css('background-image','url({{asset('imgSocio/fondo.png')}})');
		$('body').css('background-repeat','round');
    $(document).ready( function () {
        $('#example1').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay materiales disponibles",
                "sEmptyTable": "No se cuenta con materiales.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar materiales');
    } );
</script>
@endsection
