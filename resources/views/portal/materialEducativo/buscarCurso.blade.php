﻿<style>
	.semana:hover{background: #ccc1c1 !important;cursor: pointer;}
	.actSemana{background:blue;}
</style>
<div class="container bloque">
	<div class="container text-center mt-4 ">
		
		<span class="badge badge-secondary py-2 px-4 semana actSemana" style="font-size: 0.9rem;" data-semana="{{$data.'-'}}semana1" onclick="buscarSemanaNew($(this).attr('data-semana'))"><strong>Semana 1</strong></span>
		<span class="badge badge-secondary py-2 px-4 semana" style="font-size: 0.9rem;" data-semana="{{$data.'-'}}semana2" onclick="buscarSemanaNew($(this).attr('data-semana'))"><strong>Semana 2</strong></span>
		<span class="badge badge-secondary py-2 px-4 semana" style="font-size: 0.9rem;" data-semana="{{$data.'-'}}semana3" onclick="buscarSemanaNew($(this).attr('data-semana'))"><strong>Semana 3</strong></span>
		<span class="badge badge-secondary py-2 px-4 semana" style="font-size: 0.9rem;" data-semana="{{$data.'-'}}semana4" onclick="buscarSemanaNew($(this).attr('data-semana'))"><strong>Semana 4</strong></span>
	</div>
	<div class="container mt-4 containerMaterialBuscado">
		<div class="row">
			<div class="col-lg-6">
				<p class="px-3 mx-4" style="border-bottom: 2px solid #fdc500;"><strong>Guías de actividades</strong></p>
				<div class="mb-4">
					<table id="listGuias" style="width: 100%;" class="table-bordered">
						<thead class="bg-info">
							<tr class="text-center">
								<!-- <th>N°</th> -->
								<th>Nombre</th>
								<th>Comentario</th>
								<th>Archivo</th>
							</tr>
						</thead>
						<tbody>
							@foreach($listGuias as $item)
								<tr class="text-center">
									<td>{{$item->nombre}}</td>
									<td>{{$item->comentario==' '?'--':$item->comentario}}</td>
									<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/{{$item->nombrereal}}.{{$item->formato}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-lg-6">
				<p class="px-3 mx-4" style="border-bottom: 2px solid #fdc500;"><strong>Recursos</strong></p>
				<div class="mb-4">
					<table id="listRecursos" style="width: 100%;" class="table-bordered">
						<thead class="bg-info">
							<tr class="text-center">
								<!-- <th>N°</th> -->
								<th>Nombre</th>
								<th>Comentario</th>
								<th>Archivo</th>
							</tr>
						</thead>
						<tbody>
							@foreach($listRecursos as $item)
								<tr class="text-center">
									<td>{{$item->nombre}}</td>
									<td>{{$item->comentario==' '?'--':$item->comentario}}</td>
									<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/{{$item->nombrereal}}.{{$item->formato}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-lg-6">
				<p class="px-3 mx-4" style="border-bottom: 2px solid #fdc500;"><strong>Otros (Experiencias, Orientaciones entre otros)</strong></p>
				<div class="mb-4">
					<table id="listOtros" style="width: 100%;" class="table-bordered">
						<thead class="bg-info">
							<tr class="text-center">
								<!-- <th>N°</th> -->
								<th>Nombre</th>
								<th>Comentario</th>
								<th>Archivo</th>
							</tr>
						</thead>
						<tbody>
							@foreach($listOtros as $item)
								<tr class="text-center">
									<td>{{$item->nombre}}</td>
									<td>{{$item->comentario==' '?'--':$item->comentario}}</td>
									<td><a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/{{$item->nombrereal}}.{{$item->formato}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready( function () {
        $('#listGuias').DataTable( {
        	"destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('#listRecursos').DataTable( {
        	"destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('#listOtros').DataTable( {
        	"destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
</script>