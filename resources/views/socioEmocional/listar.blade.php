@extends('socioEmocional/template/template')
@section('generalBody')
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Lista de materiales</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <div class="row">
                <div class="col-md-12">
                    <table id="example1" class="table table-sm table-hover m-0 w-100">
                        <thead class="text-center">
                            <tr>
                                <th>Ugel</th>
                                <th>Categoria</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Archivo</th>
                                <th>Enlaces</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list as $item)
                            <tr class="text-center">
                                <td>{{$item->ugel}}</td>
                                <td>{{$item->categoria}}</td>
                                <td>{{$item->nombre}}</td>
                                <td>{{$item->descripcion}}</td>
                                <td>{{$item->archivo}}</td>
                                <td>
                                    @if($item->earchivo!='')
                                        <a href="{{$item->earchivo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> archivo</a>
                                    @endif
                                    @if($item->evideo!='')
                                        <a href="{{$item->evideo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> video</a>
                                    @endif
                                    @if($item->eaudio!='')
                                        <a href="{{$item->eaudio}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> audio</a>
                                    @endif
                                </td>
                                <!-- <td>{{$item->evideo}}</td>
                                <td>{{$item->eaudio}}</td> -->
                                <td>
                                    @if($item->archivo!='')
                                        <a href="http://pcr.dreapurimac.gob.pe/laravel/public/filesocioemocional/{{$item->archivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Descargar"><i class="fa fa-download"></i><!--  Descargar --></a>
                                    @endif
                                    <a href="#" class="btn btn-success btn-xs editar" data-idse="{{$item->idse}}" title="Editar"><i class="fa fa-edit"></i><!--  Editar --></a>
                                    <a href="{{url('/socioEmocional/delete').'/'.$item->idse}}" class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash"></i><!--  Eliminar --></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalEditar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header py-1">
                <h5 class="modal-title" id="exampleModalLabel">Editar registro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row text-center">
                    <div class="col-lg-6 ">
                        <div class="alert alert-warning py-0">
                            <p class="m-0 font-weight-bold">Al subir un nuevo archivo se eliminara el anterior archivo.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 ">
                        <div class="alert alert-info py-0">
                            <p class="m-0"><span class="btn btn-secundary p-1"><i class="fa fa-eraser" style="cursor: pointer;"></i></span> Boton para eliminar el enlace.</p>
                        </div>
                    </div>
                </div>
                <form id="formEditar" action="{{url('socioEmocional/editar')}}" method="post" enctype="multipart/form-data" class="m-0" novalidate="novalidate">
                    <input type="hidden" name="idse" id="idse">
                    <div class="row">
                        <div class="col-md-12 alert alert-info msjIe text-center font-weight-bold p-1 mb-1" style="display: none;"></div>
                        <div class="col-md-4">
                            <div class="form-group mb-2">
                                <label class="m-0">Ugel:</label>
                                <select name="ugel" id="ugel" class="form-control form-control-sm">
                                    <option disabled="" selected="">Elija la ugel:</option>
                                    <option value="UGEL Abancay">UGEL Abancay</option>
                                    <option value="UGEL Andahuaylas">UGEL Andahuaylas</option>
                                    <option value="UGEL Antabamba">UGEL Antabamba</option>
                                    <option value="UGEL Aymaraes">UGEL Aymaraes</option>
                                    <option value="UGEL Cotabambas">UGEL Cotabambas</option>
                                    <option value="UGEL Chincheros">UGEL Chincheros</option>
                                    <option value="UGEL Grau">UGEL Grau</option>
                                    <option value="UGEL Huancarama">UGEL Huancarama</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-2">
                                <label class="m-0">Categoria:</label>
                                <select name="categoria" id="categoria" class="form-control form-control-sm">
                                    <option disabled="" selected="">Elija la categoria:</option>
                                    <option value="La persona">La persona</option>
                                    <option value="Trabajo en equipo">Trabajo en equipo</option>
                                    <option value="Las emociones">Las emociones</option>
                                    <option value="Alertas">Alertas</option>
                                    <option value="La familia">La familia</option>
                                    <option value="COVID">COVID</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-2">
                                <label class="m-0">Nombre:</label>
                                <input type="text" name="nombre" id="nombre" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="m-0">Descripcion:</label>
                                <textarea name="descripcion" id="descripcion" cols="30" rows="4" class="form-control form-control-sm"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="m-0">Subir archivo: <a class="text-success carchivo" target="_blank" style="display: none;">(ver archivo)</a><span class="text-warning sarchivo" style="display: none;">(registro sin archivo)</span></label>
                                <input class="form-control form-control-sm" type="file" name="archivo" id="archivo">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="m-0">Enlace de archivo:</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text limpiarLink" style="cursor: pointer;"><i class="fa fa-eraser"></i></span>
                                    <span class="input-group-text"><i class="fa fa-link"></i></span>
                                </div>
                                <input type="text" name="earchivo" id="earchivo" class="form-control form-control-sm" placeholder="Archivo">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="m-0">Enlace de video:</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text limpiarLink" style="cursor: pointer;"><i class="fa fa-eraser"></i></span>
                                    <span class="input-group-text"><i class="fa fa-link"></i></span>
                                </div>
                                <input type="text" name="evideo" id="evideo" class="form-control form-control-sm" placeholder="Video">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="m-0">Enlace de audio:</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text limpiarLink" style="cursor: pointer;"><i class="fa fa-eraser"></i></span>
                                    <span class="input-group-text"><i class="fa fa-link"></i></span>
                                </div>
                                <input type="text" name="eaudio" id="eaudio" class="form-control form-control-sm" placeholder="Audio">
                            </div>
                        </div>
                    </div>
                    {{csrf_field()}}
                </form>
            </div>
            <div class="modal-footer py-1">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
                <button type="submit" form="formEditar" class="btn btn-success btn-sm">Guardar cambios</button>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready( function () {
        $('#example1').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );

        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
    $('.editar').on('click',function(){
        
        var idse=$(this).attr('data-idse');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/socioEmocional/editar') }}",
            data: {idse:idse},
            method: 'get',
            success: function(result){
                console.log(result);
                $('#idse').val(result.data.idse);
                $('#ugel').val(result.data.ugel);
                $('#categoria').val(result.data.categoria);
                $('#nombre').val(result.data.nombre);
                $('#descripcion').val(result.data.descripcion);
                if(result.data.archivo!=null)
                {
                    $('.carchivo').css('display','inherit');
                    $('.sarchivo').css('display','none');
                    $('.carchivo').attr('href','http://pcr.dreapurimac.gob.pe/laravel/public/filesocioemocional/'+result.data.archivo);
                }
                else
                {
                    $('.sarchivo').css('display','inherit');
                    $('.carchivo').css('display','none');
                }
                $('#earchivo').val(result.data.earchivo);
                $('#evideo').val(result.data.evideo);
                $('#eaudio').val(result.data.eaudio);
                $('#modalEditar').modal('show');
            }
        });
    });
    $('.limpiarLink').on('click',function(){
        $(this).parent().parent().find('input').val('');
    });
</script>
@endsection