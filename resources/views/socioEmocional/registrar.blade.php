@extends('socioEmocional/template/template')
@section('generalBody')
<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Gestion de material</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="formInsert" action="{{url('socioEmocional/registrar')}}" method="post" enctype="multipart/form-data" class="m-0" novalidate="novalidate">
                <!-- justify-content-center align-items-center /conjuntamente con row-->
                <div class="row">
                    <div class="col-md-12 alert alert-info msjIe text-center font-weight-bold p-1 mb-1" style="display: none;"></div>
                    <div class="col-md-4">
                        <div class="form-group mb-2">
	                        <label class="m-0">Ugel:</label>
	                        <select name="ugel" id="ugel" class="form-control form-control-sm">
	                            <option disabled="" selected="">Elija la ugel:</option>
	                            <option value="UGEL Abancay">UGEL Abancay</option>
	                            <option value="UGEL Andahuaylas">UGEL Andahuaylas</option>
	                            <option value="UGEL Antabamba">UGEL Antabamba</option>
	                            <option value="UGEL Aymaraes">UGEL Aymaraes</option>
	                            <option value="UGEL Cotabambas">UGEL Cotabambas</option>
	                            <option value="UGEL Chincheros">UGEL Chincheros</option>
	                            <option value="UGEL Grau">UGEL Grau</option>
	                            <option value="UGEL Huancarama">UGEL Huancarama</option>
	                        </select>
	                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group mb-2">
                            <label class="m-0">Categoria:</label>
                            <select name="categoria" id="categoria" class="form-control form-control-sm">
                                <option disabled="" selected="">Elija la categoria:</option>
                                <option value="La persona">La persona</option>
                                <option value="Trabajo en equipo">Trabajo en equipo</option>
                                <option value="Las emociones">Las emociones</option>
                                <option value="Alertas">Alertas</option>
                                <option value="La familia">La familia</option>
                                <option value="COVID">COVID</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group mb-2">
                            <label class="m-0">Nombre:</label>
                            <input type="text" name="nombre" id="nombre" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="m-0">Descripcion:</label>
                            <textarea name="descripcion" id="descripcion" cols="30" rows="4" class="form-control form-control-sm"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="m-0">Subir archivo:</label>
                            <input class="form-control form-control-sm" type="file" name="archivo" id="archivo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="m-0">Enlace de archivo:</label>
                            <input class="form-control form-control-sm" type="text" name="earchivo" id="earchivo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="m-0">Enlace de video:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-video"></i></span>
                            </div>
                            <input type="text" name="evideo" id="evideo" class="form-control form-control-sm" placeholder="Video">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="m-0">Enlace de audio:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-video"></i></span>
                            </div>
                            <input type="text" name="eaudio" id="eaudio" class="form-control form-control-sm" placeholder="Audio">
                        </div>
                    </div>
                </div>
                {{csrf_field()}}
             </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar" form="formInsert" class="btn btn-success btn-sm float-right ml-1">
            <!-- <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1 limpiar"> -->
        </div>
    </div>
</div>
@endsection