@extends('template/template')
@section('generalBody')
<style>
    .selectedRow{background: rgba(1, 1, 1, 0.22);}
    .select2-selection.select2-selection--single{padding-top: .20rem!important;}
</style>
<meta name="_token" content="{{csrf_token()}}" />
@if(session()->has('estado'))
    <div class="alert alert-info mt-3 text-center font-weight-bold p-1">
    {{session()->get('estado')}}
    </div>
@endif
<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Gestion de material</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="formInsert" action="{{url('fileSpecialist/insertMaterial')}}" method="post" enctype="multipart/form-data" class="m-0">
                <input type="hidden" id="idarchivospecialist" name="idarchivospecialist">
                <!-- justify-content-center align-items-center /conjuntamente con row-->
                <div class="row">
                    <div class="col-md-12">
                        <label class="m-0">Ugel:</label>
                        <div class="form-control form-control-sm text-center">
                            <input type="radio" name="ugel" id="ugel1" value="Aymaraes"> 
                            <label for="ugel1">Ugel Aymaraes</label>
                            <input type="radio" name="ugel" id="ugel2" value="Chincheros"> 
                            <label for="ugel2">Ugel Chincheros</label>
                            <input type="radio" name="ugel" id="ugel3" value="Abancay"> 
                            <label for="ugel3">Ugel Abancay</label>
                            <input type="radio" name="ugel" id="ugel4" value="Antabamba"> 
                            <label for="ugel4">Ugel Antabamba</label>
                            <input type="radio" name="ugel" id="ugel5" value="Cotabambas"> 
                            <label for="ugel5">Ugel Cotabambas</label>
                            <input type="radio" name="ugel" id="ugel6" value="Andahuaylas"> 
                            <label for="ugel6">Ugel Andahuaylas</label>
                            <input type="radio" name="ugel" id="ugel7" value="Grau"> 
                            <label for="ugel7">Ugel Grau</label>
                            <input type="radio" name="ugel" id="ugel8" value="Huancarama"> 
                            <label for="ugel8">Ugel Huancarama</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="m-0">Semana:</label>
                        <div class="form-control form-control-sm text-center">
                            <input type="radio" name="semana" id="semana1" value="semana9"> 
                            <label for="semana1">1-SEMANA</label>
                            <input type="radio" name="semana" id="semana2" value="semana10"> 
                            <label for="semana2">2-SEMANA</label>
                            <input type="radio" name="semana" id="semana3" value="semana11"> 
                            <label for="semana3">3-SEMANA</label>
                            <input type="radio" name="semana" id="semana4" value="semana12"> 
                            <label for="semana4">4-SEMANA</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="m-0">Tipo de guia:</label>
                        <div class="form-control form-control-sm text-center">
                            <input type="radio" name="contenido" id="guias" value="guias"> 
                            <label for="guias">Guías de actividades</label>
                            <input type="radio" name="contenido" id="recursos" value="recursos"> 
                            <label for="recursos">Recursos</label>
                            <input type="radio" name="contenido" id="otros" value="otros"> 
                            <label for="otros">Otros(Experiencias,orientaciones)</label>
                        </div>
                    </div>
                    <div class="{{session()->get('Person')->tEspecialista->nivel!='Inicial' && session()->get('Person')->tEspecialista->nivel!='Primaria'?'col-md-4':'col-md-6'}}">
                        <div class="form-group mb-2">
                            <label class="m-0">Nivel:</label>
                            <select name="nivel" id="nivel" class="form-control form-control-sm">
                                <option disabled selected>Elija el nivel:</option>
                                <option value="inicial">Inicial</option>
                                @if(session()->get('Person')->tEspecialista->nivel!='Inicial')
                                <option value="primaria">Primaria</option>
                                <option value="secundaria">Secundaria</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    @if(session()->get('Person')->tEspecialista->nivel=='Inicial')
                    <div class="col-md-6">
                        <div class="form-group mb-2">
                            <label class="m-0">Edad:</label>
                            <select name="grado" id="grado" class="form-control form-control-sm">
                                <option disabled selected>Edad:</option>
                                <option value="0">0 Años</option>
                                <option value="1">1 Años</option>
                                <option value="2">2 Años</option>
                                <option value="3">3 Años</option>
                                <option value="4">4 Años</option>
                                <option value="5">5 Años</option>
                            </select>
                        </div>
                    </div>
                    @else
                    <div class="{{session()->get('Person')->tEspecialista->nivel!='Inicial' && session()->get('Person')->tEspecialista->nivel!='Primaria'?'col-md-4':'col-md-6'}}">
                        <div class="form-group mb-2">
                            <label class="m-0">Grado:</label>
                            <select name="grado" id="grado" class="form-control form-control-sm">
                                <option disabled selected>Grado:</option>
                                <option value="1">1° Grado</option>
                                <option value="2">2° Grado</option>
                                <option value="3">3° Grado</option>
                                <option value="4">4° Grado</option>
                                <option value="5">5° Grado</option>
                                <option value="6">6° Grado</option>
                            </select>
                        </div>
                    </div>
                    @endif
                    @if(session()->get('Person')->tEspecialista->nivel!='Inicial' && session()->get('Person')->tEspecialista->nivel!='Primaria')
                    <div class="col-md-4">
                        <div class="form-group mb-2">
                            <label class="m-0">Curso:</label>
                            <select name="curso" id="curso" class="form-control form-control-sm">
                                <option disabled selected>Grado:</option>
                                <option value="mate">Matematica</option>
                                <option value="comu">Comunicacion</option>
                                <option value="csoc">Ciencias sociales</option>
                                <option value="ctec">Ciencia y tecnologia</option>
                                <option value="dper">Desarrollo Personal, Ciudadanía y Cívica</option>
                                <!-- <option value="cciv">Desarrollo Personal, Ciudadanía y Cívica</option> -->
                            </select>
                        </div>
                    </div>
                    @endif
                    <!-- @if(session()->get('Person')->tEspecialista->nivel=='General')
                    <div class="col-md-12">
                        <div class="form-group mb-2">
                            <label class="m-0">Nivel:</label>
                            <select name="nivel" id="nivel" class="form-control form-control-sm">
                                <option disabled selected>Elija el nivel:</option>
                                <option value="inicial">Inicial</option>
                                <option value="primaria">Primaria</option>
                                <option value="secundaria">Secundaria</option>
                            </select>
                        </div>
                    </div>
                    @else
                    <input type="hidden" name="nivel" id="nivel" value="{{session()->get('Person')->tEspecialista->nivel}}">
                    @endif -->
                    
                    <div class="col-md-6">
                        <div class="form-group mb-2">
                            <label class="m-0">Nombre:</label>
                            <input type="text" name="nombre" id="nombre" class="form-control form-control-sm">
                        </div>
                        <div class="form-group">
                            <label class="m-0">Archivo:</label>
                            <input class='form-control form-control-sm' type="File" name="file" id="file">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="m-0">Comentario:</label>
                            <textarea name="comentario" id="comentario" cols="30" rows="4" class="form-control form-control-sm"></textarea>
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <label class="m-0">Link del video:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-video"></i></span>
                            </div>
                            <input type="text" name="video" id="video" class="form-control form-control-sm" placeholder="video">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label class="m-0">Estado:</label>
                        <div class="form-control form-control-sm text-center">
                            <input type="checkbox" name="publico" id="publico" value="publico"> 
                            <label for="publico">Público</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label class="m-0">Compartir:</label>
                        <div class="form-control form-control-sm text-center">
                            <input type="checkbox" name="compartir" id="compartir" value="compartir"> 
                            <label for="compartir">Con especialistas</label>
                        </div>
                    </div> -->
                </div>
                {{csrf_field()}}
             </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar" form="formInsert" class="btn btn-success btn-sm float-right ml-1 btnSubmit">
            <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1 limpiar">
        </div>
    </div>
</div>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Lista de materiales</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <div class="row">
                <div class="col-md-12">
                    <table id="example1" class="table table-sm table-hover m-0 w-100">
                        <thead class="text-center">
                            <tr>
                                <th>Ugel</th>
                                <th>Semana</th>
                                <th>Tipo</th>
                                <th>Nivel</th>
                                <th>Edad/grado</th>
                                <th>Curso</th>
                                <th>Nombre</th>
                                <!-- <th>Formato</th> -->
                                <!-- <th>Peso</th> -->
                                <th>Registrado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listTae as $item)
                            <tr class="text-center">
                                <td class="bg-secondary">{{$item->ugel}}</td>
                                <td class="bg-secondary">{{$item->semana}}</td>
                                <td class="bg-secondary">{{$item->contenido}}</td>
                                <td class="bg-secondary">{{$item->cnivel}}</td>
                                <td class="bg-secondary">{{$item->grado}}</td>
                                <td>
                                    @if($item->curso=='comu')Comunicacion @endif
                                    @if($item->curso=='mate')Matematica @endif
                                    @if($item->curso=='csoc')Ciencias sociales @endif
                                    @if($item->curso=='ctec')Ciencias y tecnologia @endif

                                    @if($item->curso=='dper')Desarrollo Personal, Ciudadanía y Cívica @endif
                                </td>
                                <td>{{$item->nombre}}</td>
                                <!-- <td>
                                    @if($item->formato!='')
                                    <span class="fa {{$item->formato=='pdf'?'fa-file-pdf':''}}
                                    {{$item->formato=='docx'?'fa-file-word':''}}
                                    {{$item->formato=='xls'?'fa-file-excel':''}}"></span> {{$item->formato}}
                                    @else- -
                                    @endif
                                </td> -->
                                <!-- <td>@if($item->peso!=''){{$item->peso}}
                                @else- -
                                @endif
                                </td> -->
                                <td>{{$item->createddate}}</td>
                                <td>
                                    @if($item->status==1)
                                    <span class="btn btn-xs" data-toggle="tooltip" title="Publico"><i class="fa fa-users"></i></span>
                                    @else
                                    <span class="btn btn-xs" data-toggle="tooltip" title="Privado"><i class="fa fa-user"></i></span>
                                    @endif
                                    <!-- <a href="#" class="edit btn btn-primary btn-xs" data-id="{{$item->idarchivospecialist}}" data-toggle="tooltip" title="Editar registro"><i class="fa fa-edit"></i></a> -->

                                    <span class="btn btn-info btn-xs fab fa-rocketchat" data-toggle="tooltip" title="Comentario/observacion: {{$item->comentario}}"></span>
                                    @if(strpbrk($item->nombrereal, '-')!='')
                                    <a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileSpecialist/{{$item->nombrereal}}.{{$item->formato}}" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-download"></i></a>
                                    @endif
                                    <a href="{{url('/fileSpecialist/delete').'/'.$item->idarchivospecialist}}" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Borrar registro"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#formInsert").validate({
        errorClass: "text-danger font-italic font-weight-normal",
        ignore: ".ignore",
        rules: {
            nombre: "required",
            file: "required",
            nivel: "required",
            grado: "required",
            semana: "required",
            contenido: "required",
        },
        messages: {
            nombre: "Ingrese el nombre",
            file: "Ingrese el archivo",
            nivel: "Ingrese el nivel",
            grado: "Ingrese el grado",
            semana: "Seleccione una opción",
            contenido: "Seleccione una opción",
        },
    });
</script>
<script>
    $('.limpiar').on('click',function(){
        $('#formInsert').attr('action','{{url('fileSpecialist/insert')}}');
        $('#nombre').val('');
        $('#comentario').val('');
        $('#video').val('');
        $('#nivel option:nth-child(1)').prop("selected",true);
        $('#ies').empty();
        $("#ies").append("<option disabled selected>Primero seleccione nivel:</option>");
        $('#file').val('');
        $("#publico").prop('checked',false);
        $("#compartir").prop('checked',false);
        $('table>tbody').find('.selectedRow').removeClass('selectedRow');
    });
    
    $('#file').on('change',function(){
        var name = $('#file')[0].files[0].name;
        $('#nombre').val(name.split('.')[0]);
    });
    $('.edit').on('click',function(){
        // alert($(this).attr('data-id'));
        $('table>tbody').find('.selectedRow').removeClass('selectedRow');
        $(this).parent().parent().addClass('selectedRow');
        $(this).attr('data-id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSpecialist/editMaterial') }}",
            data: {idarchivospecialist:$(this).attr('data-id')},
            method: 'get',
            success: function(result){
                $('#formInsert').attr('action','{{url('fileSpecialist/editMaterial')}}');
                $('.btnSubmit').val('Guardar cambios');
                $('#idarchivospecialist').val(result.data.idarchivospecialist);
                $('#nombre').val(result.data.nombre);
                $('#comentario').val(result.data.comentario);
                $('#video').val(result.data.video);
                if(result.data.status==1) 
                    $("#publico").attr('checked',true);
                else
                    $("#publico").attr('checked',false);
                if(result.data.otherspecialist==1) 
                    $("#compartir").attr('checked',true);
                else
                    $("#compartir").attr('checked',false);
            }
        });
        console.log('llego aqui');
        
    });
    
    $(document).ready( function () {
        $('#example1').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );

</script>
@endsection