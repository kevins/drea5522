@extends('template/template')
@section('generalBody')
    @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')!= false)
                @include('file/insert/insertinicial')
    @endif
    @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'secundaria')!= false)
                @include('file/insert/insertsecundaria')
    @endif
    @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'primaria')!= false)
                @include('file/insert/insertprimaria')
    @endif  
@endsection
<div id="miModal"></div>
<script>
  function comentario(idCursoxdocente)
	{
		$('#miModal').load('{{url("file/comentarios")}}/'+idCursoxdocente);
	}
</script>


