<style>
	.card{
		border: 3px solid red;
    	border-radius: 5px;
    	padding: 5px;
    	box-shadow: 5px 10px 9px teal;
	}
	h5{
		text-shadow: 2px 3px 0px #d0c3c3;
    	font-size: 20px;
	}
	
</style>
<div class="row">
	<div class="col-lg-3 col-md-6 col-sm-12">
		<div class="card segun" data-segun="secundaria-cyt" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/secundaria.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">CIENCIA Y TECNOLOGIA</h5>
		  	</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12">
		<div class="card segun" data-segun="secundaria-pdcc" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/secundaria.jpg')}}" width="100%">
		  	<div class="card-body py-2">
		    	<h5 class="card-title text-center">Desarrollo Personal, Ciudadanía y Cívica</h5>
		  	</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-12">
		<div class="card segun" data-segun="secundaria-comunicacion" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/secundaria.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">COMUNICACION</h5>
		  	</div>
		</div>
	</div>
	
	<div class="col-lg-3 col-md-6 col-sm-12">
		<div class="card segun" data-segun="secundaria-matematica" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/secundaria.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">MATEMATICA</h5>
		  	</div>
		</div>
	</div>
	<div class="col-md-12"><hr></div>
	<div class="col-md-12 contenedorSegunList my-4"></div>
</div>
<script>
	// _token:$('meta[name="_token"]').attr('content')
	$('.segun').on('click',function(){
        // alert('cascasc');
        jQuery.ajax(
        { 
            url: "{{ url('/files/viewList') }}",
            data: {
            	_token:$('meta[name="_token"]').attr('content'),
            	nivel:$(this).attr('data-segun')},
            method: 'post',
            success: function(result){
            	console.log(result);
            	$('.contenedorSegunList>div').remove();
                $('.contenedorSegunList').append(result);
            }
        });
	});
</script>