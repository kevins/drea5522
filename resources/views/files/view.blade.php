﻿
<link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">

<script src="{{asset('plugin/adminlte/plugins/jquery/jquery.min.js')}}"></script>
<style>
	.titulo-re{
		text-shadow: 2px 3px 0px #d0c3c3;
    	font-size: 30px;
	}
	.separator{
		border-bottom: 1px solid #e2a04a;
	    position: relative;
	    
	    width: 90%;
	    margin: auto;
	}
</style>
<script>
	$('body').css('background-image','url({{asset('imgSocio/fondo.png')}})');
		$('body').css('background-repeat','round');
</script>
<div class="container-fluid p-0">
		<!-- <img src="{{asset('imgSocio/fondo3.jpg')}}" style="width: 100%;position:absolute;"> -->
		<!-- <div class="container-fluid m-0 p-0 ">
			<div class="col-lg-12 p-0">
				<img src="{{asset('imgSocio/cinta-andina.png')}}" class="w-100">
			</div>
		</div> -->
		<div class="container-fluid p-2 text-center" style="margin-top: 5%;">
			<div class="row" style="margin: 0 5rem;">
				<div class="col-lg-4">
					<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
					  	<img class="card-img-top" src="{{asset('imgSocio/mod-recursos.png')}}" alt="Card image cap">
					  	<div class="card-body">
					    	<!-- <h5 class="card-title font-weight-bold text-center">RECURSOS PARA EL APRENDIZAJE</h5> -->
					    	<p class="card-text">Recursos y archivos contextualizados de la region de APURÌMAC, para las diferentes edades y grados.</p>
					    	<!-- <a href="{{url('portal/recursosAprendizaje/inicio')}}" class="btn btn-primary font-weight-bold">Ingresa aquì</a> -->
					    	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<div class="row justify-content-center align-items-center">
	
	<a href="http://ipesa.dreapurimac.gob.pe/" class="btn btn-primary font-weight-bold">IPESA</a>|
	<div class="dropdown">
	  	<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    Recursos
	  	</button>
	  	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	  		<a class="dropdown-item" href="{{url('raPortal/presentacion')}}">Prsentaciòn</a>
		    <a class="dropdown-item" href="{{url('raPortal/listarInicial')}}">Inicial</a>
		    <a class="dropdown-item" href="{{url('raPortal/listarPrimaria')}}">Primaria</a>
	    	<a class="dropdown-item" href="{{url('raPortal/listarSecundaria')}}">Secundaria</a>
	  	</div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
					  	</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
					  	<img class="card-img-top" src="{{asset('imgSocio/mod-apoyo-socio.png')}}" alt="Card image cap">
					  	<div class="card-body">
					    	<!-- <h5 class="card-title font-weight-bold text-center">APOYO SOCIOEMOCIONAL</h5> -->
					    	<p class="card-text">Recursos tecnològicos para el soporte socioemocional de la comunidad educativa de la regiòn APURÌMAC.</p>
					    	<a href="{{url('categorias')}}" class="btn btn-primary font-weight-bold">Ingresa aquì</a>
					  	</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
					  	<img class="card-img-top" src="{{asset('imgSocio/mod-aula-virtual.png')}}" alt="Card image cap">
					  	<div class="card-body">
					    	<!-- <h5 class="card-title font-weight-bold text-center">AULA VIRTUAL</h5> -->
					    	<p class="card-text">Cursos de apoyo para los docentes y estudiantes de la region de APURÌMAC.</p>
					    	<a href="https://aulavirtual.dreapurimac.gob.pe/" class="btn btn-primary font-weight-bold">Ingresa aquì</a>
					  	</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
					  	<img class="card-img-top" src="{{asset('imgSocio/mod-materiales.png')}}" alt="Card image cap">
					  	<div class="card-body">
					    	<!-- <h5 class="card-title font-weight-bold text-center">MATERIAL ACADEMICO PARA DOCENTES</h5> -->
					    	<p class="card-text">Material de academico y audiovisual para el apoyo a los docentes de la region de APURÌMAC.</p>
					    	<a href="https://aulavirtual.dreapurimac.gob.pe/" class="btn btn-primary font-weight-bold">Ingresa aquì</a>
					  	</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
					  	<img class="card-img-top" src="{{asset('imgSocio/mod-documentos.png')}}" alt="Card image cap">
					  	<div class="card-body">
					    	<!-- <h5 class="card-title font-weight-bold text-center">DOCUMENTOS INSTITUCIONALES</h5> -->
					    	<p class="card-text">Informacion y documentos de las instituciones educativas de la region de APURÌMAC.</p>
					    	<a href="https://aulavirtual.dreapurimac.gob.pe/" class="btn btn-primary font-weight-bold">Ingresa aquì</a>
					  	</div>
					</div>
				</div>
			</div>
	    </div>
	</div>
<div class="container" style="display: none;">
	<div class="row">
		<div class="col-md-12 mb-4">
			<div class="row">
				<div class="col-lg-12">
					<h5 class="text-center titulo-re">RECURSOS EDUCATIVOS</h5>
				</div>
				<div class="col-lg-12 pb-4">
					<div class="separator"></div>
				</div>
				<div class="col-lg-6">
					<p>Los recursos educativos abiertos <strong>(representados con  las siglas REA)</strong> son recursos digitales creados con una finalidad educativa que se encuentran de manera gratuita y abierta para docentes, estudiantes y el público general.</p>
					<p><strong>El contenido de los mismos es bastante variado:</strong> pueden incluir textos, imágenes, recursos de audio y vídeo, juegos educativos y herramientas de software, entre otros.</p>
					<p><strong>Recursos disponibles segun nivel.</strong></p>
				</div>

				<div class="col-lg-6">
					<img src="{{asset('imagen/repositorio.jpg')}}" width="100%">
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group text-center">
				<button class="btn btn-info nivel" data-nivel="inicial"><i class="fa fa-arrow-right"></i> Inicial</button>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group text-center">
				<button class="btn btn-info nivel" data-nivel="primaria"><i class="fa fa-arrow-right"></i> Primaria</button>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group text-center">
				<button class="btn btn-info nivel" data-nivel="secundaria"><i class="fa fa-arrow-right"></i> Secundaria</button>
			</div>
		</div>
	</div>
</div>
<div class="container contenedorSegunNivel" style="margin-bottom: 50px;"></div>
<script>
	// _token:$('meta[name="_token"]').attr('content')
	$('.nivel').on('click',function(){
        jQuery.ajax(
        { 
            url: "{{ url('/files/view') }}",
            data: {
            	_token:$('meta[name="_token"]').attr('content'),
            	nivel:$(this).attr('data-nivel')},
            method: 'post',
            success: function(result){
            	$('.contenedorSegunNivel>div').remove();
                $('.contenedorSegunNivel').append(result);
            }
        });
	});
</script>

