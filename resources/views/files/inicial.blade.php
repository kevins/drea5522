<style>
	.card{
		border: 3px solid red;
    	border-radius: 5px;
    	padding: 5px;
    	box-shadow: 5px 10px 9px teal;
	}
	h5{
		text-shadow: 2px 3px 0px #d0c3c3;
    	font-size: 20px;
	}
</style>
<div class="row">
	<!-- <div class="col-md-3">
		<div class="card segun" data-segun="inicial-0" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/inicial.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">0 AÑO</h5>
		  	</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="card segun" data-segun="inicial-1" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/inicial.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">1 AÑO</h5>
		  	</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="card segun" data-segun="inicial-2" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/inicial.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">2 AÑO</h5>
		  	</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="card segun" data-segun="inicial-3" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/inicial.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">3 AÑO</h5>
		  	</div>
		</div>
	</div>
	<div class="col-md-12"><hr></div>
	<div class="col-md-3">
		<div class="card segun" data-segun="inicial-4" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/inicial.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">4 AÑO</h5>
		  	</div>
		</div>
	</div> -->
	
	<div class="col-md-3">
		<div class="card segun" data-segun="inicial-5" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/inicial.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">5 AÑO</h5>
		  	</div>
		</div>
	</div>
	<!-- <div class="col-md-3">
		<div class="card segun" data-segun="inicial-6" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/inicial.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">6 AÑO</h5>
		  	</div>
		</div>
	</div> -->
	
	<div class="col-md-12 contenedorSegunList my-4">
		
	</div>
</div>
<script>
	// _token:$('meta[name="_token"]').attr('content')
	$('.segun').on('click',function(){
        // alert('cascasc');
        jQuery.ajax(
        { 
            url: "{{ url('/files/viewList') }}",
            data: {
            	_token:$('meta[name="_token"]').attr('content'),
            	nivel:$(this).attr('data-segun')},
            method: 'post',
            success: function(result){
            	console.log(result);
            	$('.contenedorSegunList>div').remove();
                $('.contenedorSegunList').append(result);
            }
        });
	});
</script>