<script src="{{asset('js/export/jquery.dataTables.min.js')}}"></script>

<div class="mb-4">
	<div class="badge badge-info m-0 w-100 mb-2" style="font-size: 20px;">Comunicación</div>
	<table id="list" style="width: 100%;" class="table-bordered">
		<thead class="bg-info">
			<tr class="text-center">
				<th>N°</th>
				<th>GRADO</th>
				<th>CURSO</th>
				<th>T. ARCHIVO</th>
				<th>ARCHIVO</th>
			</tr>
		</thead>
		<tbody>
			<tr class="text-center">
				<td>1</td>
				<td>primer grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n1_1ro_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>2</td>
				<td>primer grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n2_1ro_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>3</td>
				<td>primer grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n3_1ro_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>4</td>
				<td>primer grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n4_1ro_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



			<tr class="text-center">
				<td>5</td>
				<td>segundo grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n1_2do_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>6</td>
				<td>segundo grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n2_2do_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>7</td>
				<td>segundo grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n3_2do_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>8</td>
				<td>segundo grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n4_2do_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>


			<tr class="text-center">
				<td>9</td>
				<td>tercero grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n1_3ro_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>10</td>
				<td>tercero grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n2_3ro_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>11</td>
				<td>tercero grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n3_3ro_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>12</td>
				<td>tercero grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n4_3ro_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



			<tr class="text-center">
				<td>13</td>
				<td>cuarto grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n1_4to_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>14</td>
				<td>cuarto grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n2_4to_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>15</td>
				<td>cuarto grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n3_4to_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>16</td>
				<td>cuarto grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n4_4to_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>17</td>
				<td>quinto grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n2_5to_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>18</td>
				<td>quinto grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n1_5to_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>19</td>
				<td>quinto grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n3_5to_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>20</td>
				<td>quinto grado</td>
				<td>Comunicación</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/secundaria/com/sesion_n4_5to_com.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
		</tbody>
	</table>
</div>
<script>
	$(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
</script>