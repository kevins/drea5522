<script src="{{asset('js/export/jquery.dataTables.min.js')}}"></script>
<!-- <script src="{{asset('js/export/dataTables.buttons.min.js')}}"></script> -->
<!-- <script src="{{asset('js/export/buttons.flash.min.js')}}"></script> -->
<!-- <script src="{{asset('js/export/jszip.min.js')}}"></script> -->
<!-- <script src="{{asset('js/export/pdfmake.min.js')}}"></script> -->
<!-- <script src="{{asset('js/export/vfs_fonts.js')}}"></script> -->
<!-- <script src="{{asset('js/export/buttons.html5.min.js')}}"></script> -->
<!-- <script src="{{asset('js/export/buttons.print.min.js')}}"></script> -->
<div class="mb-4">
	<table id="list" style="width: 100%;" class="table-bordered">
		<thead class="bg-info">
			<tr class="text-center">
				<th>N°</th>
				<th>TITULO</th>
				<th>T. ARCHIVO</th>
				<th>VER</th>
				<th>cas</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th>1</th>
				<th>direccion de estudios</th>
				<th>pdf</th>
				<th><a href="{{$v=asset('files/inicial/1.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></th>
				<th>cas</th>
			</tr>
			<tr>
				<th>cas</th>
				<th>cas</th>
				<th>cas</th>
				<th>cas</th>
				<th>cas</th>
			</tr>
		</tbody>
	</table>
</div>
<script>
	$(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
</script>