<script src="{{asset('js/export/jquery.dataTables.min.js')}}"></script>

<div class="mb-4">
	<div class="badge badge-info m-0 w-100 mb-2" style="font-size: 20px;">5 AÑOS</div>
	<table id="list" style="width: 100%;" class="table-bordered">
		<thead class="bg-info">
			<tr class="text-center">
				<th>N°</th>
				<th>EDAD</th>
				<th>TITULO</th>
				<th>T. ARCHIVO</th>
				<th>ARCHIVO</th>
			</tr>
		</thead>
		<tbody>
			<tr class="text-center">
				<td>1</td>
				<td>5 años</td>
				<td>3 orientaciones sin conectividad</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/3_orientaciones_sin_conectividad.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>2</td>
				<td>5 años</td>
				<td>actividad complementaria ugel andahuaylas</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/actividad_complementaria_ugel_andahuaylas.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>3</td>
				<td>5 años</td>
				<td>consideraciones material semana 20</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/consideraciones_material_semana_20.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>4</td>
				<td>5 años</td>
				<td>experiencia aprendizaje aspi 2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/experiencia_aprendizaje_aspi_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>5</td>
				<td>5 años</td>
				<td>experiencia de aprendizaje pronoei</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/experiencia_aprendizaje_pronoei.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>6</td>
				<td>5 años</td>
				<td>experiencia de aprendizaje sin conectividad</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/experiencia_aprendizaje_sin_conectividad.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>7</td>
				<td>5 años</td>
				<td>experiencia de aprendizaje y orientaciones a la familia grau</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/experiencia_de_aprendizaje_y_orientaciones_a_la_familia_grau.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>8</td>
				<td>5 años</td>
				<td>guia de orientacion familias inicial aymaraes</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/guia_orientacion_familias_inicial_aymaraes.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>9</td>
				<td>5 años</td>
				<td>material complementario semana 20</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/material_complementario_semana_20.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>10</td>
				<td>5 años</td>
				<td>material complementario semana 21</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/material_complementario_semana_21.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>11</td>
				<td>5 años</td>
				<td>planificacion sin acceso setiembre1 inicial aymaraes</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/planificacion_sin_acceso_setiembre1_inicial_aymaraes.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>12</td>
				<td>5 años</td>
				<td>PRIMERA EXPERIENCIA DE APRENDIZAJE: 1ra guia de PP.FF por RADIO de 5 años del 3/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/1guia_ppff_radio_5a_3_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>13</td>
				<td>5 años</td>
				<td>PRIMERA EXPERIENCIA DE APRENDIZAJE: 2da guia de PP.FF por RADIO de 5 años del 4/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/2guia_ppff_radio_5a_4_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>14</td>
				<td>5 años</td>
				<td>PRIMERA EXPERIENCIA DE APRENDIZAJE: 3ra guia de PP.FF por RADIO de 5 años del 5/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/3guia_ppff_radio_5a_5_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>15</td>
				<td>5 años</td>
				<td>PRIMERA EXPERIENCIA DE APRENDIZAJE: 4ta guia de PP.FF por RADIO de 5 años del 6/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/4guia_ppff_radio_5a_6_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>16</td>
				<td>5 años</td>
				<td>PRIMERA EXPERIENCIA DE APRENDIZAJE: 5ra guia de PP.FF por RADIO de 5 años del 7/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/5guia_ppff_radio_5a_7_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>17</td>
				<td>5 años</td>
				<td>SEGUNDA EXPERIENCIA DE APRENDIZAJE: 1ra guia de PP.FF por RADIO de 5 años del 11/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/1guia_ppff_radio_5a_11_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>18</td>
				<td>5 años</td>
				<td>SEGUNDA EXPERIENCIA DE APRENDIZAJE: 2da guia de PP.FF por RADIO de 5 años del 12/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/2guia_ppff_radio_5a_12_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>19</td>
				<td>5 años</td>
				<td>SEGUNDA EXPERIENCIA DE APRENDIZAJE: 3ra guia de PP.FF por RADIO de 5 años del 13/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/3guia_ppff_radio_5a_13_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>20</td>
				<td>5 años</td>
				<td>SEGUNDA EXPERIENCIA DE APRENDIZAJE: 4ta guia de PP.FF por RADIO de 5 años del 14/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/4guia_ppff_radio_5a_14_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>21</td>
				<td>5 años</td>
				<td>TERCERA EXPERIENCIA DE APRENDIZAJE: 1ra guia de PP.FF por RADIO de 5 años del 18/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/1guia_ppff_radio_5a_18_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>22</td>
				<td>5 años</td>
				<td>TERCERA EXPERIENCIA DE APRENDIZAJE: 2da guia de PP.FF por RADIO de 5 años del 19/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/2guia_ppff_radio_5a_19_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>23</td>
				<td>5 años</td>
				<td>TERCERA EXPERIENCIA DE APRENDIZAJE: 3ra guia de PP.FF por RADIO de 5 años del 20/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/3guia_ppff_radio_5a_20_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>24</td>
				<td>5 años</td>
				<td>TERCERA EXPERIENCIA DE APRENDIZAJE: 4ta guia de PP.FF por RADIO de 5 años del 21/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/4guia_ppff_radio_5a_21_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>25</td>
				<td>5 años</td>
				<td>TERCERA EXPERIENCIA DE APRENDIZAJE: 5ta guia de PP.FF por RADIO de 5 años del 24/08/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/inicial/5/5guia_ppff_radio_5a_24_8_2020.pdf')}}" class="btn btn-sm btn-link" target="blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
		</tbody>
	</table>
</div>
<script>
	$(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
</script>