<script src="{{asset('js/export/jquery.dataTables.min.js')}}"></script>

<div class="mb-4">
	<div class="badge badge-info m-0 w-100 mb-2" style="font-size: 20px;">III CICLO</div>
	<table id="list" style="width: 100%;" class="table-bordered">
		<thead class="bg-info">
			<tr class="text-center">
				<th>N°</th>
				<th>CICLO</th>
				<th>UGEL</th>
				<th>TITUTLO</th>
				<th>T. ARCHIVO</th>
				<th>ARCHIVO</th>
			</tr>
		</thead>
		<tbody>
			<tr class="text-center">
				<td>1</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>experiencias de aprendizaje III ciclo setiembre 2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/experiencias_de_aprendizaje_iii_ciclo_setiembre_2020.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>2</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>ficha comunicacion III ciclo 4/9/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/ficha_comunicacion_iii_ciclo_4_9_2020.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>3</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>ficha de ciencia y tecnologia III ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/ficha_de_cyt_iii_ciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>4</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>ficha de matematica III ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/ficha_de_matematica_iii_ciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>5</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>ficha de personal social III ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/ficha_de_ps_iii_ciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>


			<tr class="text-center">
				<td>6</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>1grado comunicacion primera semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_comunicacion_primera_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>7</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>1grado comunicacion segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_comunicacion_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>8</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>1grado personal social primera semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_ps_primera_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>9</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>1grado personal social segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_ps_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>


			<tr class="text-center">
				<td>10</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>2grado comunicacion primear semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_comunicacion_primear_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>11</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>2grado comunicacion segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_comunicacion_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>12</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>2grado ps primera semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_ps_primera_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>13</td>
				<td>III CICLO</td>
				<td>--</td>
				<td>2grado ps segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_ps_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



			<tr class="text-center">
				<td>14</td>
				<td>III CICLO</td>
				<td>antabamba</td>
				<td>1 ficha de ciencia y tecnogologia 1grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1_ficha_cyt_1grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>15</td>
				<td>III CICLO</td>
				<td>antabamba</td>
				<td>2 ficha de personal social 1grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2_ficha_ps_1grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>16</td>
				<td>III CICLO</td>
				<td>antabamba</td>
				<td>3 ficha de educacion fisica 1grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/3_ficha_educacion_fisica_1grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>17</td>
				<td>III CICLO</td>
				<td>antabamba</td>
				<td>4 ficha de matrematica 1grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/4_ficha_mat_1grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>18</td>
				<td>III CICLO</td>
				<td>antabamba</td>
				<td>5 ficha de comunicacion 1grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/5_ficha_com_1grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>19</td>
				<td>III CICLO</td>
				<td>antabamba</td>
				<td>1 ficha de personal social 2grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1_ficha_ps_2grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>20</td>
				<td>III CICLO</td>
				<td>antabamba</td>
				<td>2 ficha ciencia y tecnologia 2grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2_ficha_cyt_2grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>21</td>
				<td>III CICLO</td>
				<td>antabamba</td>
				<td>3 ficha de educacion fisica 2grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/3_ficha_educacion_fisica_2grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>22</td>
				<td>III CICLO</td>
				<td>antabamba</td>
				<td>5 ficha comunicacion 2grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/5_ficha_com_2grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>







			<tr class="text-center">
				<td>23</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>orientaciones a ppff 1grado ugel abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/orientaciones_a_ppff_1grado_ugel_abancay.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>






			<tr class="text-center">
				<td>24</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado leemos juntos semana1 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_leemos_juntos_sem1_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>25</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado leemos juntos semana2 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_leemos_juntos_sem2_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>26</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado leemos juntos semana3 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_leemos_juntos_sem3_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>27</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado leemos juntos semana4 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_leemos_juntos_sem4_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>28</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado leemos juntos semana5 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_leemos_juntos_sem5_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>




			<tr class="text-center">
				<td>29</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado ciencia y tecnologia semana1 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_cyt_sem1_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>30</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado matematica semana1 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_matematica_sem1_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>




			<tr class="text-center">
				<td>31</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado comunicacion semana2 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_comunicacion_sem2_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>32</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado personal social semana2 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_ps_sem2_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



			<tr class="text-center">
				<td>33</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado ciencia y tecnologia semana3 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_cyt_sem3_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>34</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado matematica semana3 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_matematica_sem3_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>




			<tr class="text-center">
				<td>35</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado comunicacion semana4 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_comunicacion_sem4_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>36</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>1grado personal social semana4 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1grado_ps_sem4_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



















			<tr class="text-center">
				<td>37</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>orientaciones a ppff 2grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/orientaciones_a_ppff_2grado_abanca.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>






			<tr class="text-center">
				<td>38</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado leemos juntos semana1 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_leemos_juntos_sem1_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>39</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado leemos juntos semana2 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_leemos_juntos_sem2_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>40</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado leemos juntos semana3 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_leemos_juntos_sem3_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>41</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado leemos juntos semana4 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_leemos_juntos_sem4_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>42</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado leemos juntos semana5 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_leemos_juntos_sem5_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



			<tr class="text-center">
				<td>43</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado ciencia y tecnologia semana1 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_cyt_sem1_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>44</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado matematica semana1 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_matematica_sem1_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>


			<tr class="text-center">
				<td>45</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado comunicacion semana2 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_comunicacion_sem2_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>46</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado personal social semana2 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_ps_sem2_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>





			<tr class="text-center">
				<td>47</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado ciencia y tecbnologia semana3 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_cyt_sem3_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>48</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado matematica semana3 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_matematica_sem3_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>





			<tr class="text-center">
				<td>49</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado comunicacion semana4 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_comunicacion_sem4_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>50</td>
				<td>III CICLO</td>
				<td>abancay</td>
				<td>2grado personal social semana4 abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2grado_ps_sem4_abanc.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>

















			<tr class="text-center">
				<td>51</td>
				<td>III CICLO</td>
				<td>chincheros</td>
				<td>chincheros comunicacion iii ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/chincheros_comunicacion_iiiciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>52</td>
				<td>III CICLO</td>
				<td>chincheros</td>
				<td>chincheros ciencia y tecnologia iii ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/chincheros_cyt_iiiciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>53</td>
				<td>III CICLO</td>
				<td>chincheros</td>
				<td>chincheros matematica iii ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/chincheros_matematica_iiiciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>54</td>
				<td>III CICLO</td>
				<td>chincheros</td>
				<td>chincheros personal social iii ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/chincheros_ps_iiiciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>












			<tr class="text-center">
				<td>55</td>
				<td>III CICLO</td>
				<td>cotabambas</td>
				<td>1 ficha comunicacion</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1_ficha_cotabambas_comunicacion.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>56</td>
				<td>III CICLO</td>
				<td>cotabambas</td>
				<td>1 ficha ciencia y tecnologia</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/1_ficha_cotabambas_cyt.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>57</td>
				<td>III CICLO</td>
				<td>cotabambas</td>
				<td>2 ficha comunicacion</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2_ficha_cotabambas_comunicacion.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>58</td>
				<td>III CICLO</td>
				<td>cotabambas</td>
				<td>2 ficha cienci y tecnologia</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/2_ficha_cotabambas_cyt.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>59</td>
				<td>III CICLO</td>
				<td>cotabambas</td>
				<td>orientacion a la familia comunicacion</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/orientacion_a_la_familia_comunicacion.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>60</td>
				<td>III CICLO</td>
				<td>cotabambas</td>
				<td>orientacion a la familia ciencia y tecnologia</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/3ciclo/orientacion_a_la_familia_cyt.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



		</tbody>
	</table>
</div>
<script>
	$(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
</script>