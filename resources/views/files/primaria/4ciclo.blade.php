<script src="{{asset('js/export/jquery.dataTables.min.js')}}"></script>

<div class="mb-4">
	<div class="badge badge-info m-0 w-100 mb-2" style="font-size: 20px;">IV CICLO</div>
	<table id="list" style="width: 100%;" class="table-bordered">
		<thead class="bg-info">
			<tr class="text-center">
				<th>N°</th>
				<th>CICLO</th>
				<th>UGEL</th>
				<th>TITUTLO</th>
				<th>T. ARCHIVO</th>
				<th>ARCHIVO</th>
			</tr>
		</thead>
		<tbody>
			<tr class="text-center">
				<td>1</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>experiencia de aprendizaje IV ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/experiencia_de_aprendizaje_iv_ciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>2</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>ficha ciencia y tecnologia IV ciclo 1/9/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ficha_cyt_iv_ciclo_1_9_2020.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>3</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>ficha de comunicacion IV ciclo 4/9/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ficha_de_comunicacion_iv_ciclo_4_9_20.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>4</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>ficha de matematica IV ciclo 2/9/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ficha_de_matematica_iv_ciclo_2_9_2020.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>5</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>ficha de personal social IV ciclo 3/9/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ficha_de_personal_social_iv_ciclo_3_9_2020.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>


			<tr class="text-center">
				<td>6</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>experiencia de aprendizaje IV ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/experiencia_de_aprendizaje_iv_ciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>7</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>ficha comumnicac 18 viertnes</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ficha_comumnicac_18_viertnes.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>8</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>ficha comunicacion 19 viernes</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ficha_comunicacion_19_viernes.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>9</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>ficha personal social 19 jueves</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ficha_ps_19_jueves.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>10</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>ficha personal social semana 18 jueves</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ficha_ps_semana_18_jueves.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



			<tr class="text-center">
				<td>11</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>formato de pagina para las fichas autoinstructivas final</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>12</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>sesion ciencia y tecnologia lunes 3/8/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/sesion_cyt_lunes_3_8_2020.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>13</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>sesion ciencia y tecnologia lunes 10/8/2020 IV ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/sesion_cyt_lunes_10_8_2020_iv_ciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>14</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>sesion matematica miercoles 5/8/2020 IV ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/sesion_matematica_miercoles_5_8_2020_iv_ciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>15</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>sesion matematica miercoles 12/8/2020 IV ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/sesion_matematica_miercoles_12_8_2020_iv_ciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



			<tr class="text-center">
				<td>16</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>3grado comunicacion primera semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/3grado_comunicacion_primera_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>17</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>3grado comunicacion segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/3grado_comunicacion_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>18</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>3grado personal social primera semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/3grado_ps_primera_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>19</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>3grado ps segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/3grado_ps_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>

			<tr class="text-center">
				<td>20</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>4grado comunicacion primera semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/4grado_comunicacion_primera_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>21</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>4grado comunicacion segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/4grado_comunicacion_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>22</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>4grado personal social primera semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/4grado_ps_primera_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>23</td>
				<td>IV CICLO</td>
				<td>--</td>
				<td>4grado personal social segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/4grado_ps_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>24</td>
				<td>IV CICLO</td>
				<td>antabamba</td>
				<td>1 ficha comunicacion 3grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/1_ficha_comunicacion_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>25</td>
				<td>IV CICLO</td>
				<td>antabamba</td>
				<td>2 ficha comunicacion 3grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/2_ficha_comunicacion_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>26</td>
				<td>IV CICLO</td>
				<td>antabamba</td>
				<td>3 ficha matematica 3grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/3_ficha_matematica_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>27</td>
				<td>IV CICLO</td>
				<td>antabamba</td>
				<td>4 ficha matematica 3grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/4_ficha_matematica_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>28</td>
				<td>IV CICLO</td>
				<td>antabamba</td>
				<td>5 ficha comunicacion 3grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/5_ficha_comunicacion_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>29</td>
				<td>IV CICLO</td>
				<td>antabamba</td>
				<td>5 ficha comunicacion 4grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/5_ficha_comunicacion_4grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>30</td>
				<td>IV CICLO</td>
				<td>antabamba</td>
				<td>6 ficha de ciencia y tecnologia 4grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/6_ficha_cyt_4grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>31</td>
				<td>IV CICLO</td>
				<td>antabamba</td>
				<td>7 ficha de ciencia y tecnologia 4grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/7_ficha_cyt_4grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>32</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>orientaciones a ppff 4grado iv ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/orientaciones_a_ppff_4grado_iv_ciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>33</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado ivc semana1 leemos juntos miercoles 2 setiembre</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ivc_s1_leemos_juntos_miercoles_2_setiembre.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>34</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado ivc semana2 leemos juntos miercoles 9 setiembre</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ivc_s2_leemos_juntos_miercoles_9_setiembre.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>35</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado ivc semana3 leemos juntos miercoles 16 setiembre</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ivc_s3_leemos_juntos_miercoles_16_setiembre.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>36</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado ivc semana5 leemos juntos miercoles 23 setiembre</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ivc_s5_leemos_juntos_miercoles_23_setiembre.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>37</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado ivc semana5 leemos juntos miercoles 30 setiembre el fuego</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ivc_s5_leemos_juntos_miercoles_30_setiembre_el_fuego.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>38</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado ivc semana5 leemos juntos miercoles 30 setiembre elfuego peru educa</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ivc_s5_leemos_juntos_miercoles_30_setiembre_elfuego_peru_educa.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>39</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado portada leemos juntos</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/portada_leemos_juntos.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>40</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado formato de pagina para las fichas autoinstructivas final ciencia y tecnologia semana1</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_cyt_s1.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>41</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado formato de pagina para las fichas autoinstructivas final matematica semana1</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_matematica_s1.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>42</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado formato de pagina para las fichas autoinstructivas final comunicacion semana2</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_comunicacion_s2.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>43</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado formato de pagina para las fichas autoinstructivas final personal social semana2</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_ps_s2.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>44</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado formato de pagina para las fichas autoinstructivas final ciencia y tecnologia semana3</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_cyt_s3.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>45</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado formato de pagina para las fichas autoinstructivas final matematica semana1</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_matematica_s3.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>46</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado formato de pagina para las fichas autoinstructivas final comunicacion semana4</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_comunicacion_s4.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>47</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>4grado formato de pagina para las fichas autoinstructivas final personal social semana4</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_ps_s4.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>48</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado orientaciones a ppff 3grado iv ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/orientaciones_a_ppff_3grado_iv_ciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>49</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado ivc semana1 leemos juntos miercoles 2 setiembre</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ivc_s1_leemos_juntos_miercoles_2_setiembre_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>50</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado ivc semana2 leemos juntos miercoles 9 setiembre</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ivc_s2_leemos_juntos_miercoles_9_setiembre_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>51</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado ivc semana3 leemos juntos miercoles 16 setiembre</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ivc_s3_leemos_juntos_miercoles_16_setiembre_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>52</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado ivc semana5 leemos juntos miercoles 23 setiembre</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ivc_s5_leemos_juntos_miercoles_23_setiembre_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>53</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado ivc semana5 leemos juntos miercoles 30 setiembre el fuego</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/ivc_s5_leemos_juntos_miercoles_30_setiembre_el_fuego_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>54</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado portada leemos juntos</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/portada_leemos_juntos_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>55</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado formato de pagina para las fichas autoinstructivas final ciencia y tecnologia semana1</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_cyt_s1_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>56</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado formato de pagina para las fichas autoinstructivas final matematica semana1</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_matematica_s1_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>57</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado formato de pagina para las fichas autoinstructivas final comunicacion semana2</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_comunicacion_s2_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>58</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado formato de pagina para las fichas autoinstructivas final personal social semana2</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_ps_s2_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>59</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado formato de pagina para las fichas autoinstructivas final ciencia y tecnologia semana3</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_cyt_s3_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>60</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado formato de pagina para las fichas autoinstructivas final matematica semana3</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_matematica_s3_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>61</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado formato de pagina para las fichas autoinstructivas final comunicacion semana4</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_comunicacion_s4_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>62</td>
				<td>IV CICLO</td>
				<td>abancay</td>
				<td>3grado formato de pagina para las fichas autoinstructivas final personal social semana4</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/formato_de_pagina_para_las_fichas_autoinstructivas_final_ps_s4_3grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>












			<tr class="text-center">
				<td>63</td>
				<td>IV CICLO</td>
				<td>chincheros</td>
				<td>1 iv ciclo ciencia y tecnologia chincheros semana 21 fp</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/1_ivciclo_cyt_chincheros_semana_21_fp.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>64</td>
				<td>IV CICLO</td>
				<td>chincheros</td>
				<td>3 iv ciclo matematica chincheros semana 20 fp</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/3_ivciclo_matematica_chincheros_semana_20_fp.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>65</td>
				<td>IV CICLO</td>
				<td>chincheros</td>
				<td>3grado 4grado iv ciclo personal social chincheros</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/3grado_4grado_ivciclo_ps_chincheros.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>





			<tr class="text-center">
				<td>66</td>
				<td>IV CICLO</td>
				<td>chincheros</td>
				<td>4 iv ciclo comunicacion chincheros semana 20 fp</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/4_ivciclo_comunicacion_chincheros_semana_20_fp.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>67</td>
				<td>IV CICLO</td>
				<td>chincheros</td>
				<td>5 iv ciclo ps chincheros semana 21 fp</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/5_ivciclo_ps_chincheros_semana_21_fp.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>68</td>
				<td>IV CICLO</td>
				<td>chincheros</td>
				<td>6 iv ciclo ps chincheros</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/6_ivciclo_ps_chincheros.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>


			<tr class="text-center">
				<td>69</td>
				<td>IV CICLO</td>
				<td>cotabambas</td>
				<td>1 ficha IV ciclo ciencia y tecnologia</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/1_ficha_IV_ciclo_cyt_cota.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>70</td>
				<td>IV CICLO</td>
				<td>cotabambas</td>
				<td>2 ficha IV ciclo ciencia y tecnologia</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/2_ficha_IV_ciclo_cyt_cota.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>71</td>
				<td>IV CICLO</td>
				<td>cotabambas</td>
				<td>orientacion familiar ciencia y tecnologia iv ciclo</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/4ciclo/orientacion_familiar_cyt_iv_ciclo.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>

		</tbody>
	</table>
</div>
<script>
	$(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
</script>