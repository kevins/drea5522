'use strict';


/*function sendFrmInsertFile()
{
	var isValid=null;

	$('#frmInsertFile').data('formValidation').validate();

	isValid=$('#frmInsertFile').data('formValidation').isValid();
    alert(isValid);
	if(!isValid)
	{

		incorrectNote();

		return;
	}
    new PNotify({title:"No se pudo proceder",text:"Por favor complete y corrija toda la informaci\u00f3n necesaria antes de continuar.",type:"error"})
	confirmDialogSend('frmInsertFile');
}*/

function sendfrmInsertFile()
{
    confirmDialogSend('frmInsertFile');
}

function sendfrmInsertFileSecundaria()
{
	confirmDialogSend('frmInsertFileSecundaria');
}

$( document ).ready(function() 
{ 
// $("#selectGrado").prop('disabled', true);
// $("#selectSeccion").prop('disabled', true);
// $("#selectCurso").prop('disabled', true);
});

$("input[name=radioarchivo]").click(function () 
{
    var aux=$('input:radio[name=radioarchivo]:checked').val();

    if(aux=='Ciclo')
    {
    	$('#selectSeccion option:nth-child(9)').prop("selected",true);
        $('#selectCurso').val(null).trigger('change');
    	$("#selectSeccion").prop('disabled', true);
		$("#selectCurso").prop('disabled', true);
    }

    if(aux=='Seccion')
    {
        $('#selectSeccion option:nth-child(1)').prop("selected",true);
        $('#selectCurso').val(null).trigger('change');
		$("#selectGrado").prop('disabled', false);
		$("#selectSeccion").prop('disabled', false);
		$("#selectCurso").prop('disabled', true);
    }

    if(aux=='Curso')
    {
      $("#selectGrado").prop('disabled', false);
      $("#selectSeccion").prop('disabled', false);
      $("#selectCurso").prop('disabled', false);
    }
 }); 
  