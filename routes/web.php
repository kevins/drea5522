<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Rutas libres

Route::match(['get','post'],'user/login','PersonController@actionLogin');
Route::get('index','PersonController@actionPortal');

//Rutas en comun: docente, director, especialista

Route::group(['middleware'=>['MDGeneric']], function()
{
    Route::get('/','PersonController@actionIndex');
    Route::get('user/logout','PersonController@actionLogout');
});

//Rutas solo docente

Route::group(['middleware'=>['MDDocente']], function()
{
    // Route::get('inicio/inicioDcte','InicioController@actionInicioDcte');


    Route::match(['get','post'],'insert/file','FileController@actionInsert');

    Route::match(['get','post'],'fileDcte/insertPa','FileDcteController@actionInsertPa');
    Route::get('fileDcte/deletePa/{idcd}','FileDcteController@actionDeletePa');

    Route::match(['get','post'],'fileUnidad/insertUnidad','FileUnidadController@actionInsertUnidad');
    Route::get('fileUnidad/deleteUnidad/{idunidad}','FileUnidadController@actionDeleteUnidad');
    Route::match(['get','post'],'fileUnidad/showFormInsertU','FileUnidadController@actionShowFormInsertU');
    Route::match(['get','post'],'fileUnidad/editUnidad','FileUnidadController@actionEditUnidad');
    
    Route::match(['get','post'],'fileSesion/insertSesion','FileSesionController@actionInsertSesion');
    Route::get('fileSesion/deleteSesion/{idsesion}','FileSesionController@actionDeleteSesion');
    Route::match(['get','post'],'fileSesion/showFormInsertS','FileSesionController@actionShowFormInsertS');
    Route::match(['get','post'],'fileSesion/editSesion','FileSesionController@actionEditSesion');
    Route::get('fileSesion/loadUnid','FileSesionController@actionLoadUnid');

    
    // Route::match(['get','post'],'fileDcte/insertUS','FileDcteController@actionInsertUS');

    Route::get('fileDcte/listFileForCurso','FileDcteController@actionListFileForCurso');

    Route::post('insert/filesecundaria','FileController@actionInsertSecundaria');
    // Route::post('insert/filesecundaria','FileController@actionInsertSecundaria');

    Route::match(['get','post'],'insert/educationalmaterial','FileController@actionInsertEducationalMaterial');
    Route::get('delete/educationalmaterial/{idcd}','FileController@actionDeleteEducationalMaterial');
    Route::post('insert/fileprimaria','FileController@actionInsertPrimaria');

    Route::match(['get','post'],'fileDcte/editPa','FileDcteController@actionEditPa');

    
});

//Rutas solo director
Route::group(['middleware'=>['MDDirector']], function()
{
    //Aqui rutas
    Route::get('fileIe/listArchivosDcte','FileIeController@actionListArchivosDcte');
});

//Rutas solo especialista
Route::group(['middleware'=>['MDEspecialista']], function()
{
    Route::get('ie/updateDcte','IeController@actionUpdateDcte');
    Route::get('ie/getiee','IeController@actionGetiee');
    Route::get('ie/updateDirector','IeController@actionUpdateDirector');

    Route::get('dcte/option','DcteController@actionOption');
    Route::get('dcte/listDcte','DcteController@actionListDcte');
    Route::get('dcte/editEstado','DcteController@actionEditEstado');
    Route::match(['get','post'],'dcte/addDcte','DcteController@actionAddDcte');
    Route::get('dcte/delegarDirector','DcteController@actionDelegarDirector');
    Route::get('dcte/changeIe','DcteController@actionChangeIe');
    
});
Route::get('download/download','DownloadController@actionDownload');
Route::get('download/download/{file}','DownloadController@actionDownload');

Route::match(['get','post'],'fileIe/insert','FileIeController@actionInsert');
Route::get('fileIe/delete/{idarchivoie}','FileIeController@actionDelete');
Route::match(['get','post'],'fileIe/edit','FileIeController@actionEdit');
Route::match(['get','post'],'fileIe/listPcDcte','FileIeController@actionListPcDcte');
Route::match(['get','post'],'fileIe/shareFile','FileIeController@actionShareFile');

Route::match(['get','post'],'shareIe/shareFileEdit','ShareIeController@actionShareFileEdit');

Route::match(['get','post'],'fileSpecialist/insertShare','FileSpecialistController@actionInsertShare');
Route::get('fileSpecialist/getie','FileSpecialistController@actionGetIe');
Route::get('fileSpecialist/getIeGeneralUgel','FileSpecialistController@actionGetIeGeneralUgel');
Route::get('fileSpecialist/delete/{idarchivospecialist}','FileSpecialistController@actionDelete');
Route::match(['get','post'],'fileSpecialist/edit','FileSpecialistController@actionEdit');
Route::match(['get','post'],'fileSpecialist/insertMaterial','FileSpecialistController@actionInsertMaterial');
Route::match(['get','post'],'fileSpecialist/editMaterial','FileSpecialistController@actionEditMaterial');

Route::get('fileSpecialist2/searchDocGes','FileSpecialist2Controller@actionSearchDocGes');
Route::get('fileSpecialist2/listDocGes','FileSpecialist2Controller@actionListDocGes');
Route::get('fileSpecialist2/searchPcDcte','FileSpecialist2Controller@actionSearchPcDcte');
Route::get('fileSpecialist2/listPcDcte','FileSpecialist2Controller@actionListListPcDcte');








Route::get('materialEducativo/mostrar','MaterialEducativoController@actionMostrar');
Route::post('materialEducativo/buscar','MaterialEducativoController@actionBuscar');
Route::post('materialEducativo/buscarSemana','MaterialEducativoController@actionBuscarSemana');


Route::get('materialEducativo2/mostrar2','MaterialEducativo2Controller@actionMostrar2');
Route::post('materialEducativo2/buscar2','MaterialEducativo2Controller@actionBuscar2');
Route::post('materialEducativo2/buscarSemana2','MaterialEducativo2Controller@actionBuscarSemana2');


Route::get('materialEducativo3/mostrar3','MaterialEducativo3Controller@actionMostrar3');
Route::post('materialEducativo3/buscar3','MaterialEducativo3Controller@actionBuscar3');
Route::post('materialEducativo3/buscarSemana3','MaterialEducativo3Controller@actionBuscarSemana3');




// ----------------------------------------

Route::post('files/view','FilesController@actionView');
Route::post('files/viewList','FilesController@actionViewList');
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
Route::get('formulario', 'FormController@actionFormulario');
Route::get('buscar', 'FormController@actionBuscar');
Route::match(['get','post'],'registrar','FormController@actionRegistrar');
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

Route::get('categorias', function () {
    return view('categorias');
});
Route::get('materiales', function () {
    return view('materiales');
});

// MODULO SOCIOEMOCIONAL
Route::match(['get','post'],'user/loginSocioEmocional','SocioEmocionalController@actionLogin');
Route::get('socioEmocional/inicio','SocioEmocionalController@actionInicio');
Route::get('user/loginSocioEmocional/logout','SocioEmocionalController@actionLogout');
Route::match(['get','post'],'socioEmocional/registrar','SocioEmocionalController@actionRegistrar');
Route::get('socioEmocional/listar','SocioEmocionalController@actionListar');
Route::get('socioEmocional/delete/{idse}','SocioEmocionalController@actionDelete');
Route::match(['get','post'],'socioEmocional/editar','SocioEmocionalController@actionEditar');

Route::get('portal/socioEmocional/material/{categoria}','SocioEmocionalController@actionMaterial');
// MODULO DE RECURSOSO PARA EL APRENDIZAJE
// ESTE CREO Q NO SERVIRAÇÇ
Route::match(['get','post'],'user/loginRecursosAprendizaje','RecursosAprendizajeController@actionLogin');
Route::get('recursosAprendizaje/inicio','RecursosAprendizajeController@actionInicio');
Route::get('user/recursosAprendizaje/logout','RecursosAprendizajeController@actionLogout');
Route::match(['get','post'],'recursosAprendizaje/registrar','RecursosAprendizajeController@actionRegistrar');
Route::get('recursosAprendizaje/listar','RecursosAprendizajeController@actionListar');
Route::get('recursosAprendizaje/delete/{idra}','RecursosAprendizajeController@actionDelete');
Route::get('recursosAprendizaje/listarPortal','RecursosAprendizajeController@actionListarPortal');


Route::get('portal/recursosAprendizaje/inicio', function () {
    return view('portal/recursosAprendizaje/inicio');
});
// ESTE CREO Q NO SERVIRAÇÇ
// MODULO DE RECURSOSO DE APRENDIZAJE PARA SECUNDARIA PARA fichas de autoaprendizaje 
Route::match(['get','post'],'recAprSec/registrar','RecAprSecController@actionRegistrar');
Route::get('recAprSec/delete/{idras}','RecAprSecController@actionDelete');
// Route::get('recursosAprendizaje/listar','RecAprSecController@actionListar');
// Route::get('recursosAprendizaje/listarPortal','RecAprSecController@actionListarPortal');
// MODULO DE RECURSOSO DE APRENDIZAJE PARA PRIMARIA PARA EL APRENDIZAJE
Route::match(['get','post'],'recAprPri/registrar','RecAprPriController@actionRegistrar');
Route::get('recAprPri/delete/{idra}','RecAprPriController@actionDelete');
Route::match(['get','post'],'recAprIni/registrar','RecAprIniController@actionRegistrar');
Route::get('recAprIni/delete/{idra}','RecAprIniController@actionDelete');
// --------------------------------------
// Route::get('raPortal/listarInicial','RaPortalController@actionListarInicial');
Route::get('raPortal/presentacion', function () {
    return view('portal/ra/presentacion');
});
Route::get('raPortal/listarInicial','RaPortalController@actionListarInicial');
Route::get('raPortal/buscarInicial','RaPortalController@actionBuscarInicial');

Route::get('raPortal/listarSecundaria','RaPortalController@actionListarSecundaria');
Route::get('raPortal/buscarSecundaria','RaPortalController@actionBuscarSecundaria');
Route::get('raPortal/buscarSecundariaFichas','RaPortalController@actionBuscarSecundariaFichas');

Route::get('raPortal/listarPrimaria','RaPortalController@actionListarPrimaria');
Route::get('raPortal/buscarPrimaria','RaPortalController@actionBuscarPrimaria');

// Route::get('portal/ra/secundaria', function () {
//     return view('portal/ra/secundaria');
// });

// MODULO DE RECURSOSO DE APRENDIZAJE PARA agregar experiencia de aprendizaje

Route::match(['get','post'],'expAprSec/registrar','ExpAprSecController@actionRegistrar');
Route::match(['get','post'],'expAprSec/editar','ExpAprSecController@actionEditar');
Route::get('expAprSec/delete/{ide}','ExpAprSecController@actionDelete');

Route::match(['get','post'],'expAprPri/registrar','ExpAprPriController@actionRegistrar');
Route::get('expAprPri/delete/{ide}','ExpAprPriController@actionDelete');
Route::match(['get','post'],'expAprIni/registrar','ExpAprIniController@actionRegistrar');
Route::get('expAprIni/delete/{ide}','ExpAprIniController@actionDelete');

// MODULO GESTION DE TABLETAS PRONATEL
Route::match(['get','post'],'user/loginTabletasPronatel','TabletasPronatelController@actionLogin');
Route::get('gestionPronatel/inicio','TabletasPronatelController@actionInicio');
Route::match(['get','post'],'pronatelAsignacion/asignacion','PronatelAsignacionController@actionAsignacion');

