<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TExperiencias extends Model
{
	protected $table='texperiencias';
	protected $primaryKey='ide';
	public $incrementing=true;
	public $timestamps=false;
}
?>