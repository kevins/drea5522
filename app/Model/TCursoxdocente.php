<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TCursoxdocente extends Model
{
	protected $table='tcursoxdocente';
	protected $primaryKey='idcd';
	public $incrementing=true;
	public $timestamps=false;

	public function tDocente()
    {
        return $this->belongsTo('App\Model\TDocente','dni');
	}
	public function tCurso()
    {
        return $this->belongsTo('App\Model\TCurso','idcurso');
	}
	public function tGrado()
    {
        return $this->belongsTo('App\Model\TGrado','idgrado');
	}
	public function tSeccion()
    {
        return $this->belongsTo('App\Model\TSeccion','idseccion');
    }

    public function tUnidad()
    {
        return $this->hasMany('App\Model\TUnidad','idcd');
	}

}
?>