<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TUsuario extends Model
{
	protected $table='tusuario';
	protected $primaryKey='dni';
	public $incrementing=false;
	public $timestamps=false;
}
?>