<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TSesion extends Model
{
	protected $table='tsesion';
	protected $primaryKey='idsesion';
	public $incrementing=true;
	public $timestamps=false;

	public function tUnidad()
    {
        return $this->belongsTo('App\Model\TUnidad','idunidad');
	}
}
?>