<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TArchivodcte extends Model
{
	protected $table='tarchivodcte';
	protected $primaryKey='idarchivo';
	public $incrementing=true;
	public $timestamps=false;
	
	public function tCursoxDocente()
    {
        return $this->belongsTo('App\Model\TCursoxDocente','idcd');
    }

}
?>