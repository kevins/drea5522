<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TFormulario extends Model
{
	protected $table='tformulario2';
	protected $primaryKey='cm';
	public $incrementing=false;
	public $timestamps=false;
}
?>