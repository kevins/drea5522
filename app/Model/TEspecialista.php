<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TEspecialista extends Model
{
	protected $table='especialista';
	protected $primaryKey='dni';
	public $incrementing=true;
	public $timestamps=false;

	public function tUgel()
	{
		return $this->belongsTo('App\Model\TUgel','ugelid');
	} 
	public function tPersona()
	{
		return $this->belongsTo('App\Model\TPersona','dni');
	}
}
?>