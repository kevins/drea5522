<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TIIEE extends Model
{
	protected $table='iiee';
	protected $primaryKey='codigomodular';
	public $incrementing=true;
    public $timestamps=false;

    public function tDocente()
    {
        return $this->hasMany('App\Model\TDocente','ie');
    }
    public function tUbigeo_()
    {
        return $this->belongsTo('App\Model\TUbigeo','ubigeoid');
    }
    public function tUgel_()
    {
        return $this->belongsTo('App\Model\TUgel','ugelid');
    }
    public function tUgel()
    {
        return $this->belongsTo('App\Model\TUgel','ugelid');
    }
    public function tArchivoie()
    {
        return $this->hasMany('App\Model\TArchivoie','ie');
    }
}
?>