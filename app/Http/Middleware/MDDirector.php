<?php
namespace App\Http\Middleware;

use Closure;
use Session;


class MDDirector
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if( Session::has('Person') & session::get('rol')=='Director')
        {
            $response = $next($request);
            return $response;
            
        }
        //Session::flash('msj-error', 'Inicie sessión');
    }
}