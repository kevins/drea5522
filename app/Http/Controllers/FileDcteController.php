<?php
namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\File;
 
use Session;
use DB;
use Mail;

use App\Model\TCurso;
use App\Model\TDocente;
use App\Model\TGrado;
use App\Model\TSeccion;
use App\Model\TDetallecursopordocente;
use App\Model\TCursoxdocente;

class FileDcteController extends Controller
{
    protected $documentos = array("pdf", "docx", "xlsx");

    public function actionInsertPa(Request $request,SessionManager $sessionManager)
    {
        if($_POST)
        { 
            try
            {
                if($request->hasFile('file'))
                {
                    DB::beginTransaction();

                    if(is_null($request->input('selectCurso')))
                    {
                        $verifyCursoxdocente = TCursoxDocente::whereRaw('dni=? and idgrado=? and idseccion=?',[session()->get('Person')->dni,
                            $request->selectGrado,
                            $request->selectSeccion
                        ])->first();
                    }
                    else
                    {
                        $verifyCursoxdocente = TCursoxDocente::whereRaw('dni=? and idgrado=? and idseccion=? and idcurso=?',[session()->get('Person')->dni,
                            $request->selectGrado,
                            $request->selectSeccion,
                            $request->selectCurso
                        ])->first();
                    }
                    
                    if($verifyCursoxdocente!=null)
                    {
                        return $this->helperdrea->redirectAlert('Ya existe el plan anual.', 'fileDcte/insertPa');
                    }

                    $tCursoxDocente= new TCursoxdocente();
                    $tCursoxDocente->dni=session()->get('Person')->dni;
                    $tCursoxDocente->idcurso=is_null($request->input('selectCurso'))?'28':$request->input('selectCurso');
                    $tCursoxDocente->idseccion=$request->input('selectSeccion');
                    $tCursoxDocente->idgrado=$request->input('selectGrado');
                    $tCursoxDocente->nombreArchivo='Plan anual';
                    $tCursoxDocente->formato=strtolower($request->file('file')->getClientOriginalExtension());
                    $tCursoxDocente->peso=round($request->file('file')->getSize()/1024).'-kb';
                    $tCursoxDocente->nivel=session()->get('Person')->tDocente->tIIEE->nivelm;
                    $tCursoxDocente->periodoAcademico=$request->input('periodo');
                    $tCursoxDocente->comentario=$request->input('txtComentario');
                    $tCursoxDocente->estadoArchivo=$request->estadoArchivo=='publico'?'1':'0';
                    $tCursoxDocente->createddate=date('Y-m-d H:m:s');
    
                    $tCursoxDocente->save();

                    $request->file('file')->move(public_path().'/filedcte'.'/',session()->get('Person')->dni.'-'.$tCursoxDocente->idcd.'.'.$tCursoxDocente->formato);
                
                    DB::commit();
                    
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileDcte/insertPa');
                }
                else 
                {
                    return $this->helperdrea->redirectError('No cargo ningun archivo.', 'fileDcte/insertPa');
                }
            }

            catch(\exception $ex)
            {
                DB::rollback();
                return $this->helperdrea->redirectError('Algo salió mal, comuniquese con el administrador.', 'fileDcte/insertPa');
            }
        }

        if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'Secundaria')!==false)
        {
            $tGrado=TGrado::all()->take(5);
            $tCurso=TCurso::whereRaw('nivel=?','Secundaria')->get();
        }
        else if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'Primaria')!==false)
        {
            $tGrado=TGrado::all()->take(6);
            $tCurso=TCurso::whereRaw('nivel=?','Primaria')->get();
        }
        else if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')!==false)
        {
            $tGrado=TGrado::whereBetween('idgrado',[7,13])->get();
            $tCurso=TCurso::whereRaw('nivel=?','Inicial')->get();
        }
        
        $tSeccion=TSeccion::all();

        $tCursoxDocente=TCursoxDocente::whereRaw('dni=? and nombreArchivo=?',[session()->get('Person')->dni,'Plan anual'])->get();

        return view('fileDcte/insertPa',['tCurso'=>$tCurso,'tGrado'=>$tGrado,'tSeccion'=>$tSeccion,'tCursoxDocente'=>$tCursoxDocente]);
    }
    public function actionDeletePa($idcd=null)
    {
        $tArchivoie=TCursoxdocente::find($idcd);
        
        if($tArchivoie!=null)
        {
            if($tArchivoie->delete())
            {
                $rutaArchivo = public_path().'/filedcte/'.session()->get('Person')->dni.'-'.$tArchivoie->idcd.'.'.$tArchivoie->formato;

                if(File::delete($rutaArchivo))
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileDcte/insertPa');
                }
                else
                {
                    return $this->helperdrea->redirectError('No se elimino el archivo correctamente.', 'fileDcte/insertPa');
                }
            }
        }

        return $this->helperdrea->redirectError('Ocurrió un error, comuniquese con el administrador.', 'insert/file');

    }
    
    public function actionEditPa(Request $request,SessionManager $sessionManager)
    {
        $tCursoxdocente=TCursoxdocente::find($request->idcd);
        if($_POST)
        {
            if(is_null($request->input('selectCurso')))
            {
                $verifyCursoxdocente = TCursoxDocente::whereRaw('dni=? and idgrado=? and idseccion=?',[session()->get('Person')->dni,
                    $request->selectGrado,
                    $request->selectSeccion
                ])->first();
            }
            else
            {
                $verifyCursoxdocente = TCursoxDocente::whereRaw('dni=? and idgrado=? and idseccion=? and idcurso=?',[session()->get('Person')->dni,
                    $request->selectGrado,
                    $request->selectSeccion,
                    $request->selectCurso
                ])->first();
            }
            // echo('$tCursoxdocente->idcd--');
            // echo($tCursoxdocente->idcd);
            // echo('<br>');
            // echo('$verifyCursoxdocente->idcd--');
            // echo($verifyCursoxdocente->idcd);
            // exit();
            if($verifyCursoxdocente!=null && $tCursoxdocente->idcd!=$verifyCursoxdocente->idcd)
            {
                return $this->helperdrea->redirectAlert('Ya existe el plan anual.', 'fileDcte/insertPa');
            }

            $tCursoxdocente->idcurso = is_null($request->input('selectCurso'))?'28':$request->selectCurso;
            $tCursoxdocente->idseccion = $request->selectSeccion;
            $tCursoxdocente->idgrado = $request->selectGrado;
            $tCursoxdocente->periodoAcademico=$request->periodo;
            $tCursoxdocente->comentario=$request->txtComentario;
            $tCursoxdocente->estadoArchivo=$request->estadoArchivo=='publico'?'1':'0';
echo('----avanzo normal');
            // echo($tCursoxdocente);exit();

            $file = $request->file('file');
            
            if($file!='')
            {
                $rutaArchivo = public_path().'/filedcte/'.session()->get('Person')->dni.'-'.$tCursoxdocente->idcd.'.'.$tCursoxdocente->formato;
                $formato = explode('.', $file->getClientOriginalName())[1];
                $peso = round(filesize($file)/1024).'-kb';

                if(File::delete($rutaArchivo))
                {
                    
                    if(in_array($formato, $this->documentos))
                    {
                        $ruta = public_path().'/filedcte';
                        $fileName = session()->get('Person')->dni.'-'.$tCursoxdocente->idcd.'.'.$formato;
                        $file->move($ruta,$fileName);

                        $tCursoxdocente->formato = $formato;
                        $tCursoxdocente->peso = $peso;

                        if($tCursoxdocente->save())
                        {
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileDcte/insertPa');
                        }
                        else
                        {
                            return $this->helperdrea->redirectError('Hubo problemas con el archivo al momento de guardar, contactese con el administrador.', 'fileDcte/insertPa');
                        }
                    }
                    else
                    {
                        return $this->helperdrea->redirectError('Ocurrió un error.', 'fileDcte/insertPa');
                    }
                }
                else
                {
                    return $this->helperdrea->redirectError('Ocurrió un error.', 'fileDcte/insertPa');
                }
            }
            else
            {
                if($tCursoxdocente->save())
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileDcte/insertPa');

                }
            }
        }
        return response()->json(['data'=>$tCursoxdocente]);
        // echo $request->idarchivoie;
    }
    public function actionInsertUS(Request $request,SessionManager $sessionManager)
    {
        return view('fileDcte/insertUS');
    }
    public function actionListFileForCurso(Request $request,SessionManager $sessionManager)
    {
        return view('fileDcte/listFileForCurso');   
    }
}