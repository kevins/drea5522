<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\File;

use Session;
use DB;
use Mail;

use App\Model\TPersona;
use App\Model\TDocente;
use App\Model\TFuncionario;
use App\Model\TArchivoie;
use App\Model\TIIEE;
use App\Model\TSesion;
use App\Model\TUnidad;
use App\Model\TCursoxdocente;
use App\Model\TDetallecursopordocente;

class FileIeController extends Controller
{
	protected $documentos = array("pdf", "docx", "xlsx");

    public function actionInsert(Request $request,SessionManager $sessionManager)
    {
    	$codigomodular = $request->session()->get('Person')->tDocente->tIIEE->codigomodular;
    	if($_POST)
    	{
    		// echo 'entro';exit();
    		$tArchivoie = new TArchivoie();
    		$tArchivoie->iddocente = $request->session()->get('Person')->dni;
    		$tArchivoie->idie = $codigomodular;
    		//lo utilizo ya que pueda que tenga varios archivos con el mismo CM no guardados
    		$tArchivoie->nombrereal = uniqid();
    		$tArchivoie->nombre = $request->input('nombre');
            $tArchivoie->compartido = 'no';
    		$tArchivoie->comentario = $request->input('comentario');
    		$tArchivoie->createdby = $request->session()->get('Person')->dni;
    		$tArchivoie->createddate = date('Y-m-d H:m:s');
            // echo('--');exit();
    		if($tArchivoie->save())
    		{
                // echo('Operación realizada correctamente');exit();
    			$file = $request->file('file');
    			if($file!='')
    			{
    				$formato = explode('.', $file->getClientOriginalName())[1];
    				$peso = round(filesize($file)/1024).'-kb';
    				// echo in_array($formato, $this->documentos);
    				if(in_array($formato, $this->documentos))
    				{
    					$tArchivoie = TArchivoie::where('nombrereal','=',$tArchivoie->nombrereal)->first();
		    			$ruta = public_path().'/fileIe';
		    			$fileName = $tArchivoie->nombrereal.'-'.$tArchivoie->idarchivoie.'.'.$formato;
		    			$file->move($ruta,$fileName);

		    			$tArchivoie->nombrereal = $tArchivoie->nombrereal.'-'.$tArchivoie->idarchivoie;
		    			$tArchivoie->formato = $formato;
	    				$tArchivoie->peso = $peso;

		    			if($tArchivoie->save())
		    			{
		    				// $sessionManager->flash('estado','se guardo exitosamente');
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileIe/insert');
		    			}
		    			else
		    			{
		    				$sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
		    			}
    				}
    				else
    				{
    					$sessionManager->flash('estado','no tiene el formato correcto');
    				}
    			}
    			else
    			{
    				// $sessionManager->flash('estado','se registro exitosamente, pero no subio ningun archivo');
                    return $this->helperdrea->redirectCorrect('Operación realizada exitosamente, pero no subio ningun archivo.', 'fileIe/insert');
    			}
    		}
    		else
    		{
    			$sessionManager->flash('no se pudo guardar el registro');
    		}
    	}
    	$listTarchivoie = TArchivoie::where('idie',$codigomodular)->where('compartido','no')->get();
    	// $listTarchivoie = json_encode($listTarchivoie);
    	// json_encode($listTarchivoie);
    	// dd(TArchivoie::where('idie',$codigomodular)->get());
    	// echo $listTarchivoie;exit();
        return view('fileIe/insert',['listTarchivoie'=>$listTarchivoie]);
        // return view('fileIe/insert',['listTarchivoie'=>TArchivoie::where('idie',$codigomodular)->get()]);
    }
    public function actionDelete(Request $request,SessionManager $sessionManager,$idarchivoie=null)
    {
        $tArchivoie=TArchivoie::find($idarchivoie);
        $ruta = $tArchivoie->compartido=='no'?'insert':'shareFile';

        if($tArchivoie!=null)
        {
            if($tArchivoie->delete())
            {
                $rutaArchivo = public_path().'/fileie/'.$tArchivoie->nombrereal.'.'.$tArchivoie->formato;

                if(File::delete($rutaArchivo))
                {
                    // $sessionManager->flash('estado','se elimino exitosamente');
                    return $this->helperdrea->redirectCorrect('Se elimino exitosamente.', 'fileIe/'.$ruta);
                }
                else
                {
                    // File::move($rutaArchivo,public_path().'/fileie/'.'borrar.'.$tArchivoie->formato);
                    // $sessionManager->flash('estado','se elimino el registro pero el archivo no se pudo eliminar.');en realidad seria este el mensaje
                    return $this->helperdrea->redirectCorrect('Se elimino exitosamente.', 'fileIe/'.$ruta);
                    // $sessionManager->flash('estado','se elimino exitosamente');
                }
            }
        }
        return redirect('fileIe/'.$ruta);
    }
    public function actionEdit(Request $request,SessionManager $sessionManager)
    {
        $tArchivoie=TArchivoie::find($request->idarchivoie);
        if($_POST)
        {
            $tArchivoie->nombre = $request->nombre;
            $tArchivoie->comentario = $request->comentario;

            $file = $request->file('file');
            if($file!='')
            {
                // echo $file;exit();
                $formato = explode('.', $file->getClientOriginalName())[1];
                $peso = round(filesize($file)/1024).'-kb';
                if(strpbrk($tArchivoie->nombrereal, '-')!='')
                {
                    $rutaArchivo = public_path().'/fileie/'.$tArchivoie->nombrereal.'.'.$tArchivoie->formato;
                    if(File::delete($rutaArchivo))
                    {
                        // $formato = explode('.', $file->getClientOriginalName())[1];
                        // $peso = round(filesize($file)/1024).'-kb';
                        // echo in_array($formato, $this->documentos);
                        if(in_array($formato, $this->documentos))
                        {
                            $ruta = public_path().'/fileIe';
                            $fileName = $tArchivoie->nombrereal.'.'.$tArchivoie->formato;
                            $file->move($ruta,$fileName);

                            // $tArchivoie->nombrereal = $tArchivoie->nombrereal.'-'.$tArchivoie->idarchivoie;
                            $tArchivoie->formato = $formato;
                            $tArchivoie->peso = $peso;

                            if($tArchivoie->save())
                            {
                                // $sessionManager->flash('estado','se guardo exitosamente');
                                return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileIe/insert');
                            }
                            else
                            {
                                // echo 'hubo problemas con el archivo al momento de guardar, contactese con el administrador';
                                $sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
                            }
                        }
                        else
                        {
                            $sessionManager->flash('estado','no tiene el formato correcto');
                        }
                    }
                    else
                    {
                        $sessionManager->flash('estado','no se pudo eliminar el archivo');
                    }
                }
                else
                {
                    //guardar
                    if(in_array($formato, $this->documentos))
                    {
                        $ruta = public_path().'/fileIe';
                        $fileName = $tArchivoie->nombrereal.'-'.$tArchivoie->idarchivoie.'.'.$formato;
                        $file->move($ruta,$fileName);

                        $tArchivoie->nombrereal = $tArchivoie->nombrereal.'-'.$tArchivoie->idarchivoie;
                        $tArchivoie->formato = $formato;
                        $tArchivoie->peso = $peso;

                        if($tArchivoie->save())
                        {
                            $sessionManager->flash('estado','se guardo exitosamente');
                        }
                        else
                        {
                            $sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
                        }
                    }
                    else
                    {
                        $sessionManager->flash('estado','no tiene el formato correcto');
                    }
                }
            }
            else
            {
                if($tArchivoie->save())
                {
                    // $sessionManager->flash('estado','se guardo exitosamente');
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileIe/insert');

                }
            }
            // strpbrk($row->nombrereal, '-')!=''
            return redirect('fileIe/insert');
        }
        return response()->json(['data'=>$tArchivoie]);
        // echo $request->idarchivoie;
    }
    public function actionListPcDcte(Request $request,SessionManager $sessionManager)
    {
        $listTdocente = $request->session()->get('Person')->tDocente->tIIEE->tDocente;
        $cm = $request->session()->get('Person')->tDocente->ie;

        //mejorar esta consulta de abajo ya que trae docentes que no tienen archivos
        if($_POST)
        {
            if($request->docente!='')
                $listPcurricular = TDocente::where('ie',$cm)->where('dni',$request->docente)->get();
            else
                $listPcurricular = TDocente::where('ie',$cm)->get();
            // echo $request->docente;exit();
        }
        else
        {
            $listPcurricular = TDocente::where('ie',$cm)->get();
        }
        // echo $listPcurricular;exit();
        return view('fileIe/listPcDcte',['listTdocente'=>$listTdocente,'listPcurricular'=>$listPcurricular]);
    }
    public function actionListArchivosDcte(Request $request,SessionManager $sessionManager)
    {
        if($request->tipoList=='plana')
        {
            $list=TCursoxdocente::whereRaw('dni=?',$request->dni)->get();
            // $anio = date("Y", $fechaComoEntero);
            return view('fileIe/list/listPa',['list'=>$list]);
        }
        if($request->tipoList=='unidad')
        {
            $list=[];
            if($request->mes!='' && $request->fi=='' && $request->ff=='')
            {
                $list = TUnidad::select('tunidad.*')
                ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                ->where('tcursoxdocente.dni',$request->dni)
                ->where('tunidad.fechaPertenece',$request->mes)
                ->get();
                return view('fileIe/list/listUnidad',['list'=>$list]);
            }
            $fi = date_create($request->fi);
            $fi = date_format($fi,'y-m-d h:i:s');
            $ff = date_create($request->ff);
            $ff = date_format($ff,'y-m-d h:i:s');

            if($request->fi!='' && $request->ff!='' && strtotime($ff)>=strtotime($fi) && $request->mes=='')
            {
                $list = TUnidad::select('tunidad.*')
                ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                ->where('tcursoxdocente.dni',$request->dni)
                ->whereBetween('tunidad.createddate', [$fi, $ff])
                ->get();
                return view('fileIe/list/listUnidad',['list'=>$list]);
            }
            $list = TUnidad::select('tunidad.*')
                ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                ->where('tcursoxdocente.dni',$request->dni)
                ->get();
            
            return view('fileIe/list/listUnidad',['list'=>$list]);
        }
        if($request->tipoList=='sesion')
        {
            $list=0;
            if($request->fi=='' && $request->ff=='')
            {
                $list = TSesion::select('tsesion.*')
                    ->join('tunidad', 'tunidad.idunidad', '=', 'tsesion.idunidad')
                    ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                    ->where('tcursoxdocente.dni',$request->dni)
                    ->get();
            }

            $fi = date_create($request->fi);
            $fi = date_format($fi,'y-m-d h:i:s');
            $ff = date_create($request->ff);
            $ff = date_format($ff,'y-m-d h:i:s');

            if($request->fi!='' && $request->ff!='' && strtotime($ff)>=strtotime($fi))
            {
                
                $list = TSesion::select('tsesion.*')
                    ->join('tunidad', 'tunidad.idunidad', '=', 'tsesion.idunidad')
                    ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                    ->where('tcursoxdocente.dni',$request->dni)
                    ->whereBetween('tsesion.createddate', [$fi, $ff])
                    ->get();
            }
            // else
            // {
            //     // return $this->helperdrea->redirectError(, 'fileIe/listPcDcte');
            //     //echo('entro l error');//mndr lgun msj y ue este no se muestr porue no se crg l pgin
            //     Session::flash('globalMessage', 'no ingreso de mner correct ls fechs.');
            //     Session::flash('type', 'error');
            //     $list=[];
            // }
            
            return view('fileIe/list/listSesion',['list'=>$list]);
        }
        return view('fileIe/listArchivosDcte',['dni'=>$request->dni]);
    }
    public function actionShareFile(Request $request,SessionManager $sessionManager)
    {
        $codigomodular = $request->session()->get('Person')->tDocente->ie;
        if($_POST)
        {
            $tArchivoie = new TArchivoie();
            $tArchivoie->iddocente = $request->session()->get('Person')->dni;
            $tArchivoie->idie = $codigomodular;
            //lo utilizo ya que pueda que tenga varios archivos con el mismo CM no guardados
            $tArchivoie->nombrereal = uniqid();
            $tArchivoie->nombre = $request->input('nombre');
            $tArchivoie->comentario = $request->input('comentario');
            $tArchivoie->video = $request->input('video');
            $tArchivoie->createdby = $request->session()->get('Person')->dni;
            $tArchivoie->createddate = date('Y-m-d H:m:s');

            $compartido='';
            $shareDcte = $request->input('docentes');
            if(!empty($shareDcte))
            {
                foreach ($shareDcte as $value) 
                {   $compartido = $compartido.'-'.$value;}
            }
            
            $tArchivoie->compartido = empty($compartido)?'todos':$compartido;
            // echo $tArchivoie;
            // exit();
            if($tArchivoie->save())
            {
                $file = $request->file('file');
                if($file!='')
                {
                    $formato = explode('.', $file->getClientOriginalName())[1];
                    $peso = round(filesize($file)/1024).'-kb';
                    // echo in_array($formato, $this->documentos);
                    if(in_array($formato, $this->documentos))
                    {
                        $tArchivoie = TArchivoie::where('nombrereal','=',$tArchivoie->nombrereal)->first();
                        $ruta = public_path().'/fileIe';
                        $fileName = $tArchivoie->nombrereal.'-'.$tArchivoie->idarchivoie.'.'.$formato;
                        $file->move($ruta,$fileName);

                        $tArchivoie->nombrereal = $tArchivoie->nombrereal.'-'.$tArchivoie->idarchivoie;
                        $tArchivoie->formato = $formato;
                        $tArchivoie->peso = $peso;

                        if($tArchivoie->save())
                        {
                            // $sessionManager->flash('estado','se guardo exitosamente');
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileIe/shareFile');
                        }
                        else
                        {
                            $sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
                        }
                    }
                    else
                    {
                        $sessionManager->flash('estado','no tiene el formato correcto');
                    }
                }
                else
                {
                    // $sessionManager->flash('estado','se registro exitosamente, pero no subio ningun archivo');
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente, pero no subio ningun archivo.', 'fileIe/shareFile');
                }
            }
            else
            {
                $sessionManager->flash('No se pudo guardar el registro');
            }
        }
        $listTdocente = $request->session()->get('Person')->tDocente->tIIEE->tDocente;
        $listTarchivoShare = TArchivoie::where('idie',$codigomodular)->where('compartido','!=','no')->get();
        // echo $listTarchivoShare;exit();
        return view('fileIe/shareFile',['listTdocente'=>$listTdocente,'listTarchivoShare'=>$listTarchivoShare]);
    }
}
