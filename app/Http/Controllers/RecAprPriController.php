<?php
namespace App\Http\Controllers;

namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\File;

use Session;
use DB;

use App\Model\TRasecundaria;
use App\Model\TUsuario;
use App\Model\TExperiencias;

class RecAprPriController extends Controller
{
    public function actionRegistrar(Request $request)
    {
        if($_POST)
        {
            $tRasecundaria=new TRasecundaria();
            $tRasecundaria->dni=session()->get('Person')->dni;
            $tRasecundaria->ide=$request->ide;
            $tRasecundaria->tipoMaterial='ficAut';
            $tRasecundaria->categoria=$request->categoria;
            $tRasecundaria->ugel=$request->ugel;
            $tRasecundaria->mes=$request->mes;
            $tRasecundaria->nivel='Primaria';
            $tRasecundaria->grado=$request->grado;
            $tRasecundaria->curso=$request->curso;
            $tRasecundaria->orden=$request->orden;
            $tRasecundaria->nombre=$request->nombre;
            $tRasecundaria->descripcion=$request->descripcion;
            $tRasecundaria->earchivo=$request->earchivo;
            $tRasecundaria->evideo=$request->evideo;
            $tRasecundaria->eaudio=$request->eaudio;
            if($tRasecundaria->save())
            {
                if($request->hasFile('archivoPdf'))
                {
                    $file = $request->file('archivoPdf');
                    $formato = explode('.', $file->getClientOriginalName())[1];
                    if($formato=='pdf')
                    {
                        $tRasecundaria=TRasecundaria::all()->last();
                        $nombreArchivo =strtolower(str_replace(' ', '_', $request->archivoPdf->getClientOriginalName()));
                        $request->file('archivoPdf')->move(public_path().'/filera/pri/pdf'.'/',$tRasecundaria->idras.'_'.$nombreArchivo);
                        $tRasecundaria->archivoPdf=$tRasecundaria->idras.'_'.$nombreArchivo;
                        $tRasecundaria->save();
                    }
                    else
                    {
                        return $this->helperdrea->redirectCorrect('Solo se acepta archivos en formato PDF.', 'recAprPri/registrar');
                    }
                }
                if($request->hasFile('archivoDoc'))
                {
                    $file = $request->file('archivoDoc');
                    $formato = explode('.', $file->getClientOriginalName())[1];
                    if($formato=='docx')
                    {
                        $tRasecundaria=TRasecundaria::all()->last();
                        $nombreArchivo =strtolower(str_replace(' ', '_', $request->archivoDoc->getClientOriginalName()));
                        $request->file('archivoDoc')->move(public_path().'/filera/pri/doc'.'/',$tRasecundaria->idras.'_'.$nombreArchivo);
                        $tRasecundaria->archivoDoc=$tRasecundaria->idras.'_'.$nombreArchivo;
                        $tRasecundaria->save();
                    }
                    else
                    {
                        return $this->helperdrea->redirectCorrect('Solo se acepta archivos en formato DOCX o WORD.', 'recAprPri/registrar');
                    }
                }
            }
            else
            {
                return $this->helperdrea->redirectCorrect('Ocurrio un problema al guardar el registro.', 'recAprPri/registrar');
            }
            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'recAprPri/registrar');
        }
        $listTexperiencias=TExperiencias::whereRaw('nivel=?',['Primaria'])->get();
        $listTrasecundaria=TRasecundaria::whereRaw('dni=? && nivel=?',[session()->get('Person')->dni,'Primaria'])->get();
        return view('recAprPri/registrar',['listTexperiencias'=>$listTexperiencias,'listTrasecundaria'=>$listTrasecundaria]);
    }
    public function actionEditar(Request $request)
    {
        $tSocioemocional = TSocioemocional::find($request->idse);
        if($_POST)
        {
            $tSocioemocional->ugel=$request->ugel;
            $tSocioemocional->categoria=$request->categoria;
            $tSocioemocional->nombre=$request->nombre;
            $tSocioemocional->descripcion=$request->descripcion;
            $tSocioemocional->earchivo=$request->earchivo;
            $tSocioemocional->evideo=$request->evideo;
            $tSocioemocional->eaudio=$request->eaudio;
            if($tSocioemocional->save())
            {
                if($request->hasFile('archivo'))
                {
                    $archivoEliminado=true;
                    if($tSocioemocional->archivo!='')
                    {
                        // dd('Se eliminon el archivo anterior');
                        $rutaArchivo = public_path().'/filesocioemocional/'.$tSocioemocional->archivo;
                        if(!File::delete($rutaArchivo))
                        {
                            $archivoEliminado=false;
                        }
                    }
                    if($archivoEliminado)
                    {
                        // dd('entro ya q no tiene archivo');
                        $nombreArchivo =strtolower(str_replace(' ', '_', $request->archivo->getClientOriginalName()));
                        $request->file('archivo')->move(public_path().'/filesocioemocional'.'/',$tSocioemocional->idse.'_'.$nombreArchivo);
                        $tSocioemocional->archivo=$tSocioemocional->idse.'_'.$nombreArchivo;
                        // $tSocioemocional->save();
                        if($tSocioemocional->save())
                        {
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'socioEmocional/listar');
                        }
                        else
                        {
                            return $this->helperdrea->redirectCorrect('Error al guardar el nuevo archivo.', 'socioEmocional/listar');
                        }
                    }
                    else
                    {
                        return $this->helperdrea->redirectCorrect('Error al eliminar el archivo anterior.', 'socioEmocional/listar');
                    }
                }
            }
            else
            {
                return $this->helperdrea->redirectCorrect('Ocurrio un problema al guardar cambios.', 'socioEmocional/listar');
            }
            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'socioEmocional/listar');
        }
        
        return response()->json(['data'=>$tSocioemocional]);
    }
    public function actionDelete($idra=null)
    {
        $tRasecundaria=TRasecundaria::find($idra);
        
        if($tRasecundaria!=null)
        {
            if($tRasecundaria->delete())
            {
                if($tRasecundaria->archivoPdf!='')
                {
                    $rutaArchivo = public_path().'/filera/pri/pdf/'.$tRasecundaria->archivoPdf;
                    if(File::delete($rutaArchivo))
                    {
                        if($tRasecundaria->archivoDoc!='')
                        {
                            $rutaArchivo = public_path().'/filera/pri/doc/'.$tRasecundaria->archivoDoc;
                            if(File::delete($rutaArchivo))
                            {
                                return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'recAprPri/registrar');
                            }
                            else
                            {
                                return $this->helperdrea->redirectError('Ocurrió un error al eliminar el archivo en word.', 'recAprPri/registrar');
                            }
                        }
                        else
                        {
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'recAprPri/registrar');
                        }
                    }
                    else
                    {
                        return $this->helperdrea->redirectError('Ocurrió un error al eliminar el archivo pdf.', 'recAprPri/registrar');
                    }
                }
                else
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'recAprPri/registrar');
                }
            }
        }
        return $this->helperdrea->redirectError('No se encontro el registro.', 'recAprPri/registrar');

    }
    public function actionLogout(Request $request,SessionManager $sessionManager)
    {
        $sessionManager->flush();
        return redirect('/');
    }
    // portal
    public function actionListarPortal(Request $request)
    {
        $grado=$request->grado+1;
        $list=TRecursosaprendizaje::whereRaw('nivel=? and grado=? and tipoMaterial=? and mes=?',[$request->nivel,$grado,$request->tipo,$request->mes])->get();
        return view('portal/recursosAprendizaje/listarPortal',['list'=>$list]);
    }
}
