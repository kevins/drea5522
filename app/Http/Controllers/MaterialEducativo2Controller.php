<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Session\SessionManager;
// use Illuminate\Encryption\Encrypter;
// use Illuminate\Support\Facades\Response;

// use Session;

use App\Model\TArchivospecialist;

class MaterialEducativo2Controller extends Controller
{
    public function actionMostrar2(Request $request)
    {
        return view('portal/materialEducativo2/mostrar2');
    }
    public function actionBuscarSemana2(Request $request)
    {
        // dd($request->data);
        $array = explode("-", $request->data);


        if($array[1]!='sec')
        {
            $listGuias = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana',$array[2])
                ->where('contenido','guias')
                ->get();
            $listRecursos = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana',$array[2])
                ->where('contenido','recursos')
                ->get();
            $listOtros = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana',$array[2])
                ->where('contenido','otros')
                ->get();
            $data = $array[0].'-'.$array[1];
            // dd($listTae[0]->nombre);
            return view('portal/materialEducativo2/buscarSemana2',['listGuias'=>$listGuias,'listRecursos'=>$listRecursos,'listOtros'=>$listOtros,'data'=>$data,'semana'=>$array[2]]);
        }
        else
        {
            // dd($request->data);
            $listGuias = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana',$array[3])
                ->where('contenido','guias')
                ->get();
            $listRecursos = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana',$array[3])
                ->where('contenido','recursos')
                ->get();
            $listOtros = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana',$array[3])
                ->where('contenido','otros')
                ->get();
            $data = $array[0].'-'.$array[1].'-'.$array[2];
            return view('portal/materialEducativo2/buscarSemana2',['listGuias'=>$listGuias,'listRecursos'=>$listRecursos,'listOtros'=>$listOtros,'data'=>$data,'semana'=>$array[3]]);
        }
    }
    public function actionBuscar2(Request $request)
    {
    	$array = explode("-", $request->data);

    	if($array[1]!='sec')
    	{
	    	$listGuias = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana','semana5')
                ->where('contenido','guias')
                ->get();
            $listRecursos = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana','semana5')
                ->where('contenido','recursos')
                ->get();
            $listOtros = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana','semana5')
                ->where('contenido','otros')
                ->get();
            
	    	// dd($listTae[0]->nombre);
	    	return view('portal/materialEducativo2/buscar2',['listGuias'=>$listGuias,'listRecursos'=>$listRecursos,'listOtros'=>$listOtros,'data'=>$request->data]);
    	}
    	else
    	{
            // dd($request->all());
            // echo 'es secundaria';
            $listGuias = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana','semana5')
                ->where('contenido','guias')
                ->get();
            $listRecursos = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana','semana5')
                ->where('contenido','recursos')
                ->get();
            $listOtros = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana','semana5')
                ->where('contenido','otros')
                ->get();
            return view('portal/materialEducativo2/buscarCurso2',['listGuias'=>$listGuias,'listRecursos'=>$listRecursos,'listOtros'=>$listOtros,'data'=>$request->data]);
    	}
    }
}
