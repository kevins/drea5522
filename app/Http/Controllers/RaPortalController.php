<?php
namespace App\Http\Controllers;

namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\File;

use Session;
use DB;

use App\Model\TRasecundaria;
use App\Model\TUsuario;
use App\Model\TExperiencias;

class RaPortalController extends Controller
{
    public function actionListarSecundaria(Request $request)
    {
        $listMes= TExperiencias::select(DB::raw('mes'))
            ->where('nivel','secundaria')
            ->groupBy('mes')
            ->get();
        // echo($listMes);exit();
        return view('portal/ra/sec/listarSecundaria',['listMes'=>$listMes]);
    }
    public function actionBuscarSecundaria(Request $request)
    {
    	if($request->mes=='' && $request->grado=='')
    	{
    		return response()->json(['data'=>false]);
    		 // return $this->helperdrea->redirectError('Es necesario que ingrese los datos correctos.', 'raPortal/listarSecundaria');
    	}
    	$listEa=TExperiencias::whereRaw('mes=? && nivel=? && grado=?',[$request->mes,'Secundaria',$request->grado])->first();
        // echo($listEa);
        // echo('ckabsckjabsbjk');
        // exit();
        
        $listFaCs=TRasecundaria::whereRaw('ide=? && nivel=? && curso=?',[$listEa->ide,'Secundaria','ciencias sociales'])->orderBy('orden','ASC')->get();
        $listFaComu=TRasecundaria::whereRaw('ide=? && nivel=? && curso=?',[$listEa->ide,'Secundaria','comunicacion'])->orderBy('orden','ASC')->get();
        $listFaMate=TRasecundaria::whereRaw('ide=? && nivel=? && curso=?',[$listEa->ide,'Secundaria','matematica'])->orderBy('orden','ASC')->get();
        $listFaCyt=TRasecundaria::whereRaw('ide=? && nivel=? && curso=?',[$listEa->ide,'Secundaria','ciencia y tecnologia'])->orderBy('orden','ASC')->get();
        return view('portal/ra/sec/buscarSecundaria',[
            'listEa'=>$listEa,
            'listFaCs'=>$listFaCs,
            'listFaComu'=>$listFaComu,
            'listFaMate'=>$listFaMate,
            'listFaCyt'=>$listFaCyt,
            'mes'=>$request->mes,
            'grado'=>$request->grado]);

    }
    // echo($request->ide);exit();
    public function actionBuscarSecundariaFichas(Request $request)
    {

        $tExperiencias = TExperiencias::find($request->ide);
        $listFaCs=TRasecundaria::whereRaw('ide=? && nivel=? && curso=?',[$request->ide,'Secundaria','ciencias sociales'])->orderBy('orden','ASC')->get();
        $listFaComu=TRasecundaria::whereRaw('ide=? && nivel=? && curso=?',[$request->ide,'Secundaria','comunicacion'])->orderBy('orden','ASC')->get();
        $listFaMate=TRasecundaria::whereRaw('ide=? && nivel=? && curso=?',[$request->ide,'Secundaria','matematica'])->orderBy('orden','ASC')->get();
        $listFaCyt=TRasecundaria::whereRaw('ide=? && nivel=? && curso=?',[$request->ide,'Secundaria','ciencia y tecnologia'])->orderBy('orden','ASC')->get();

        return view('portal/ra/sec/buscarSecundariaFichas',[
            'listFaCs'=>$listFaCs,
            'listFaComu'=>$listFaComu,
            'listFaMate'=>$listFaMate,
            'listFaCyt'=>$listFaCyt,
            'tExperiencias'=>$tExperiencias]);

    }
    public function actionListarPrimaria(Request $request)
    {
        $listMes= TExperiencias::select(DB::raw('mes'))
            ->where('nivel','Primaria')
            ->groupBy('mes')
            ->get();
        // echo($listMes);exit();
        return view('portal/ra/pri/listarPrimaria',['listMes'=>$listMes]);
    }
    public function actionBuscarPrimaria(Request $request)
    {
        // $this->cat1($request->mes,$request->grado,$request->categoria);

        if($request->categoria=='cat1')
        {
            if($request->mes=='' || $request->grado=='' || $request->categoria=='')
            {
                return response()->json(['data'=>false]);
            }
            $listEa=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && tipoMaterial=? && categoria=?',[$request->mes,'Primaria',$request->grado,'expApr',$request->categoria])->get();
            $listFaCs=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && tipoMaterial=? && curso=? && categoria=?',[$request->mes,'Primaria',$request->grado,'ficAut','ciencias sociales',$request->categoria])->orderBy('orden','ASC')->get();
            $listFaComu=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && tipoMaterial=? && curso=? && categoria=?',[$request->mes,'Primaria',$request->grado,'ficAut','comunicacion',$request->categoria])->orderBy('orden','ASC')->get();
            $listFaMate=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && tipoMaterial=? && curso=? && categoria=?',[$request->mes,'Primaria',$request->grado,'ficAut','matematica',$request->categoria])->orderBy('orden','ASC')->get();
            $listFaCyt=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && tipoMaterial=? && curso=? && categoria=?',[$request->mes,'Primaria',$request->grado,'ficAut','ciencia y tecnologia',$request->categoria])->orderBy('orden','ASC')->get();
            $listFaOtr=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && tipoMaterial=? && curso=? && categoria=?',[$request->mes,'Primaria',$request->grado,'ficAut','otras',$request->categoria])->orderBy('orden','ASC')->get();
            return view('portal/ra/pri/buscarPrimaria',[
                'listEa'=>$listEa,
                'listFaCs'=>$listFaCs,
                'listFaComu'=>$listFaComu,
                'listFaMate'=>$listFaMate,
                'listFaCyt'=>$listFaCyt,
                'listFaOtr'=>$listFaOtr,
                'grado'=>$request->grado,
                'categoria'=>$request->categoria]);
        }
        if($request->categoria=='cat2')
        {
            if($request->mes=='' || $request->categoria=='')
            {
                return response()->json(['data'=>false]);
            }
            $listEa=TRasecundaria::whereRaw('mes=? && nivel=? && tipoMaterial=? && categoria=?',[$request->mes,'Primaria','expApr',$request->categoria])->get();
            $listFaCs=TRasecundaria::whereRaw('mes=? && nivel=? && tipoMaterial=? && curso=? && categoria=?',[$request->mes,'Primaria','ficAut','ciencias sociales',$request->categoria])->orderBy('orden','ASC')->get();
            $listFaComu=TRasecundaria::whereRaw('mes=? && nivel=? && tipoMaterial=? && curso=? && categoria=?',[$request->mes,'Primaria','ficAut','comunicacion',$request->categoria])->orderBy('orden','ASC')->get();
            $listFaMate=TRasecundaria::whereRaw('mes=? && nivel=? && tipoMaterial=? && curso=? && categoria=?',[$request->mes,'Primaria','ficAut','matematica',$request->categoria])->orderBy('orden','ASC')->get();
            $listFaCyt=TRasecundaria::whereRaw('mes=? && nivel=? && tipoMaterial=? && curso=? && categoria=?',[$request->mes,'Primaria','ficAut','ciencia y tecnologia',$request->categoria])->orderBy('orden','ASC')->get();
            $listFaOtr=TRasecundaria::whereRaw('mes=? && nivel=? && tipoMaterial=? && curso=? && categoria=?',[$request->mes,'Primaria','ficAut','otras',$request->categoria])->orderBy('orden','ASC')->get();
            return view('portal/ra/pri/buscarPrimaria',[
                'listEa'=>$listEa,
                'listFaCs'=>$listFaCs,
                'listFaComu'=>$listFaComu,
                'listFaMate'=>$listFaMate,
                'listFaCyt'=>$listFaCyt,
                'listFaOtr'=>$listFaOtr,
                'grado'=>$request->grado,
                'categoria'=>$request->categoria]);
        }
        if($request->categoria=='cat3' || $request->categoria=='cat5')
        {
            if($request->mes=='' || $request->categoria=='' || $request->grado=='')
            {
                return response()->json(['data'=>false]);
            }
            // $listEa=TRasecundaria::whereRaw('mes=? && nivel=? && tipoMaterial=? && categoria=?',[$request->mes,'Primaria','expApr',$request->categoria])->get();
            $listEa=TExperiencias::whereRaw('mes=? && nivel=? && grado=? && categoria=?',[$request->mes,'Primaria',$request->grado,$request->categoria])->get();
            // -----------------------
            // $grado1=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && categoria=?',[$request->mes,'Primaria','1',$request->categoria])->orderBy('orden','ASC')->get();
            // $grado2=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && categoria=?',[$request->mes,'Primaria','2',$request->categoria])->orderBy('orden','ASC')->get();
            // $grado3=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && categoria=?',[$request->mes,'Primaria','3',$request->categoria])->orderBy('orden','ASC')->get();
            // $grado4=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && categoria=?',[$request->mes,'Primaria','4',$request->categoria])->orderBy('orden','ASC')->get();
            // $grado5=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && categoria=?',[$request->mes,'Primaria','5',$request->categoria])->orderBy('orden','ASC')->get();
            // $grado6=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && categoria=?',[$request->mes,'Primaria','6',$request->categoria])->orderBy('orden','ASC')->get();
            // -----------------------
            $grado1=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && categoria=?',[$request->mes,'Primaria',$request->grado,$request->categoria])->orderBy('orden','ASC')->get();
            // return view('portal/ra/pri/buscarPrimaria',[
            //     'listEa'=>$listEa,
            //     'grado1'=>$grado1,
            //     'grado2'=>$grado2,
            //     'grado3'=>$grado3,
            //     'grado4'=>$grado4,
            //     'grado5'=>$grado5,
            //     'grado6'=>$grado6,
            //     'grado'=>$request->grado,
            //     'categoria'=>$request->categoria]);
            return view('portal/ra/pri/buscarPrimaria',[
                'listEa'=>$listEa,
                'grado1'=>$grado1,
                'grado'=>$request->grado,
                'categoria'=>$request->categoria]);
        }

    }
    public function actionListarInicial(Request $request)
    {
        // $listEa=TExperiencias::whereRaw('nivel=? && grado=?',['Inicial','5'])->get();
        // return view('portal/ra/ini/buscarInicial',[
        //     'listEa'=>$listEa,
        //     'mes'=>'Junio',
        //     'grado'=>'5']);


        $listMes= TExperiencias::select(DB::raw('mes'))
            ->where('nivel','Inicial')
            ->groupBy('mes')
            ->get();
        return view('portal/ra/ini/listarInicial',['listMes'=>$listMes]);
    }

    public function actionBuscarInicial(Request $request)
    {
        if($request->mes=='')
        {
            return response()->json(['data'=>false]);
        }
        $listEa=TExperiencias::whereRaw('mes=? && nivel=?',[$request->mes,'Inicial'])->get();
        $listAa=TRasecundaria::whereRaw('mes=? && nivel=?',[$request->mes,'Inicial'])->orderBy('orden','ASC')->get();
        // $listFaComu=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && tipoMaterial=? && curso=?',[$request->mes,'Secundaria',$request->grado,'ficAut','comunicacion'])->orderBy('orden','ASC')->get();
        // $listFaMate=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && tipoMaterial=? && curso=?',[$request->mes,'Secundaria',$request->grado,'ficAut','matematica'])->orderBy('orden','ASC')->get();
        // $listFaCyt=TRasecundaria::whereRaw('mes=? && nivel=? && grado=? && tipoMaterial=? && curso=?',[$request->mes,'Secundaria',$request->grado,'ficAut','ciencia y tecnologia'])->orderBy('orden','ASC')->get();
        return view('portal/ra/ini/buscarInicial',[
            'listEa'=>$listEa,
            'listAa'=>$listAa,
            'mes'=>$request->mes]);

    }
}
