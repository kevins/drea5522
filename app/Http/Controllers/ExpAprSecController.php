<?php
namespace App\Http\Controllers;

namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\File;

use Session;
use DB;

use App\Model\TRasecundaria;
use App\Model\TUsuario;
use App\Model\TExperiencias;

class ExpAprSecController extends Controller
{
    public function actionRegistrar(Request $request)
    {
        if($_POST)
        {
            $tExperiencias=new TExperiencias();
            $tExperiencias->dni=session()->get('Person')->dni;
            $tExperiencias->ugel=$request->ugel;
            $tExperiencias->mes=$request->mes;
            $tExperiencias->grado=$request->grado;
            $tExperiencias->nivel='Secundaria';
            $tExperiencias->nombre=$request->nombre;
            $tExperiencias->descripcion=$request->descripcion;
            $tExperiencias->earchivo=$request->earchivo;
            $tExperiencias->evideo=$request->evideo;
            $tExperiencias->eaudio=$request->eaudio;

            if($tExperiencias->save())
            {
                if($request->hasFile('archivoPdf'))
                {
                    $file = $request->file('archivoPdf');
                    $formato = explode('.', $file->getClientOriginalName())[1];
                    if($formato=='pdf')
                    {
                        $tExperiencias=TExperiencias::all()->last();
                        $nombreArchivo =strtolower(str_replace(' ', '_', $request->archivoPdf->getClientOriginalName()));
                        $request->file('archivoPdf')->move(public_path().'/fileea/sec/pdf'.'/',$tExperiencias->ide.'_'.$nombreArchivo);
                        $tExperiencias->archivoPdf=$tExperiencias->ide.'_'.$nombreArchivo;
                        $tExperiencias->save();
                    }
                    else
                    {
                        return $this->helperdrea->redirectCorrect('Solo se acepta archivos en formato PDF.', 'expAprSec/registrar');
                    }
                }
                if($request->hasFile('archivoDoc'))
                {
                    $file = $request->file('archivoDoc');
                    $formato = explode('.', $file->getClientOriginalName())[1];
                    if($formato=='docx')
                    {
                        $tExperiencias=TExperiencias::all()->last();
                        $nombreArchivo =strtolower(str_replace(' ', '_', $request->archivoDoc->getClientOriginalName()));
                        $request->file('archivoDoc')->move(public_path().'/fileea/sec/doc'.'/',$tExperiencias->ide.'_'.$nombreArchivo);
                        $tExperiencias->archivoDoc=$tExperiencias->ide.'_'.$nombreArchivo;
                        $tExperiencias->save();
                    }
                    else
                    {
                        return $this->helperdrea->redirectCorrect('Solo se acepta archivos en formato DOCX o WORD.', 'expAprSec/registrar');
                    }
                }
            }
            else
            {
                return $this->helperdrea->redirectCorrect('Ocurrio un problema al guardar el registro.', 'expAprSec/registrar');
            }
            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'expAprSec/registrar');
        }
        $list=TExperiencias::whereRaw('nivel=?',['Secundaria'])->get();
        return view('recApr/sec/registrar',['list'=>$list]);
    }
    public function actionListar()
    {
        $list=TRecursosaprendizaje::whereRaw('dni=?',session()->get('Person')->dni)->get();
        return view('recursosAprendizaje/listar',['list'=>$list]);
    }
    public function actionEditar(Request $request)
    {
        $tExperiencias = TExperiencias::find($request->ide);
        // echo($tExperiencias);exit();
        if($_POST)
        {
            // --------
            $tExperiencias->ugel=$request->eugel;
            $tExperiencias->mes=$request->emes;
            $tExperiencias->grado=$request->egrado;
            $tExperiencias->nombre=$request->enombre;
            $tExperiencias->descripcion=$request->edescripcion;
            $tExperiencias->earchivo=$request->eearchivo;
            $tExperiencias->evideo=$request->eevideo;
            $tExperiencias->eaudio=$request->eeaudio;
            // --------
            if($tExperiencias->save())
            {

                if($request->hasFile('earchivoPdf'))
                {
                    $file = $request->file('earchivoPdf');
                    $formato = explode('.', $file->getClientOriginalName())[1];
                    // echo('llego hasta aka---'.$formato);exit();
                    if($formato=='pdf')
                    {
                        // $tExperiencias=TExperiencias::all()->last();
                        $nombreArchivo =strtolower(str_replace(' ', '_', $request->earchivoPdf->getClientOriginalName()));
                        $request->file('earchivoPdf')->move(public_path().'/fileea/sec/pdf'.'/',$tExperiencias->ide.'_'.$nombreArchivo);
                        $tExperiencias->archivoPdf=$tExperiencias->ide.'_'.$nombreArchivo;
                        $tExperiencias->save();
                    }
                    else
                    {
                        return $this->helperdrea->redirectCorrect('Solo se acepta archivos en formato PDF.', 'expAprSec/registrar');
                    }
                }
                if($request->hasFile('earchivoDoc'))
                {
                    $file = $request->file('earchivoDoc');
                    $formato = explode('.', $file->getClientOriginalName())[1];
                    if($formato=='docx')
                    {
                        // $tExperiencias=TExperiencias::all()->last();
                        $nombreArchivo =strtolower(str_replace(' ', '_', $request->earchivoDoc->getClientOriginalName()));
                        $request->file('earchivoDoc')->move(public_path().'/fileea/sec/doc'.'/',$tExperiencias->ide.'_'.$nombreArchivo);
                        $tExperiencias->archivoDoc=$tExperiencias->ide.'_'.$nombreArchivo;
                        $tExperiencias->save();
                    }
                    else
                    {
                        return $this->helperdrea->redirectCorrect('Solo se acepta archivos en formato DOCX o WORD.', 'expAprSec/registrar');
                    }
                }
                // echo('no entro a ningun ig');exit();
            }
            else
            {
                return $this->helperdrea->redirectCorrect('Ocurrio un problema al guardar el registro.', 'expAprSec/registrar');
            }
            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'expAprSec/registrar');
        }
        
        return response()->json(['data'=>$tExperiencias]);
    }
    public function actionDelete($ide=null)
    {
        $tExperiencias=TExperiencias::find($ide);
        
        if($tExperiencias!=null)
        {
            if($tExperiencias->delete())
            {
                if($tExperiencias->archivoPdf!='')
                {
                    $rutaArchivo = public_path().'/fileea/sec/pdf/'.$tExperiencias->archivoPdf;
                    if(File::delete($rutaArchivo))
                    {
                        if($tExperiencias->archivoDoc!='')
                        {
                            $rutaArchivo = public_path().'/fileea/sec/doc/'.$tExperiencias->archivoDoc;
                            if(File::delete($rutaArchivo))
                            {
                                return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'expAprSec/registrar');
                            }
                            else
                            {
                                return $this->helperdrea->redirectError('Ocurrió un error al eliminar el archivo en word.', 'expAprSec/registrar');
                            }
                        }
                        else
                        {
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'expAprSec/registrar');
                        }
                    }
                    else
                    {
                        return $this->helperdrea->redirectError('Ocurrió un error al eliminar el archivo.', 'expAprSec/registrar');
                    }
                }
                else
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'expAprSec/registrar');
                }
            }
        }

        return $this->helperdrea->redirectError('No se encontro el registro.', 'expAprSec/registrar');

    }
    public function actionLogout(Request $request,SessionManager $sessionManager)
    {
        $sessionManager->flush();
        return redirect('/');
    }
    // portal
    public function actionListarPortal(Request $request)
    {
        $grado=$request->grado+1;
        $list=TRecursosaprendizaje::whereRaw('nivel=? and grado=? and tipoMaterial=? and mes=?',[$request->nivel,$grado,$request->tipo,$request->mes])->get();
        return view('portal/recursosAprendizaje/listarPortal',['list'=>$list]);
    }
}
