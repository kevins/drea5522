<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
 
use Session;
use DB;
use Mail;

use App\Model\TCurso;
use App\Model\TGrado;
use App\Model\TUnidad;
use App\Model\TSesion;
use App\Model\TDocente;
use App\Model\TArchivoie;
use App\Model\TCursoxdocente;
use App\Model\TIIEE;
use App\Model\TArchivospecialist;

class FilesController extends Controller
{
    public function actionView(Request $request)
    {
        if($request->nivel=='inicial')
            return view('files/inicial');
        if($request->nivel=='primaria')
            return view('files/primaria');
        if($request->nivel=='secundaria')
            return view('files/secundaria');
    }
    public function actionViewList(Request $request)
    {
        // echo $request->nivel;
        // echo 'EN ESTOS MOMENTOS ESTAMOS EN MANTENIMIENTO, POR FAVOR INTENTE MAS TARDE';
        $nivel = explode("-", $request->nivel);
        // echo $nivel[0];exit();
        if($nivel[0]=='inicial')
        {
            switch ($nivel[1]) 
            {
                case 0:echo "i es igual a 0";break;
                case 1:echo "i es igual a 1";break;
                case 2:echo "i es igual a 2";break;
                case 3:echo "i es igual a 2";break;
                case 4:echo "i es igual a 2";break;
                case 5:return view('files/inicial/5aos');break;
                case 6:echo "i es igual a 2";break;
            }
        }
        if($nivel[0]=='primaria')
        {
            switch ($nivel[1]) 
            {
                case 3:return view('files/primaria/3ciclo');break;
                case 4:return view('files/primaria/4ciclo');break;
                case 5:return view('files/primaria/5ciclo');break;
            }
        }
        if($nivel[0]=='secundaria')
        {
            switch ($nivel[1]) 
            {
                case 'cyt':return view('files/secundaria/cyt');break;
                case 'pdcc':return view('files/secundaria/pdcc');break;
                case 'matematica':return view('files/secundaria/matematica');break;
                case 'comunicacion':return view('files/secundaria/comunicacion');break;
            }
        }
        
    	// return view('files/list');
    }
}
