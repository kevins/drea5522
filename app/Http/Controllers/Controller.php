<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Helper\Helperdrea;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    
	protected $globalMessage;
    protected $helperdrea;

    public function __construct()
    {
    	$this->globalMessage='';
    	$this->helperdrea= new Helperdrea();
    }
}
