<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Response;

use Session;
use DB;
use Mail;

use App\Model\TIIEE;
use App\Model\TDocente;
use App\Model\TPersona;

class DcteController extends Controller
{
    public function actionOption(Request $request,SessionManager $sessionManager)
    {
        return view('dcte/option');
    }
    public function actionAddDcte(Request $request,SessionManager $sessionManager,Encrypter $encrypter)
    {
    	$ie = TIIEE::where('codigomodular',$request->cm)->first();
        if($_POST)
        { 
            try
            {
                DB::beginTransaction();

                $tDocente = TDocente::where('dni',$request->dni)->first();

                if($tDocente!=null)
                {
                    return $this->helperdrea->redirectAlert('Ya existe el docente.', 'dcte/option');
                }
                $tPersona= new TPersona();
                $tPersona->dni=$request->dni;
                $tPersona->apaterno=$request->apaterno;
                $tPersona->amaterno=$request->amaterno;
                $tPersona->nombres=$request->nombre;
                $tPersona->sexo=$request->sexo=='M'?'M':'F';
                $tPersona->fnacimiento=$request->fNacimiento;

                $tPersona->save();

                $tDocente= new TDocente();
                $tDocente->dni=$request->dni;
                $tDocente->password=$encrypter->encrypt('@repo2020');
                $tDocente->ie=$request->cm;
                $tDocente->cargo=$request->cargo;
                $tDocente->condicion=$request->condicion;
                $tDocente->director=0;
                $tDocente->created_by=$request->session()->get('Person')->dni;
                $tDocente->created_date=date('Y-m-d H:m:s');
                $tDocente->estado=$request->estado=='activo'?'1':'0';

                $tDocente->save();

                DB::commit();
                
                return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'dcte/option');
            }
            catch(\exception $ex)
            {
                DB::rollback();
                return $this->helperdrea->redirectError('Algo salió mal, comuniquese con el administrador.', 'dcte/option');
            }
        }
        return view('dcte/addDcte',['ie'=>$ie]);
    }
    public function actionListDcte(Request $request,SessionManager $sessionManager)
    {
    	$ie = TIIEE::where('codigomodular',$request->cm)->first();
        // $docentes = TPersona::select('persona.*')
        //     ->join('docente', 'docente.dni', '=', 'persona.dni')
        //     ->where('docente.ie',$request->cm)
        //     ->get();

        // return response()->json(['data'=>$ie,'ugel'=>$ie->tUgel->ugel_nombre,'director'=>$director,'docentes'=>$docentes]);
            // return view('dcte/listDcte');
        return view('dcte/listDcte',['ie'=>$ie]);
    }
    public function actionEditEstado(Request $request,SessionManager $sessionManager)
    {
    	$docente = TDocente::where('dni',$request->dni)->first();
    	$docente->estado=$docente->estado==1?0:1;
    	if($docente->save())
    	{
    		return response()->json(['success'=>1,'dni'=>$request->dni]);
    	}
    	return response()->json(['success'=>0,'dni'=>$request->dni]);
    }
    public function actionDelegarDirector(Request $request,SessionManager $sessionManager)
    {
        $docente = TDocente::where('dni',$request->dni)->first();
        $docente->director=$docente->director==1?0:1;
        $docente->delegado=$docente->delegado==1?0:1;
        if($docente->save())
        {
            return response()->json(['success'=>1,'dni'=>$request->dni]);
        }
        return response()->json(['success'=>0,'dni'=>$request->dni]);
    }
    public function actionChangeIe(Request $request,SessionManager $sessionManager)
    {
        // echo $request->cm;
        $docente = TDocente::where('dni',$request->dni)->first();
        $docente->ie = $request->cm;

        if($docente->save())
        {
            return response()->json(['success'=>1,'nombreDcte'=>$docente->tPersona_->nombres]);
        }
        return response()->json(['success'=>0,'dni'=>$request->dni]);
        // $docente->director=$docente->director==1?0:1;
        // $docente->delegado=$docente->delegado==1?0:1;
        // if($docente->save())
        // {
        //     return response()->json(['success'=>1,'dni'=>$request->dni]);
        // }
        // return response()->json(['success'=>0,'dni'=>$request->dni]);
    }
    
}
